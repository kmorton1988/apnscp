#!/usr/bin/env apnscp_php
<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	define('INCLUDE_PATH', dirname(__DIR__) . '/');
	include INCLUDE_PATH . '/lib/CLI/cmd.php';
	$args = [];
	register_shutdown_function(static function () use (&$args) {
		if (!\Error_Reporter::is_error()) {
			return;
		}
		if (is_debug() || array_get($args, 'options.output') === 'json') {
			// already listed or to be listed
			return;
		}
		foreach (\Error_Reporter::get_errors() as $e) {
			fwrite(STDERR, \Error_Reporter::errno2str(\Error_Reporter::E_ERROR) . ": $e\n");
		}
	});
	\Opcenter\Lock::lock();
	$args = Opcenter\CliParser::parse();

	$force = $args['options']['force'] ?? false;
	$sites = \Opcenter\CliParser::getSiteFromArgs(...array_get($args, 'command'));
	foreach ($sites as $site) {
		$instance = new \Opcenter\Account\Activate($site);

		try {
			$ret = $instance->exec();
		} catch (\Throwable $e) {
			\Error_Reporter::handle_exception($e);
		}
		$domain = $instance->getDomain();

		$buffer = Error_Reporter::get_buffer();
		dlog('Activated %s, %s (%x) %s',
			$site,
			$domain,
			strtoupper(Error_Reporter::error_type(Error_Reporter::get_severity()) ?: 'success'),
			Error_Reporter::is_error() ? 'Failed' : 'Succeeded'
		);
	}
	//cli\dump_buffer($buffer);
	exit ((int)Error_Reporter::is_error());
