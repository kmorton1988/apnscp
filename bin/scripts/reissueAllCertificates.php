<?php
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
 */

/**
 * Reissue all Let's Encrypt certificates for a given location.
 * Useful if the LE storage location changes in a later release.
 */

define('INCLUDE_PATH', realpath(__DIR__ . '/../../'));
include(INCLUDE_PATH . '/lib/CLI/cmd.php');
$old = \Error_Reporter::set_verbose();
\Error_Reporter::set_verbose(max($old, 2));
// initialize user, root
\cli\cmd();
if (empty($_SERVER['argv'][1])) {
    fatal("usage: %s dirname\n\t%s",
        basename($_SERVER['argv'][0]),
        "dirname is the directory to scan for Let's Encrypt certificates and renew all"
    );
}
$dir = $_SERVER['argv'][1];

if (!is_dir($dir)) {
    fatal("`%s' is not a directory", $dir);
}

$sslModule = new Ssl_Module();
$renewal = new class extends Letsencrypt_Module
{
    public function __invoke($site, $x509)
    {
        return (new static)->_renew($site, $x509);
    }
};

$dh = opendir($dir) or fatal("failed to open directory `%s'", $dir);
while (false !== ($entry = readdir($dh))) {
    $site = basename($entry);
    $certFile = "${dir}/${entry}/cert.pem";
    if (!file_exists($certFile)) {
        continue;
    }
    $x509 = $sslModule->parse_certificate(file_get_contents($certFile));
    if (!(new Letsencrypt_Module())->is_ca($x509)) {
        continue;
    }
    $san = $sslModule->get_alternative_names($x509);

    info("Renewing %s, with names: %s\n", $site, join(",", $san));
    if (!$renewal($site, $x509)) {
        error("! Failed to renew `%s'", $site);
    }
    // sleep a little to avoid triggering rate-limiter
    usleep(500000);
}
closedir($dh);
exit((int)\Error_Reporter::is_error());