#!/usr/bin/apnscp_php
<?php
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
 */

	use Opcenter\Role\User;
	use Opcenter\SiteConfiguration;

	define('INCLUDE_PATH', realpath(dirname($_SERVER['SCRIPT_FILENAME']) . '/../../'));
	include(INCLUDE_PATH . '/lib/CLI/cmd.php');


	class Backup_CLI
	{
		// require free space equal to
		// 2% of database size to backup
		const PERCENTAGE_MIN = 0.02;
		private static $mysql_password;
		private static $admin_cache;
		private static $uid_cache;
		private static $sql;
		private static $afi;


		private static function init()
		{
			self::$afi = cli\cmd();
			self::$admin_cache = array();
			self::$uid_cache = array();
			self::$sql = new mysqli();
			self::db_init();
		}

		/**
		 *  Move database connection to separate method
		 *  to avoid connection timeouts
		 *  BUG #101
		 */
		private static function db_init()
		{
			self::$sql->init();
			self::$sql->options(MYSQLI_OPT_CONNECT_TIMEOUT, 30);
			if (!self::$sql->real_connect(APNSCP_HOST, APNSCP_USER, APNSCP_PASSWORD, APNSCP_DATABASE))
				die("Unable to connect to database");
		}

		public static function find_jobs()
		{
			self::init();
			$q = self::$sql->query(
				"SELECT
            	    site_id,
                	replace(db_name,'\\\\','') as db_name,
                	db_type,
                	day_span,
                	email,
                	extension,
                	preserve,
                	email IS NULL AS do_email,
                	DATE_ADD(CONVERT(NOW(),DATE), INTERVAL day_span DAY) as new_date
                FROM
                	sql_dumps
                WHERE
                    (
                        NOW() >= next_date
                    ) or 1 = 1
                	AND
                	next_date IS NOT NULL
                ORDER BY
                	site_id, email, db_name"
			);
			if ($q->num_rows < 1) return;
			$prev_id = 0;
			$jobs = array();
			$email = null;
			$mysqlver = self::$sql->server_version;

			while (null !== ($row = $q->fetch_object())) {
				$fsroot = FILESYSTEM_VIRTBASE . '/site' . $row->site_id;
				$dbfsname = $row->db_name;

				if ($row->db_type === 'mysql' && $mysqlver >= 50100) {
					$dbfsname = str_replace("-", "@002d", $dbfsname);
				}

				if (!file_exists($fsroot) || file_exists($fsroot . '/info/disabled')) {
					debug("Skipping %(db)s on %(site)s - suspended state", [
						'db' => $row->db_name,
						'site' => "site". $row->site_id
					]);
					continue;
				} else if ($row->db_type == 'mysql' && !file_exists(Mysql_Module::MYSQL_DATADIR . '/' . $dbfsname)) {
					debug("Skipping %(db)s on %(site)s - missing from %(datadir)s", [
						'db'   => $row->db_name,
						'site' => "site" . $row->site_id,
						'datadir' => Mysql_Module::MYSQL_DATADIR
					]);
					continue;
				} else if (!self::db_exists($row->db_type, $row->db_name)) {
					// DB missing, but task still exists
					continue;
				}
				$ret = self::process_backup($row->db_type, $row->site_id, $row->db_name, $row->extension);
				if (!is_int($ret)) {
					// failure, we need to know about it...
					Mail::send(Crm_Module::COPY_ADMIN, ($row->db_type == 'mysql' ? 'MySQL' : 'PostgreSQL') . ' Backup Failure- ' . $row->db_name,
						"Failure detected on database " . $row->db_name . "\n" .
						"Output as follows:\n" . $ret,
						'From: ' . Crm_Module::FROM_NAME . ' <' . Crm_Module::FROM_ADDRESS . '>' . "\r\n" .
						'Precedence: bulk' . "\n",
						'-f' . Crm_Module::FROM_ADDRESS);
					continue;
				}
				$filename = self::generate_file_name(
					$row->site_id,
					$row->db_type,
					$row->db_name,
					$row->extension
				);
				// backup was successful, send out a notice
				$current_job = array(
					'db'    => $row->db_name,
					'email' => $row->email,
					'size'  => $ret,
					'fname' => $filename,
					'type'  => $row->db_type,
					'next'  => $row->new_date,
					'purge' => 0
				);

				$purge_status = 0;

				// delete old jobs

				if (!is_null($row->preserve) && $row->preserve >= 0) {
					$purge_status = self::purge_backups(
						$row->site_id,
						$row->db_type,
						$row->db_name,
						$row->preserve
					);
				}
				$current_job['purge'] = $purge_status;

				// BUG #101
				// Unpredictable timeouts requires close and reconnect
				self::$sql->ping();
				$query = "UPDATE 
	                		sql_dumps 
	                	SET 
	                		next_date = '" . $row->new_date . "' 
	                	WHERE 
	                		db_type = '" . $row->db_type . "' 
	                		AND 
	                		db_name = '" . $row->db_name . "'";
				if (!is_debug()) {
					self::$sql->query($query);
				}
				if ($prev_id == $row->site_id) {
					/**
					 * Backup task includes one or more backups with an e-mail
					 */
					$jobs[] = $current_job;
				} else if ($prev_id) {
					if (!empty($jobs) && $email) self::flush_jobs($prev_id, $jobs, $email);
					$email = null;
					$jobs = array($current_job);
				} else {
					$jobs[] = $current_job;
				}
				if (!$email && $row->email) $email = $row->email;
				$prev_id = $row->site_id;
			}
			if (!empty($jobs) && $email) self::flush_jobs($prev_id, $jobs, $email);

		}

		private static function purge_backups($mSite, $mType, $mName, $mPreserve)
		{
			$path = FILESYSTEM_VIRTBASE . '/site' . $mSite . '/fst/home/' . self::get_username($mSite) . '/' . $mType . '_backups';
			$snapshot = glob( $path . '/' . $mName . '-20*-*-snapshot.sql');
			$delete = (time() - 86400*5);
			// autoexpire snapshots first
			for ($i = 0, $n = sizeof($snapshot); $i < $n; $i++) {
				if (filemtime($snapshot[$i]) < $delete) {
					unlink($snapshot[$i]);
				}
			}
			$files = glob($path . '/' . $mName . '-20*');
			$size = 0;
			$fcnt = sizeof($files);
			$max = $mPreserve + 1 /* latest dump */;
			for ($i = 0; $i < ($fcnt - $max); $i++) {
				if (!isset($files[$i])) {
					break;
				}
				if (!is_file($files[$i]) || (false !== strpos($files[$i], "-snapshot.sql"))) {
					$fcnt++;
					continue;
				}
				$size += filesize($files[$i]);
				unlink($files[$i]);
			}
			return $size;
		}

		private static function flush_jobs($mSite, $mJobs, $email = '')
		{
			$info_str = 'Type       Name                       Size          Next Backup  +/-' . "\n" .
				'=========================================================================' . "\n";
			foreach ($mJobs as $job) {
				$net_change = ($job['size'] - $job['purge']) / 1024;
				$size = $job['size'] / 1024 / 1024;
				$type = $job['type'] == 'mysql' ? 'MySQL' : 'PostgreSQL';
				$buffer = sprintf(
					'%-10s %-26s %6.2f %-6s %-12s %+.2f KB',
					$type,
					$job['db'],
					$size,
					'MB',
					$job['next'],
					$net_change
				);

				$info_str .= $buffer . "\n";

			}
			$quota = self::get_quota($mSite);
			if ($quota[1] > 0) {
				$quotaStr = sprintf('%d MB/%d MB (%d%%)', $quota[0] / 1024, $quota[1] / 1024, $quota[0] / $quota[1] * 100);
			} else {
				$quotaStr = sprintf('%d MB', $quota[0] / 1024);
			}

			$email = self::get_email_from_site($mSite);

			$msg = "The following notice is to inform you that your database " .
				"backups for " . self::get_domain_from_site($mSite) . " have completed.\n\n" .
				$info_str . "\n\n" .
				"Storage Used: " . $quotaStr . "\n\n" .
				"Database backups are accessible under /home/" . self::get_username($mSite) . "/" . ($job['type']) . "_backups/.  Backup " .
				"properties may be changed within the control panel at http://" . self::get_domain_from_site($mSite) . "/cpadmin" . "\n\n" .
				"Apis Networks Staff";
			Mail::send($email,
				'Database Backup Report - ' . self::get_domain_from_site($mSite),
				$msg,
				'From: "' . Crm_Module::FROM_NAME . '" <' . Crm_Module::FROM_NO_REPLY_ADDRESS . '>' . "\n" .
				'Precedence: bulk' . "\n",
				'-f' . Crm_Module::FROM_NO_REPLY_ADDRESS . " -F'" . Crm_Module::FROM_NAME . "'");
			fwrite(STDERR, "Processed jobs for " . self::get_domain_from_site($mSite) . "\n");
			return true;
		}

		private static function get_quota($mSite)
		{
			$quota = \Opcenter\Filesystem\Quota::getGroup(self::get_gid($mSite));
			return [
				$quota['qused'], $quota['qhard']
			];
		}

		private static function db_exists($type, $database)
		{
			$fn = 'sql_' . $type . '_database_exists';
			return call_user_func(array(self::$afi, $fn), $database);
		}

		/**
		 * Get database size
		 *
		 * @param string $type
		 * @param string $database
		 * @return int database size in kilobytes
		 */
		private static function get_db_size($type, $database)
		{
			$size = self::$afi->sql_get_database_size($type, $database);
			return $size / 1024;
		}

		private static function generate_file_name($mSite, $mType, $mName, $mExt)
		{
			return '/home/' . self::get_username($mSite) . '/' .
			$mType . '_backups/' . $mName . '-' . date('Ymd') . '.sql' .
			($mExt == 'bz' ?
				'.bz2' :
				($mExt == 'none' ? '' : $mExt));
		}

		private static function has_minimum_space_free($used, $total, $dbsize)
		{
			if (!$total) {
				// unlimited quota
				return true;
			}
			if (($total - $used) < ($dbsize * self::PERCENTAGE_MIN)) {
				return false;
			}
			return true;
		}

		private static function process_backup($mType, $mSite, $mDB, $mExtension)
		{
			$target_dir = FILESYSTEM_VIRTBASE . '/site' . $mSite .
				'/fst/home/' . self::get_username($mSite) . '/' . $mType . '_backups/';
			if (!is_dir(dirname($target_dir))) {
				return 0;
			}

			$dbsize = self::get_db_size($mType, $mDB);
			list ($used, $total) = self::get_quota($mSite);

			if (!self::has_minimum_space_free($used, $total, $dbsize)) {
				return -1;
			}
			$extension = $pipe = '';
			if ($mExtension == 'gz') {
				$pipe = ' | gzip -';
				$extension = '.gz';
			} else if ($mExtension == 'bz') {
				$pipe = ' | bzip2 - -z';
				$extension = '.bz2';
			} else if ($mExtension == 'zip') {
				$pipe = ' | zip -q -9';
				$extension = '.zip';
			}

			if (!file_exists($target_dir)) {
				\Opcenter\Filesystem::mkdir(
					$target_dir,
					self::get_uid($mSite),
					self::get_gid($mSite),
					0700
				);
			}

			debug("Backing up `%s'", $mDB);

			$output = $target_dir . '/' . $mDB . '-' . date('Ymd') . '.sql' . $extension;
			$proc = new Util_Process();
			$args = array(
				'output'    => $output,
				'db'        => $mDB,
				'pipe'      => $pipe,
			);
			if ($mType == 'pgsql') {
				$cmd = 'pg_dump -b -x -c -f-';
			} else {
				// password comes from /root/.my.cnf
				$args['cnf'] = '/root/.my.cnf';
				$cmd = explode(' ', Mysql_Module::EXPORT_CMD, 2);
				array_splice($cmd, 1, 0, '--defaults-extra-file=%(cnf)s');
				$cmd = implode(' ', $cmd) . ' -u root';
			}
			$cmd .= ' %(db)s %(pipe)s > %(output)s';
			if (is_link($output) || file_exists($output)) {
				unlink($output);
			}
			if (false === file_put_contents($output, '', LOCK_EX)) {
				fwrite(STDERR, 'Dump failed for ' . $mDB . ' -- lock failed');
				return false;
			}
			$ret = $proc->run($cmd, $args);
			if (!$ret['success'] || !file_exists($output)) {
				fwrite(STDERR, 'Dump failed for ' . $mDB . ' -- output: ' . $output . "\n");
				return $output;
			}
			\Opcenter\Filesystem::chogp($output, self::get_uid($mSite), self::get_gid($mSite), 0600);

			return filesize($output);


		}

		private static function get_username($mSite)
		{
			if (isset(self::$admin_cache[$mSite]))
				return self::$admin_cache[$mSite];

			$adminUser = (new SiteConfiguration("site${mSite}"))->getServiceValue('siteinfo', 'admin_user');

			if (!$adminUser) {
				fwrite(STDERR, "Cannot find admin user for site" . $mSite . "\n");
				return '';
			}

			return self::$admin_cache[$mSite] = $adminUser;
		}

		private static function get_email_from_site($mSite)
		{
			$email = (new SiteConfiguration("site${mSite}"))->getServiceValue('siteinfo', 'email');
			if (!$email) {
				fwrite(STDERR, "Cannot find email for site" . $mSite . "\n");
				return '';
			}

			return $email;
		}

		private static function get_uid($mSite)
		{
			if (isset(self::$uid_cache[$mSite])) {
				return self::$uid_cache[$mSite]['uid'];
			}

			$admin = (new SiteConfiguration("site${mSite}"))->getServiceValue('siteinfo', 'admin');
			if (!$admin) {
				fwrite(STDERR, "Cannot find admin uid for site" . $mSite . "\n");
				return '';
			}

			$pwd = (new User())->getpwnam($admin);
			self::$uid_cache[$mSite] = $pwd;
			return self::$uid_cache[$mSite]['uid'];
		}

		private static function get_domain_from_site($mSite)
		{
			if (!$domain =\Auth::get_domain_from_site_id((int)$mSite)) {
				fwrite(STDERR, "Crit: cannot find domain for " . $mSite . "\n");
				exit(1);
			}

			return $domain;
		}

		private static function get_gid($mSite)
		{
			if (!isset(self::$uid_cache[$mSite]['gid'])) {
				self::get_uid($mSite);
			}

			return self::$uid_cache[$mSite]['gid'];
		}
	}

	Backup_CLI::find_jobs();
