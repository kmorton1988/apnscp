<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Tip_Engine
	{
		static public function add($name, $tip, $appid = null, $replace = false)
		{
			if (!$appid) {
				$appid = Page::get()->getApplicationID();
			}
			$name = str_replace(' ', '_', strtolower($name));
			if (!$replace && isset($_SESSION['tooltips'][$appid][$name])) {
				return true;
			}
			if (!is_array($tip)) {
				$tip = array('title' => null, 'body' => $tip);
			}
			if (!isset($_SESSION['tooltips'][$appid])) {
				$_SESSION['tooltips'][$appid] = array();
			}
			$_SESSION['tooltips'][$appid][$name] = $tip;
		}

		public static function get($name, $id)
		{
			// session expired - no tooltips available
			if (!isset($_SESSION['tooltips'])) {
				return false;
			}

			if (!$name) {
				return $_SESSION['tooltips'][$id];
			}
			if (!isset($_SESSION['tooltips'][$id][$name])) {
				return false;
			}
			if (!isset($_SESSION['tooltips'][$id][$name]['title'])) {
				$_SESSION['tooltips'][$id][$name]['title'] = '';
			}
			return $_SESSION['tooltips'][$id][$name];
		}
	}
