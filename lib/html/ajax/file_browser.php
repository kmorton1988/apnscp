<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	//
	// jQuery File Tree PHP Connector
	//
	// Version 1.01
	//
	// Cory S.N. LaViska
	// A Beautiful Site (http://abeautifulsite.net/)
	// 24 March 2008
	//
	// History:
	//
	// 1.01 - updated to work with foreign characters in directory/file names (12 April 2008)
	// 1.00 - released (24 March 2008)
	//
	// Output a list of files for jQuery File Tree
	//

	// avoid pagespeed interference
	header('Content-type: text/plain', true);

	$filter = '';
	$filters = array(
		'dir'    => true,
		'file'   => true,
		'access' => false,
		'chown'  => false,
		'show'   => array()
	);
	$DO_JSON = \HTML_Kit::jsonify();
	if (isset($_GET['filter'])) {
		$token = strtok($_GET['filter'], ';');
		while ($token !== false) {
			$token = explode(',', $token);
			if (!isset($token[1])) {
				$token[1] = true;
			}
			if ($token[0] == 'show') {
				$filters['show'][] = $token[1];
			} else {
				$filters[$token[0]] = $token[1];
			}
			$token = strtok(';');
		}
	}
	if (!isset($_POST['dir'])) {
		return false;
	}

	$function = apnscpFunctionInterceptor::init();
	$inputDir = html_entity_decode($_POST['dir']);
	$files = $function->file_get_directory_contents($inputDir);
	$translatedDir = null;
	$stat = $function->file_stat($inputDir);
	if ($stat['referent']) {
		$translatedDir = $stat['referent'];
	}
	if (isset($filters['show'])) {
		foreach ($filters['show'] as $dir) {
			if ((!$stat = $function->file_stat($dir)) || !$stat['referent']) {
				continue;
			}

			if ($stat['referent'] !== $dir) {
				// just in case
				$filters['show'][] = $stat['referent'];
			}
		}
	}
	$filtered = array();
	if ($files) {
		foreach ($files as $file) {
			if ($translatedDir && 0 === strpos($file['file_name'], $translatedDir)) {
				$file['file_name'] = $inputDir . substr($file['file_name'], \strlen($translatedDir));
			}
			if ($filters['show']) {
				// filter pattern
				// file matched forced pattern
				$force_match = false;
				// pattern matched component for component of file
				$full_match = false;

				foreach ($filters['show'] as $pattern) {
					$file_components = explode('/', $file['file_name']);
					$match_components = explode('/', $pattern);
					for ($i = 0, $szcomponents = sizeof($file_components);
						 $i < $szcomponents; $i++) {
						if (!isset($match_components[$i])) {
							break;
						}
						$force_match = fnmatch($match_components[$i], $file_components[$i]);
						if (!$force_match) {
							break;
						}
					}

					$full_match = $force_match && $i == sizeof($file_components);

					if ($force_match) {
						break;
					}
				}
				if (!$force_match) {
					continue;
				}

				if (!$full_match && $filters['chown'] && !$file['can_chown'] ||
					$filters['access'] && $file['can_descend']
				) {
					continue;
				}
			}
			if ($filters['dir'] && ($file['file_type'] == 'dir' || $file['file_type'] == 'link' && $file['link'] & 2)) {
				$dir = htmlentities(basename($file['file_name']));
				$filtered[] = '<li class="directory collapsed">' .
					'<a href="#" rel="' . ($file['file_name']) . '">' .
					$dir .
					'</a>' .
					'</li>';
			}

			if ($filters['file'] && $filter != 'dir' && $file['file_type'] == 'file') {
				$ext = substr(strrchr($file['file_name'], '.'), 1);
				$filtered[] = "<li class=\"file ext-$ext\">" .
					'<a href="#" rel="' . ($file['file_name']) . '">' .
					htmlentities(basename($file['file_name']), ENT_QUOTES) .
					'</a>' .
					'</li>';
			}
			if ($DO_JSON) {
				$listing[] = $file;
			}
		}

	}
	if ($DO_JSON) {
		print json_encode($listing);
	} else if ($filtered) {
		echo '<ul class="file-manager" style="display: none;">' .
			join("\n", $filtered) . '</ul>';
	}
