<?php

	use Opcenter\Net\IpCommon;

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Auth_Anvil
	{

		const MIN_REJECTIONS = ANVIL_MIN_ATTEMPTS;
		// minimum time to delay
		const ANVIL_MIN = 1;
		// maximum time to delay
		const ANVIL_MAX = 4;
		// wait factor for failed attempts
		const ANVIL_MULTIPLIER = 0.85;
		// 5 minutes
		const ANVIL_EXPIRY = ANVIL_TTL;
		//
		const ANVIL_REJECTION_THRESHOLD = ANVIL_LIMIT;

		const GC_PROBABILITY = 7;

		const CACHE_KEY = 'auth.anvil';

		protected static $whitelisted;

		/**
		 * Check and automatically block where appropriate
		 *
		 * @param string|null IP address to use as source
		 * @return bool
		 */
		public static function anvil($ip = null)
		{
			// @XXX
			/*if (is_debug()) {
				return true;
			}*/
			if (!$ip) {
				$ip = \Auth::client_ip();
			}
			if (self::whitelisted($ip)) {
				return true;
			}
			$blockcnt = self::get_count($ip);
			if ($blockcnt > Auth_Anvil::ANVIL_REJECTION_THRESHOLD) {
				// execution ends and let's not flood with junk
				\Error_Reporter::set_report(null);
				if (!headers_sent()) {
					// @todo custom 429 error page
					http_response_code(429);
					echo 'too many bad logins! try again later';
				}
				(new \Auth\Log(\Auth::autoload()))->log("Brute-force throttle hit %(ip)s");
				fatal('%s: too many bad logins!', $ip);
			}

			$cache = self::update_count($ip);

			if ($blockcnt > self::MIN_REJECTIONS) {
				$min = self::ANVIL_MIN + $cache['count'] * self::ANVIL_MULTIPLIER;
				$max = self::ANVIL_MAX + $cache['count'] * self::ANVIL_MULTIPLIER;
				self::block($min, $max);
			}

			if (mt_rand(0, 100) < self::GC_PROBABILITY) {
				self::_expire_cache();
			}

			return true;
		}


		protected static function whitelisted($ip): bool
		{
			if (isset(self::$whitelisted[$ip])) {
				return self::$whitelisted[$ip];
			}
			self::$whitelisted[$ip] = false;
			foreach (ANVIL_WHITELIST as $wlip) {
				if (IpCommon::is($ip, $wlip)) {
					self::$whitelisted[$ip] = true;
					return true;
				}
			}

			return false;
		}

		public static function get_count($ip)
		{
			$cache = self::_get_cache($ip);

			return $cache['count'];
		}

		private static function _get_cache($ip)
		{
			$success = false;
			$addrs = apcu_fetch(self::CACHE_KEY, $success);
			if (!$success || !isset($addrs[$ip])) {
				return [
					'count' => 0,
					'ts'    => self::_get_time()
				];
			}

			return $addrs[$ip];
		}

		private static function _get_time()
		{
			if (isset($_SERVER['REQUEST_TIME'])) {
				return $_SERVER['REQUEST_TIME'];
			}

			return time();
		}

		public static function update_count($ip)
		{
			if (self::whitelisted($ip)) {
				return true;
			}

			return self::_update_cache($ip);
		}

		/**
		 * Update cache and return latest info
		 *
		 * @param string   $ip
		 * @param int|null $increment increment
		 * @return array
		 */
		public static function _update_cache($ip, $increment = 1): array
		{
			$cache = apcu_fetch(self::CACHE_KEY);
			$time = self::_get_time();
			if (!isset($cache[$ip])) {
				$cache[$ip] = [
					'count' => 0,
					'ts'    => null
				];
			}
			$cache[$ip]['count'] += $increment;
			$cache[$ip]['ts'] = $time;
			apcu_store(self::CACHE_KEY, $cache);

			return $cache[$ip];
		}

		/**
		 * Stall a connection
		 *
		 * To prevent timing attacks, delay is randomized,
		 * ideally that puts it in range of a typical login
		 *
		 * @param int $min minimum duration to block
		 * @param int $max maximum duration to block
		 * @return bool
		 */
		public static function block($min = self::ANVIL_MIN, $max = self::ANVIL_MAX)
		{
			if ($min > $max) {
				$max = $min;
			}

			usleep(mt_rand($min, $max) * 1000000);

			return true;
		}

		private static function _expire_cache()
		{
			$addrs = (array)apcu_fetch(self::CACHE_KEY);
			$time = self::_get_time();
			$time -= self::ANVIL_EXPIRY;

			foreach ($addrs as $k => $v) {
				if ($v['ts'] < $time) {
					unset($addrs[$k]);
				}
			}

			apcu_store(self::CACHE_KEY, $addrs);

			return true;
		}

		public static function remove($ip = null)
		{
			if (null === $ip) {
				$ip = \Auth::client_ip();
			}
			$addrs = (array)apcu_fetch(self::CACHE_KEY);
			if (!isset($addrs[$ip])) {
				return true;
			}
			unset($addrs[$ip]);

			apcu_store(self::CACHE_KEY, $addrs);

			return true;

		}

		/**
		 * Decrease tick count by 1, used by redirect
		 *
		 * @param string $ip
		 * @return bool
		 */
		public static function decrement($ip = null)
		{
			if (null === $ip) {
				$ip = \Auth::client_ip();
			}
			$addrs = (array)apcu_fetch(self::CACHE_KEY);
			if (!isset($addrs[$ip])) {
				return true;
			}
			$addrs[$ip]['count']--;

			apcu_store(self::CACHE_KEY, $addrs);

			return true;
		}

		private static function _force_expire($ip)
		{
			$addrs = (array)apcu_fetch(self::CACHE_KEY);
			if (!isset($addrs[$ip])) {
				return true;
			}

			unset($addrs[$ip]);

			apcu_store(self::CACHE_KEY, $addrs);

			return true;
		}
	}