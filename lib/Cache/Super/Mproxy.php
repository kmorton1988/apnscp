<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * Class Cache_Super_Mproxy
	 *
	 * A unified cache backend
	 */
	class Cache_Super_Mproxy extends Cache_Mproxy
	{
		const REDIS_PORT = CACHE_SUPER_GLOBAL_PORT;
		protected static $connection;

		protected static function _connect(Redis $c)
		{
			$port = null === self::REDIS_PORT ? null : (int)self::REDIS_PORT;
			if (!$c->connect(CACHE_SUPER_GLOBAL_HOST, $port)) {
				warn('cache: cannot access SG server');

				return parent::_connect($c);
			}
			$c->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_PHP);
			if (CACHE_SUPER_GLOBAL_PASSWORD && !$c->auth(CACHE_SUPER_GLOBAL_PASSWORD)) {
				fatal('Failed to authenticate to super global cache provider - `%s\'', $c->getLastError());
			}

			return $c;
		}
	}