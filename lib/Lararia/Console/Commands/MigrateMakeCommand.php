<?php declare(strict_types=1);

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */

	namespace Lararia\Console\Commands;

	use Illuminate\Support\Str;

	class MigrateMakeCommand extends \Illuminate\Database\Console\Migrations\MigrateMakeCommand
	{
		protected $type = 'db';

		/**
		 * The console command description.
		 *
		 * @var string
		 */

		public function __construct()
		{
			$this->signature .= '{--platform : Create migration for platform instead.}';
			$creator = app('migration.creator');
			parent::__construct($creator, app('composer'));
		}

		/**
		 * Execute the console command.
		 *
		 * @return mixed
		 */
		public function handle()
		{
			if ($this->option('platform')) {
				$this->type = 'platform';
				$this->creator->setType('platform');
			}

			if ($this->type === 'db') {
				return parent::handle();
			}

			// clone of the parent
			// It's possible for the developer to specify the tables to modify in this
			// schema operation. The developer may also specify if this table needs
			// to be freshly created so we can create the appropriate migrations.
			$name = Str::snake(trim($this->input->getArgument('name')));
			// Now we are ready to write the migration out to disk. Once we've written
			// the migration out, we will dump-autoload for the entire framework to
			// make sure that the migrations are registered by the class loaders.
			$this->writeMigrationPlay($name);
		}

		/**
		 * Write the migration file to disk.
		 *
		 * @param  string $name
		 * @return string
		 */
		protected function writeMigrationPlay($name)
		{
			return $this->writeMigration($name, null, null);
		}

		/**
		 * Get migration path (either specified by '--path' option or default location).
		 *
		 * @return string
		 */

		protected function getMigrationPath()
		{
			if ($this->type === 'db') {
				return parent::getMigrationPath();
			}

			if (!\is_null($targetPath = $this->input->getOption('path'))) {
				return $this->laravel->basePath() . '/' . $targetPath;
			}

			return resource_path('playbooks/migrations');
		}
	}