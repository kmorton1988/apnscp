<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2020
	 */

	namespace Lararia\Mail;

	use Illuminate\Mail\Mailable;
	use Illuminate\Queue\SerializesModels;

	class Simple extends Mailable
	{
		use SerializesModels;
		use \ContextableTrait;
		use \apnscpFunctionInterceptorTrait;

		protected $args;
		/**
		 * @var string
		 */
		protected $method = 'view';

		public $subject = PANEL_BRAND . ' message';

		/**
		 * Create a new message instance.
		 *
		 * @param array $args
		 */
		public function __construct(string $view, array $args = [])
		{
			$this->view = $view;
			$this->args = $args;
		}

		public function asMarkdown(): self
		{
			$this->method = 'markdown';

			return $this;
		}

		public function asView(): self
		{
			$this->method = 'view';

			return $this;
		}

		public function args(array $args): self {
			$this->args = array_replace($this->args, $args);

			return $this;
		}

		public function setSubject(string $subject): self
		{
			$this->subject = $subject;
			return $this;
		}

		public function build()
		{
			if ($this->method === 'view') {
				$handler = app('view');
				if (!\array_key_exists('mail', $handler->getFinder()->getHints())) {
					$handler->addNamespace('mail', resource_path('views/email/html'));
					$handler->addNamespace('mail', config_path('custom/resources/views/email/html'));
				}


			}

			$mail = $this->{$this->method}($this->view, $this->args)->
				subject(_($this->subject));

			if ($this->inContext()) {
				$mail->to($this->getApnscpFunctionInterceptor()->common_get_email());
			}

			return $mail;
		}
	}