<?php

	namespace Lararia\Providers;

	use Illuminate\Queue\Events\JobProcessed;
	use Illuminate\Support\Facades\Queue;
	use Illuminate\Support\ServiceProvider;
	use Lararia\Database\Migrations\Creator;
	use Lararia\Database\Migrations\DatabaseMigrationRepository;
	use Lararia\Database\Migrations\PlatformMigrator;

	class AppServiceProvider extends ServiceProvider
	{
		public $defer = true;

		/**
		 * Register any application services.
		 *
		 * @return void
		 */
		public function register()
		{
			$this->app->singleton('platformmigrator', static function ($app) {
				$repository = $app['migration.repository'];

				return new PlatformMigrator($repository, $app['db'], $app['files']);
			});

			$this->app->extend('migration.repository', static function ($command, $app) {
				$table = $app['config']['database.migrations'];

				return new DatabaseMigrationRepository($app['db'], $table);
			});

			$this->app->extend('migration.creator', static function ($command, $app) {

				return new Creator($app['files']);
			});

			foreach (\Module\Support\Webapps\PathManager::applicationViewPaths() as $appName => $paths) {
				$this->app->get('view')->addNamespace("@webapp(${appName})", $paths);
			}
		}

		public function boot() {
			// force Preferences cache cleanup resolves lingering cache
			// sample BT of lingering cache:
			/**
			 *  4. apnscpFunctionInterceptor->__call("common_save_preferences", [[_ts:1598327427.5719, [ArrayArray])
			 * [/usr/local/apnscp/lib/Preferences.php:299]
			 * 5. Preferences->sync()
			 * [/usr/local/apnscp/lib/Preferences.php:237]
			 * 6. Preferences->__destruct()
			 * [/usr/local/apnscp/lib/Module/Support/Webapps.php:581]
			 * 7. Module\Support\Webapps->getOptions("/var/www/html")
			 * [/usr/local/apnscp/lib/Module/Support/Webapps.php:748]
			 * 8. Module\Support\Webapps->getVersionLock("/var/www/html")
			 * [/usr/local/apnscp/lib/Module/Support/Webapps/App/Type/Wordpress/module.php:924]
			 * 9. Wordpress_Module->update_themes("example.com", "")
			 * [/usr/local/apnscp/lib/Module/Support/Webapps/App/Type/Wordpress/module.php:836]
			 * 10. Wordpress_Module->update_all("example.com", "", "5.5")
			 */
			Queue::after(function (JobProcessed $event) {
				\Preferences::resetCaches();
			});
		}

		/**
		 * Get the services provided by the provider.
		 *
		 * @return array
		 */
		public function provides()
		{
			return [
				'migration.repository',
				'platformmigrator',
				'migration.creator'
			];
		}
	}
