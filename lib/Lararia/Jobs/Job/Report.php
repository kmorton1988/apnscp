<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Jobs\Job;

	use Illuminate\Contracts\Mail\Mailable;
	use Lararia\Jobs\Job;

	class Report implements \Iterator, \ArrayAccess
	{
		/**
		 * @var Intern[]
		 */
		protected $jobs;
		/**
		 * @var int
		 */
		protected $status = \Error_Reporter::E_OK;
		/**
		 * @var Mailable
		 */
		protected $template;

		public function __construct()
		{
		}

		public function add(Intern $intern): Report
		{
			$this->jobs[] = $intern;
			if (method_exists($intern->getJob(), 'getStatus')) {
				$this->status = max($this->status, $intern->getJob()->getStatus());
			}
			$template = $intern->getJob()->getMailableClass();
			if (null === $this->template) {
				$this->template = $template;
			} else if ($this->template !== $template) {
				warn("job report requested template `%s', but template `%s' already set for batch - check group tag",
					$template, $this->template);
			}

			return $this;
		}

		public function getTemplate()
		{
			return $this->template;
		}

		public function getView()
		{
			if (!$this->jobs) {
				return null;
			}

			return $this->jobs[0]->getJob()->getView();
		}

		public function success(): bool
		{
			return $this->status < \Error_Reporter::E_ERROR;
		}

		public function current(): ?Job
		{
			return ($tmp = current($this->jobs)) ? $tmp->getJob() : null;
		}

		public function last(): ?Job
		{
			$job = end($this->jobs);
			reset($this->jobs);
			return $job ? $job->getJob() : null;
		}

		public function next()
		{
			return next($this->jobs);
		}

		public function key()
		{
			return key($this->jobs);
		}

		public function valid()
		{
			return current($this->jobs) !== false;
		}

		public function rewind()
		{
			reset($this->jobs);
		}

		public function offsetExists($offset)
		{
			return \array_key_exists($offset, $this->jobs);
		}

		public function offsetGet($offset): Job
		{
			return $this->jobs[$offset]->getJob();
		}

		public function offsetSet($offset, $value)
		{
			fatal('call %s::add()', \get_class($this));
			//$this->jobs[$offset] = $value;
		}

		public function offsetUnset($offset)
		{
			unset($this->jobs[$offset]);
		}


	}