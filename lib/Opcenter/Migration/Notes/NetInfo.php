<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
 */

namespace Opcenter\Migration\Notes;

use Opcenter\Migration\Note;
use Opcenter\Net\Ip4;
use Opcenter\Net\Ip6;

class NetInfo extends Note {

	private $ip;
	private $ip6;
	private $hostname;

	public function __construct(string $note)
	{
		parent::__construct($note);

		if (!Ip4::valid($note) && !Ip6::valid($note)) {
			fatal('Unknown IPv4/IPv6 address %s', $note);
		}
		$this->ip = $note;
	}

	public function setHostname(string $val): void
	{
		$this->hostname = $val;
	}

	public function getHostname(): ?string
	{
		if (!$this->hostname) {
			warn("No hostname set for `%s'", $this->ip);
		}
		return $this->hostname;
	}

	/**
	 * Set IPv4 associated with account
	 *
	 * @param string $ip
	 * @return bool
	 */
	public function setIp(string $ip): bool
	{
		if (!Ip4::valid($ip)) {
			fatal("`%s' is not valid IPv4", $ip);
		}
		$this->ip = $ip;
	}

	/**
	 * Set IPv6 associated with account
	 *
	 * @param string $ip
	 * @return bool
	 */
	public function setIp6(string $ip): bool
	{
		if (!Ip6::valid($ip)) {
			fatal("`%s' is not valid IPv6", $ip);
		}
		$this->ip6 = $ip;
	}

	/**
	 * Get IPv4
	 *
	 * @return string|null
	 */
	public function getIp(): ?string
	{
		return $this->ip;
	}

	/**
	 * Get IPv6
	 *
	 * @return string|null
	 */
	public function getIp6(): ?string
	{
		return $this->ip6;
	}

}