<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
 */

namespace Opcenter\Migration\Formats\Contracts;

use Opcenter\Migration\Import;

interface ConsistencyValidationInterface {
	/**
	 * Task passes preflight checks
	 *
	 * @param Import $task
	 * @param array  $ctor optional constructor arguments
	 * @return bool
	 */
	public static function check(Import $task, array $ctor = []): bool;
}