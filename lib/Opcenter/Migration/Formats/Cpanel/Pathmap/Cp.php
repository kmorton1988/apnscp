<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
	 */

	namespace Opcenter\Migration\Formats\Cpanel\Pathmap;

	use Opcenter\Migration\Bill;
	use Opcenter\Migration\Contracts\BackupStreamInterface;
	use Opcenter\Migration\Formats\Cpanel;
	use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;
	use Opcenter\Migration\Import;
	use Opcenter\Migration\Notes\MailTransports;
	use Opcenter\Migration\Notes\NetInfo;
	use Opcenter\Migration\Notes\User;
	use Opcenter\Service\ConfigurationContext;
	use Opcenter\Service\Plans;
	use Opcenter\SiteConfiguration;

	class Cp extends ConfigurationBuilder
	{
		const OPC_MAP = [
			'DNS'          => 'siteinfo.domain',
			'CONTACTEMAIL' => 'siteinfo.email',
			'HASSHELL'     => 'ssh.enabled',
		];
		const DEPENDENCY_MAP = [
			Version::class
		];
		protected $domain;
		protected $username;

		protected $bill;

		/**
		 * @var array parsed data
		 */
		protected $data;

		public function __construct(Import $task, BackupStreamInterface $archiveStream)
		{
			parent::__construct($task, $archiveStream);
			$this->data = array_change_key_case($this->readFromIni(), CASE_UPPER);
			$this->bill = new Bill($this->data['DNS'], $task->getPlan());
		}

		/**
		 * Get migration bill
		 *
		 * @return Bill
		 */
		public function getBill(): Bill
		{
			return $this->bill;
		}

		/**
		 * Create migration bill
		 *
		 * @param Import        $task
		 * @param BackupStreamInterface $archiveStream
		 * @return null|Bill
		 */
		public static function createBill(Import $task, BackupStreamInterface $archiveStream): ?Bill
		{
			$c = new static($task, $archiveStream);
			if (!$c->parse()) {
				return null;
			}
			$task->setMigrationJob($c->getBill());
			return $c->getBill();
		}

		/**
		 * @inheritDoc
		 */
		public function parse(): bool
		{
			$this->bill->addNote((new MailTransports('')));
			foreach ($this->data as $key => $val) {
				if (isset(static::OPC_MAP[$key])) {
					$this->bill->set(...explode('.', static::OPC_MAP[$key], 2) + [9999 => \Util_Conf::inferType($val) ?? '']);
					continue;
				}
				$method = camel_case('extract-' . strtolower($key));
				if (0 === strncmp($key, 'MXCHECK-', 8)) {
					$domain = strtolower(substr($key, 8));
					switch ((string)$val) {
						case 'secondary':
							warn('Secondary MX not supported in %s. Disabling mail for domain %s', MISC_PANEL_BRAND, $domain);
						case 'remote':
							$this->bill->getNote(MailTransports::class)->revokeTransport($domain);
							warn('Domain MX reported as remote. Disabling mail for domain %s', $domain);
							break;
						case '0':
							// handles mail
							break;
						default:
							warn('Unknown MXCHECK directive detected: %s=%s. Treating domain as authoritative for mail', $key, $val);
					}

				}
				if (!method_exists($this, $method)) {
					continue;
				}
				$this->{$method}($val);
			}

			return true;
		}

		protected function extractUser($val): void
		{
			$note = new User($val);
			$note->setNewUser(array_get($this->task->getConfigurationOverrides(), 'siteinfo.admin_user', $val));
			$this->bill->addNote($note);
			$this->bill->set('siteinfo', 'admin_user', $val);
		}

		protected function extractPlan($val): void
		{
			if (!Plans::exists($val)) {
				warn("PLAN name `%s' does not exist as plan, ignoring", $val);
				return;
			}
			$this->bill->set('siteinfo', 'plan', $val);

		}

		/**
		 * Extract "customer since" field
		 * @param $val
		 * @return void
		 */
		protected function extractStartdate($val): void {
			$cfgctx = new ConfigurationContext('billing', new SiteConfiguration(null));
			if ($cfgctx->getValidatorClass('ctime')) {
				$this->bill->set('billing', 'ctime', (int)$val);
			}
		}

		/**
		 * Extract hosting IP address
		 *
		 * @param $val IP address
		 */
		protected function extractIp($val): void {
			$this->bill->addNote(new NetInfo((string)$val));
		}

		protected function extractDemo($val) {
			if ($val) {
				$this->bill->set('billing', 'invoice', BILLING_DEMO_INVOICE);
			}
		}

		protected function extractBwlimit(string $val) {
			$bwlimit = Cpanel::quantityConverter($val);
			$this->bill->set('bandwidth', 'threshold', $bwlimit);
			$this->bill->set('bandwidth', 'units', 'B');
		}

		protected function extractContactemail2(string $val) {
			if (!$val) {
				return;
			}
			if (platform_is('7.5')) {
				return warn('Multiple contact emails are not supported on v7.5 platforms');
			}
			warn("Multiple contact emails are not supported yet, ignoring `%s'", $val);
		}

		protected function extractMaxsql(string $val)
		{
			$limit = Cpanel::quantityConverter($val);
			$this->bill->set('mysql', 'dbasenum', $limit);
		}

		protected function extractMaxaddon(string $val) {
			if (null !== ($limit = Cpanel::quantityConverter($val))) {
				$limit = max((int)$this->bill->get('aliases', 'max', 1), $limit);
			}
			$this->bill->set('aliases', 'max', $limit);
		}

		protected function extractMaxpark(string $val) {
			return $this->extractMaxaddon($val);
		}

		protected function extractMaxlst(string $val)
		{
			$cfgctx = new ConfigurationContext('mlist', new SiteConfiguration(null));
			if ($cfgctx->getValidatorClass('max')) {
				$limit = Cpanel::quantityConverter($val);
				$this->bill->set('mlist', 'max', $limit);
			}
		}

		protected function extractMaxsub(string $val)
		{
			$cfgctx = new ConfigurationContext('apache', new SiteConfiguration(null));
			if ($cfgctx->getValidatorClass('subnum')) {
				// apnscp v3.1+
				$limit = Cpanel::quantityConverter($val);
				$this->bill->set('apache', 'subnum', $limit);
			}
		}

		protected function extractDiskBlockLimit($val) {
			$limit = Cpanel::quantityConverter($val);
			if ($limit) {
				$limit = \Formatter::changeBytes($limit, 'MB', 'KB');
			}
			$this->bill->set('diskquota', 'quota', $limit);
			$this->bill->set('diskquota', 'units', 'MB');
		}
	}

