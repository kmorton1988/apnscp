<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
	 */

	namespace Opcenter\Migration\Formats\Cpanel\Pathmap;

	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Account\Import;
	use Opcenter\Auth\Password;
	use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;
	use Opcenter\Migration\Notes\MailMan;
	use Opcenter\SiteConfiguration;

	class Mm extends ConfigurationBuilder
	{
		const PERMUTATIONS = [
			'owner-%1$s@%2$s',
			'%1$s-admin@%2$s',
			'%1$s-bounces@%2$s',
			'%1$s-confirm@%2$s',
			'%1$s-join@%2$s',
			'%1$s-leave@%2$s',
			'%1$s-owner@%2$s',
			'%1$s-request@%2$s',
			'%1$s-subscribe@%2$s',
			'%1$s-unsubscribe@%2$s',
			'%1$s@%2$s',
		];
		public function parse(): bool
		{
			$ignored = [];
			foreach ($this->getDirectoryEnumerator() as $fileObj) {
				// @var $fileObj \SplFileObject
				$path = $fileObj->getPathname();
				if (!is_dir($path)) {
					continue;
				}
				$file = basename($path);
				if (false === strpos($file, '_')) {
					warn('Invalid/corrupted list discovered in backup: %s - ignoring', $file);
					continue;
				}
				$name = basename($file);
				[$listName, $domain] = [
					substr($name, 0, $pos = strrpos($name, '_')),
					substr($name, ++$pos)
				];
				$ignored = array_merge($ignored, append_config($this->permute($listName, $domain)));
			}

			$this->bill->addNote((new MailMan('dummy field'))->setLists($ignored));

			Cardinal::register([Import::HOOK_ID, Events::SUCCESS],
				function ($event, SiteConfiguration $s) {
					foreach (new \DirectoryIterator($this->getDirectoryEnumerator()->getPath()) as $fileObj) {
						$base = $fileObj->getFilename();
						if ($base === '..' || $base === '.') {
							continue;
						}
						$path = $fileObj->getPathname();
						$this->processList($s, $path);
					}
				}
			);

			return true;

		}

		/**
		 * Process mailman list from directory
		 *
		 * @param SiteConfiguration $s
		 * @param string            $path list base directory
		 * @return bool
		 */
		private function processList(SiteConfiguration $s, string $path): bool
		{
			$base = basename($path);
			if (!is_dir($path) || false === ($first = strpos($base, '_'))) {
				return error('Skipping garbage mailing list? %s', $base);
			}

			// laid out as <name>_<domain>
			$list = substr($base, 0, $first);
			$domain = substr($base, strrpos($base, '_') + 1);
			if (!$this->bill->getNote(MailMan::class)->in("${list}@${domain}")) {
				return warn('List configuration found named %s but no corresponding address found during scan', $list);
			}
			if (!$data = $this->extractPack($path . '/config.pck')) {
				return error('failed to process config.pck in %s', $path);
			}

			if (!empty($data['moderator'])) {
				warn('Moderation detected in %s. Requires manual setup in Mail > Mailing List', $base);
			}
			$password = Password::generate(8);
			if (array_get($data, 'password')) {
				warn('List password for %(list)s generated to %(password)s. Password formats are nonconvertible.',
					['list' => $base, 'password' => $password]);
			}
			$vars = [
				'administrivia' => array_get($data, 'administrivia', true),
				'admin_passwd' => $password,
				'description'  => array_get($data, 'description', ''),
				'info'         => array_get($data, 'info', ''),
				'subject_prefix' => array_get($data, 'subject_prefix', ''),
				'welcome'      => array_get($data, 'send_welcome_msg', ''),
				'maxlength'    => (int)(array_get($data, 'max_message_size', 10000) * 1000),
				'subscribe_policy' => ($tmp = array_get($data, 'subscribe_policy')) == '1' ? 'auto+confirm' : (
						$tmp == '2' ? 'closed+confirm' : 'open+confirm'),
				'message_fronter' => array_get($data, 'msg_header', ''),
				'message_footer'  => array_get($data, 'msg_footer', ''),
			];

			$members = array_keys($data['members']);
			$afi = $s->getSiteFunctionInterceptor();
			if ($afi->majordomo_mailing_list_exists($list)) {
				// @todo remove
				debug('Overwriting conflicting list %s', $list);
				$afi->majordomo_delete_mailing_list($list);
			}
			$adminEmail = array_get($data, 'moderator.0', $afi->common_get_admin_email());

			$config = $afi->majordomo_change_configuration_options($vars);

			if (!$afi->majordomo_create_mailing_list($list, $password, $adminEmail, $domain) ||
				!$afi->majordomo_set_mailing_list_users($list, $members) ||
				!$afi->majordomo_save_configuration_options($list, $config))
			{
				return error("Failed to set mailing list for `%s'", $list);
			}

			return true;
		}

		/**
		 * Extract Mailman pickle
		 *
		 * @param string $path
		 * @return array|null
		 */
		private function extractPack(string $path): ?array
		{
			if (!is_file($path)) {
				return null;
			}
			$binPath = resource_path('storehouse/migration/mailman-unpacker.py');
			$ret = \Util_Process_Safe::exec('python %s %s', [$binPath, $path]);
			if (!$ret['success']) {
				warn('failed to decode %s', basename($path));
				return null;
			}
			if (null === ($data = json_decode($ret['stdout'], true))) {
				error('Failed to decode pack');
				return null;
			}

			return $data;
		}

		private function permute(string $name, string $domain): array {
			$addresses = [];

			foreach (static::PERMUTATIONS as $address) {
				$addresses[] = sprintf($address, $name, $domain);
			}

			return $addresses;
		}

	}

