<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
 */

namespace Opcenter\Migration\Brokers\Command;

use Opcenter\Migration\Brokers\Command;

class Local extends Command {

	/**
	 * @inheritDoc
	 */
	public function run(string $command, ...$args): array {
		$proc = new \Util_Process_Safe();
		// nice perk to always using sh, no need to worry about wrapping
		return $proc->run($command, ...$args);
	}
}
