<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
 */

namespace Opcenter\Migration;

class Detection {

	protected $task;

	public function __construct(BaseMigration $task)
	{
		$this->task = $task;
	}

	/**
	 * Return first matching format from list
	 *
	 * @param array $formats
	 * @return string|null
	 */
	public function match(array $formats): ?string
	{
		foreach ($formats as $type) {
			$class = Format::classFrom($type);
			if ((new $class($this->task))->valid()) {
				return $type;
			}
		}
		return null;
	}
}
