<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
 */

namespace Opcenter\Migration;

use Opcenter\Filesystem;
use Opcenter\Migration\ConfigurationOverrides;

/**
 * Class Generalized
 *
 * Generic migration helper
 *
 * @package Opcenter\Migration
 */
abstract class BaseMigration {
	use \NamespaceUtilitiesTrait;

	/**
	 * @var string migration task format
	 */
	protected $format;

	/**
	 * @var array creation overrides
	 */
	protected $cfgOverrides;

	/**
	 * @var string base plan
	 */
	protected $plan = OPCENTER_DEFAULT_PLAN;

	/**
	 * Task options
	 *
	 * @var array
	 */
	protected $opts = [];

	/**
	 * Task passes integrity/security checks
	 *
	 * @return bool
	 */
	public function verify(): bool
	{
		return true;
	}

	/**
	 * Get configuration overrides
	 *
	 * @return array
	 */
	public function getConfigurationOverrides(): ConfigurationOverrides
	{
		return $this->cfgOverrides ?? new ConfigurationOverrides();
	}

	/**
	 * Get base level plan
	 *
	 * @return string
	 */
	public function getPlan(): string
	{
		return $this->plan;
	}

	/**
	 * Get option setting
	 *
	 * @param string $name
	 * @param mixed  $default
	 * @return bool|null|string
	 */
	public function getOption(string $name, $default = null)
	{
		return $this->opts[$name] ?? $default;
	}

	/**
	 * No activation
	 *
	 * @return bool
	 */
	public function isDryRun(): bool
	{
		return !$this->getOption('activate');
	}

	/**
	 * Get supported formats
	 *
	 * @return array
	 */
	public static function getFormats(): array
	{
		$targetNamespace = static::appendNamespace('Contracts\\' . static::getBaseClassName()  . 'Interface');
		$formats = array_filter(Filesystem::readdir(__DIR__ . '/Formats', static function ($f) use ($targetNamespace) {
			if ('.php' !== substr($f, -4)) {
				return false;
			}
			$name = basename($f, '.php');
			try {
				$cls = new \ReflectionClass(static::appendNamespace('Formats\\' . $name));
			} catch (\ReflectionException $e) {
				warn('Garbage file detected in %s/Formats/%s', __DIR__, $f);
				return false;
			}
			if ($cls->implementsInterface($targetNamespace)) {
				return strtolower($name);
			}
		}));
		return $formats;
	}

	public function commit(): bool
	{
		return false;
	}
}