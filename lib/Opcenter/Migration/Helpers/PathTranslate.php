<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
 */

namespace Opcenter\Migration\Helpers;

use Error_Reporter;
use Opcenter\Migration\Brokers\Command\Local;

class PathTranslate {
	/**
	 * @var string log path for changes
	 */
	protected $logPath;
	/**
	 * @var Command $handler
	 */
	private $handler;

	public function __construct()
	{
		$this->handler = new Local();
	}

	/**
	 * Set log path
	 *
	 * @param string $path
	 */
	public function setLogPath(string $path): void {
		$this->logPath = $path;
	}

	/**
	 * Substitute paths
	 *
	 * @param string $oldbase
	 * @param string $newbase
	 * @param array|string  $searchPath
	 * @return bool
	 */
	public function translate(string $oldbase, string $newbase, $searchPath = null): bool
	{
		/**
		 * @XXX potential complication - .htaccess AuthUserFile references that require a non-jailed
		 *      path can be mistakenly updated in going from non-jailed to jailed. Always be sure
		 *      to do separate passhtru as needed
		 *
		 *      symlinks on $searchPath are not followed
		 */
		$errors = Error_Reporter::flush_buffer();
		$searchPath = $searchPath ?? $newbase;
		$logger = $this->logPath ? ' -fprint %(log)s' : null;
		// @todo limit on paths?
		$paths = implode(' ', array_map('escapeshellarg', $this->filterDuplicateDirectories((array)$searchPath)));
		$ret = $this->handler->run('find ' . $paths . ' -xdev -type f -exec grep -I -m1 -q %(old)s "{}" \; ' . $logger .' -print0 | xargs -0 ' .
			'perl -n -p -i -e \'use re "eval";\' ' .
			'-e \'$oldsite="%(old)s"; $newsite="%(new)s";\' ' .
			'-e \'$len = length($newsite)-length($oldsite) ; ' .
			's!s:(\d+)(?{$l=$^N+$len}):"$oldsite!s:$l:"$newsite!g ; ' .
			's!$oldsite!$newsite!g\'',
			['new' => $newbase, 'old' => $oldbase, 'search' => $searchPath, 'log' => $this->logPath]
		);

		$this->handleFindNoSuchDirectoryErrors($errors);
		if (!$ret['success']) {
			return error('Translate path failed: %s', $ret['stderr']);
		}

		return true;
	}

	/**
	 * Filter duplicated child directories
	 *
	 * @param array $paths
	 * @return array
	 */
	protected function filterDuplicateDirectories(array $paths): array
	{
		$paths = array_unique($paths);

		return array_filter($paths, static function ($v) use ($paths) {
			foreach ($paths as $p) {
				if (0 === strpos($v, $p . '/')) {
					return false;
				}
			}
			debug("Filtering path change in `%s'", $p);
			return true;
		});
	}

	private function handleFindNoSuchDirectoryErrors(array $buffer)
	{
		$newerrors = Error_Reporter::flush_buffer();
		$tmp = array();
		foreach ($newerrors as $e) {
			if (false !== strpos($e['message'], 'No such file or directory')) {
				continue;
			}
			$tmp[] = $e;
		}
		$buffer = array_merge($buffer, $tmp);
		if (!$tmp) {
			return true;
		}
		Error_Reporter::set_buffer($buffer);

		return false;
	}
}