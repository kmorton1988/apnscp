<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */


	namespace Opcenter\Database\PostgreSQL\v906;

	class Query extends \Opcenter\Database\PostgreSQL\Generic\Query
	{
		public function createUser($user, $password): string
		{
			// NOCREATEUSER no longer present as of 9.6
			return 'CREATE ROLE "' . $user . '" WITH NOCREATEDB NOCREATEROLE ' .
				"LOGIN NOINHERIT UNENCRYPTED PASSWORD '" . $password . "';";
		}
	}