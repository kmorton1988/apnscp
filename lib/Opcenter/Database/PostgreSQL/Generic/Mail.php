<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Opcenter\Database\PostgreSQL\Generic;

	class Mail
	{
		public function saveGid(int $site_id, int $gid)
		{
			return "INSERT INTO gids (site_id, gid) VALUES($site_id, $gid)";
		}

		public function saveUid(int $site_id, int $uid, string $name): string
		{
			return "INSERT INTO uids(site_id, uid, \"user\") VALUES(${site_id}, ${uid}, '" . pg_escape_string($name) . "')";
		}

		public function deleteUid(int $site_id, int $uid): string
		{
			return "DELETE FROM uids WHERE site_id = ${site_id} AND uid = ${uid}";
		}

		public function deleteGid(int $site_id): string
		{
			return "DELETE FROM gids WHERE site_id = ${site_id}";
		}

		public function renameDomain(string $old, string $new): string
		{
			return "UPDATE domain_lookup SET domain = '" . pg_escape_string($new) .
				"' WHERE domain = '" . pg_escape_string($old) . "'";
		}

		public function renameUser(string $old, string $new, int $uid): string
		{
			return 'UPDATE "uids" SET "user" = \'' . pg_escape_string($new) . '\' ' .
				'WHERE "user" = \'' . pg_escape_string($old) . '\' AND uid = ' . $uid;
		}


	}