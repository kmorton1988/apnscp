<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, September 2019
 */

namespace Opcenter\Crypto\Letsencrypt\Solvers;

use AcmePhp\Core\Http\Base64SafeEncoder;
use function debug_ex as debug;

class Dns extends Base {
	protected $afi;

	public function canUse(): bool
	{
		return $this->getApnscpFunctionInterceptor()->dns_configured();
	}

	public function resolve(): bool
	{
		$afi = $this->getApnscpFunctionInterceptor();
		$subdomain = '_acme-challenge';
		$domain = $this->authorization->getDomain();

		if (!$afi->dns_zone_exists($domain)) {
			$isSite = $this->getAuthContext()->level & (PRIVILEGE_SITE | PRIVILEGE_USER);
			if (!$isSite || !($split = $afi->web_split_host(substr($domain, strpos($domain, '.')+1)))) {
				debug('Zone %s does not exist in DNS or DNS not configured for account - skipping', $this->authorization->getDomain());
				return false;
			}

			$subdomain = implode('.', array_filter([$subdomain, $split['subdomain'], substr($domain, 0, strpos($domain, '.'))]));
			$domain = $split['domain'];
		}
		// remove if exists, keep record at end for user to reuse elsewhere
		if ($afi->dns_record_exists($domain, $subdomain, 'TXT')) {
			$afi->dns_remove_record($domain, $subdomain, 'TXT', '');
		}

		$payload = (new Base64SafeEncoder)->encode(hash('sha256', $this->payload, true));

		debug('Setting DNS TXT record %s.%s with value %s', $subdomain, $domain, $payload);
		if (!$afi->dns_add_record($domain, $subdomain, 'TXT', $payload, 120)) {
			return false;
		}
		$myns = (array)$afi->dns_get_hosting_nameservers($domain);
		$hostname = "${subdomain}.${domain}";

		$ok = false;
		for ($i = 0; $i < 10; usleep(500000), $i++) {
			// initial async record wait
			if (!$afi->dns_record_exists($domain, $subdomain, 'TXT', $payload)) {
				continue;
			}
		}

		foreach ($myns as $ns) {
			// confirm external report, used to catch replication delays in CloudFlare
			$j = 0;
			do {
				$present = silence(static function () use ($ns, $hostname) {
					return (new \Net_Gethost(1000, $ns))->lookup($hostname, DNS_TXT);
				});

				if ($present === $payload) {
					$ok = true;
					continue 2;
				}
				debug(
					"DNS record `%s.%s' added asynchronously to %s - wait %d/%d",
					$subdomain,
					$domain,
					$ns,
					++$j,
					LETSENCRYPT_DNS_VALIDATION_WAIT
				);
				sleep(1);
			} while ($j < LETSENCRYPT_DNS_VALIDATION_WAIT);
			// DNS failed
		}

		if ($ok) {
			return true;
		}

		// couldn't solve all results, query nameserver settings
		if (!$afi->dns_domain_uses_nameservers($domain)) {
			debug('Cannot use DNS solver. Zone %(domain)s does not use nameservers. Expected %(expected)s, saw %(actual)s',
				[
					'domain' => $domain,
					'actual' => implode(', ', $afi->dns_get_authns_from_host($domain)),
					'expected' => implode(', ', $afi->dns_get_hosting_nameservers($domain))
				]
			);

			return false;
		}

		return warn("Failed to confirm record `%s.%s' TXT (payload: `%s') - DNS validation may fail", $subdomain, $domain, $payload);
	}

}