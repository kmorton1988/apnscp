<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, September 2019
 */

namespace Opcenter\Crypto\Letsencrypt\Solvers;

use AcmePhp\Core\Protocol\AuthorizationChallenge;

abstract class Base {
	use \apnscpFunctionInterceptorTrait;
	use \ContextableTrait;

	/**
	 * @var string
	 */
	protected $token;
	/**
	 * @var string
	 */
	protected $payload;
	/**
	 * @var AuthorizationChallenge
	 */
	protected $authorization;

	private function __construct(string $token, string $payload)
	{
		$this->setPayload($token, $payload);
	}

	/**
	 * Solver can be used successfully
	 *
	 * @return bool
	 */
	public function canUse(): bool
	{
		return false;
	}

	public function setPayload(string $token, string $payload): self
	{
		$this->token = $token;
		$this->payload = $payload;

		return $this;
	}

	/**
	 * Set authorization challenge metadata
	 *
	 * @param AuthorizationChallenge $auth
	 * @return Base
	 */
	public function setAuthorization(AuthorizationChallenge $auth): self {
		$this->authorization = $auth;
		return $this;
	}

	abstract public function resolve(): bool;
}

