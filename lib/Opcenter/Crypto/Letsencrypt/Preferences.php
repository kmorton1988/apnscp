<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, September 2019
 */

namespace Opcenter\Crypto\Letsencrypt;

class Preferences {
	const KEY_TYPE = 'ssl.key_type';
	const SENSITIVITY = 'letsencrypt.sensitivity';
	const VERIFY_IP = 'letsencrypt.verifyip';
}
