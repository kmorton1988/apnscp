<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\VirusScanner;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Malware\Clamd;

	class SignatureWhitelist implements SettingsInterface
	{
		const SIGNATURE_FILE = '/var/lib/clamav/custom-whitelist.ign2';

		public function set($val): bool
		{
			if (!ANTIVIRUS_INSTALLED) {
				return error('No anti-virus package installed');
			}

			if ($val === $this->get()) {
				// no need to set the value again
				return true;
			}

			$values = $this->get();
			$val = (array)$val;
			foreach ($val as &$v) {
				if (\in_array($v, $values, true)) {
					$v = null;
				}
				if (false !== ($pos = strpos($v, '(')) && $pos < strrpos($v, ')')) {
					warn("Removed `%s' from signature", substr($v, $pos));
					$v = substr($v, 0, $pos);
				}
			}
			unset($v);

			$values = array_merge($values, array_filter($val));

			if (false === file_put_contents(self::SIGNATURE_FILE, implode("\n", $values))) {
				return error('Failed to write to %s', self::SIGNATURE_FILE);
			}

			Clamd::reload('2 minutes');
			return info('clamd will reload in 2 minutes');
		}

		public function get()
		{
			if (!file_exists(self::SIGNATURE_FILE)) {
				return [];
			}
			return file(self::SIGNATURE_FILE, FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
		}

		public function getHelp(): string
		{
			return 'Whitelist virus signatures';
		}

		public function getValues()
		{
			return 'array';
		}

		public function getDefault()
		{
			return [];
		}

	}
