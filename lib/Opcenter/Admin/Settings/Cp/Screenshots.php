<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2020
	 */

	namespace Opcenter\Admin\Settings\Cp;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class Screenshots implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				return true;
			}

			$cfg = new Config();
			$cfg['has_screenshots'] = (bool)$val;
			unset($cfg);
			Bootstrapper::run('apnscp/bootstrap', 'software/tmpfiles');

			return true;
		}

		public function get()
		{
			$config = new Config();

			return $config['has_screenshots'] ?? !$config['has_low_memory'];
		}

		public function getHelp(): string
		{
			return 'Enable Web App screenshots';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return true;
		}
	}