<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2019
	 */

	namespace Opcenter\Admin\Settings\System;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;

	class SshdPubkeyOnly implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			if ($val !== true && $val !== false) {
				return error('Setting must be true/false');
			}

			$cfg = new Bootstrapper\Config();
			$cfg['sshd_pubkey_only'] = $val;
			$cfg->sync();
			if ($val) {
				warn('Before exiting terminal session verify you can login '.
					'using public key. You will not be able to login again '.
					'using a password!'
				);
			}
			Bootstrapper::run('system/sshd');

			return true;
		}

		public function get()
		{
			$config = new Bootstrapper\Config();
			return $config['sshd_pubkey_only'] ?? false;
		}

		public function getHelp(): string
		{
			return 'Restrict SSH access to public key';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}
	}