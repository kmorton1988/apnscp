<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */

	namespace Opcenter\Admin\Settings\System;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Apnscp;
	use Opcenter\Http\Apache;
	use Opcenter\Map;

	class Timezone implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			$ret = \Util_Process_Safe::exec('timedatectl set-timezone %s', $val);
			if (!$ret['success']) {
				return error($ret['stderr']);
			}
			if (!date_default_timezone_set($val)) {
				return error('Failed to set default timezone');
			}
			foreach ([config_path('php.ini'), '/etc/php.ini'] as $ini) {
				Map::write($ini)->section('apnscp')->set('date.timezone', $val);
			}
			Apnscp::restart();
			Apache::restart();

			return true;
		}

		public function get()
		{
			return date_default_timezone_get();
		}

		public function getHelp(): string
		{
			return 'Change system timezone';
		}

		public function getValues()
		{
			return 'Any timezone';
		}

		public function getDefault()
		{
			return date_default_timezone_get();
		}
	}