<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */

	namespace Opcenter\Admin\Settings\Cron;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Map;

	class StartRange implements SettingsInterface
	{
		public const ANACRON_CONF = Notify::ANACRON_CONF;

		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			if (\is_int($val)) {
				return error('Hours must be a range such as 3-22');
			} else if (false !== strpos((string)$val, '-')) {
				[$l, $r] = explode('-', $val);
				if ((int)$l > (int)$r) {
					return error('Hours must be increasing, 1-4 but not 4-1');
				}
				if ((int)$l < 0 || (int)$l > 23) {
					return error("Hour must be between 0 and 23 - got `%d'", $l);
				} else if ((int)$r < 0 || (int)$r > 23) {
					return error("Hour must be between 0 and 23 - got `%d'", $r);
				}
			} else {
				return error('Got non-sensical time range, must be between 0 and 23 such as 3-23');
			}
			Map::load(self::ANACRON_CONF, 'wd', 'inifile')->section(null)['START_HOURS_RANGE'] = $val;

			return \Util_Process::exec('anacron -T')['success'];
		}

		public function get()
		{
			if (!file_exists(self::ANACRON_CONF)) {
				return null;
			}

			$map = Map::load(self::ANACRON_CONF, 'r', 'inifile');

			return $map->section(null)['START_HOURS_RANGE'] ?? $this->getDefault();
		}

		public function getDefault()
		{
			return '3-22';
		}

		public function getHelp(): string
		{
			return 'Run cronjobs during these hours. Current time: ' . (new \DateTime())->format('H:i T');
		}

		public function getValues()
		{
			return 'int range';
		}
	}