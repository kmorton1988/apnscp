<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */

	namespace Opcenter\Admin\Settings\Apache;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Http\Apache;
	use Opcenter\Map;

	class SystemDirective implements SettingsInterface
	{
		protected const CONFIG_FILE = Apache::HTTP_HOME . '/conf/httpd-custom.conf';

		public function get(...$directive)
		{
			$ini = Map::load(Apache::SYSCONFIG_FILE);
			if (!($opts = $ini->section(null)->offsetGet('OPTIONS'))) {
				return false;
			}
			if ($directive) {
				return false !== strpos($opts, '-D' . $directive[0]);
			}
			return preg_split('/-D/', trim($opts, '\'"'), -1, PREG_SPLIT_NO_EMPTY);
		}

		public function set($directive, ...$val): bool
		{
			$val = $val[0] ?? true;
			$ini = Map::write(Apache::SYSCONFIG_FILE);
			$opts = (string)$ini->section(null)->quoted(true)->offsetGet('OPTIONS');
			$pos = strpos($opts, '-D' . $directive);
			if ((false === $pos && !$val) || (false !== $pos && $val)) {
				return true;
			}
			if (!$val) {
				$opts = preg_replace('/\s*-D' . $directive . '\b/', '', $opts);
			} else {
				$opts .= ' -D' . $directive;
			}

			return $ini->set('OPTIONS', ltrim($opts)) && (Apache::run('stop') || true) && Apache::run('start');
		}

		public function getHelp(): string
		{
			return 'Set Apache directive (-DNAME)';
		}

		public function getValues()
		{
			return 'string';
		}

		public function getDefault()
		{
			return '';
		}
	}