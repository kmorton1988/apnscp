<?php declare(strict_types=1);
	/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, September 2019
 */

	namespace Opcenter\Admin\Settings\Ssl;

	use Opcenter\Admin\Settings\Cp\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Apnscp;

	class Debug implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				return true;
			}

			$cfg = new Config();

			return $cfg->set('letsencrypt', 'debug', (bool)$val) && Apnscp::restart();
		}

		public function get()
		{
			return (new Config())->get('letsencrypt', 'debug');
		}

		public function getHelp(): string
		{
			return 'Toggle Let\'s Encrypt debug mode';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}
	}