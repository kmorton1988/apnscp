<?php declare(strict_types=1);
	/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, September 2019
 */

	namespace Opcenter\Admin\Settings\Ssl;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\Net\Hostname;
	use Opcenter\Admin\Settings\SettingsInterface;

	class Request implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			$cfg = new Config();
			$cfg['ssl_hostnames'] = $val;
			unset($cfg);
			Bootstrapper::run('apnscp/register-ssl');

			return true;
		}

		public function get()
		{
			return LETSENCRYPT_ADDITIONAL_CERTS;
		}

		public function getHelp(): string
		{
			return 'Request SSL certificates for the following hosts';
		}

		public function getValues()
		{
			return 'string|array';
		}

		public function getDefault()
		{
			return (new Hostname())->get();
		}
	}