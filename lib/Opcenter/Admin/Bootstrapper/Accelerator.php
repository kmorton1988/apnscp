<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, November 2019
 */

namespace Opcenter\Admin\Bootstrapper;

use CLI\Yum\Synchronizer\Utils;
use Opcenter\Versioning;

class Accelerator {
	private static $path = '';

	const MITOGEN_BLACKLISTED_TAGS = [
		// @TODO Mitogen requires /tmp|TMPDIR patch
		'filesystem/make-mounts'
	];

	const MITOGEN_STRATEGY_PATH = 'ansible_mitogen/plugins/strategy';

	public static function getAcceleratorType(): string
	{
		return 'mitogen';
	}

	/**
	 * Acceleration module installed
	 *
	 * @return bool
	 */
	public static function installed(): bool
	{
		return [] !== static::getEnvironment();
	}

	/**
	 * Mitogen may be used without conflict
	 *
	 * @param array $tags Bootstrapper tags
	 * @return bool
	 */
	public static function compatibleWithTags(array $tags = []): bool
	{
		if (!$tags) {
			return false;
		}

		foreach (static::MITOGEN_BLACKLISTED_TAGS as $tag) {
			if (\in_array($tag, $tags, true)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Get Accelerator environment
	 *
	 * @return array
	 */
	public static function getEnvironment(): array
	{
		if (!$pythonPath = static::getMitogenLocation()) {
			return [];
		}

		return [
			'ANSIBLE_STRATEGY_PLUGINS' => $pythonPath,
			'ANSIBLE_STRATEGY' => 'mitogen_linear'
		];
	}

	/**
	 * Get Mitogen path
	 *
	 * @return string|null
	 */
	private static function getMitogenLocation(): ?string
	{
		$cache = \Cache_Global::spawn();
		$cacheKey = 'bs.mitogenpath';
		if (false !== ($path = $cache->get($cacheKey))) {
			return $path;
		}
		// refactor to separate class
		$python = version_compare(Versioning::asMinor(os_version()), '8.0', '>=') ? 'python3' : 'python2';
		$pythonPath = null;
		if (Utils::exists($python . '-mitogen')) {
			$files = Utils::getFilesFromRPM($python . '-mitogen');
			$pythonPath = \dirname(array_shift($files));
		} else {
			// prevent pyenv from intercepting request
			$ret = \Util_Process_Safe::exec('/usr/bin/python -c %(code)s',
				[
					'code' => 'import mitogen; import json; print json.dumps(mitogen.__path__);'
				],
				[0],
				[
					'reporterror' => false
				]
			);
			if ($ret['success']) {
				$pythonPath = \dirname(array_get((array)json_decode($ret['stdout'], true), 0, [])) ?: null;
			}
		}

		if ($pythonPath) {
			$pythonPath .= DIRECTORY_SEPARATOR . self::MITOGEN_STRATEGY_PATH;
		}

		$cache->set($cacheKey, $pythonPath, 43200);

		return $pythonPath;
	}
}

