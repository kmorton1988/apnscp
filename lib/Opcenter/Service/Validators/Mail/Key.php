<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Mail;

	use Opcenter\CliParser;
	use Opcenter\Mail;
	use Opcenter\Service\Contracts\DefaultNullable;
	use Opcenter\Service\Validators\Common\ProviderKey;

	class Key extends ProviderKey
	{
		const DESCRIPTION = 'Mail provider key';

		public function valid(&$value): bool
		{
			if (!$this->ctx['enabled']) {
				return parent::valid($value);
			}

			if ($value === DefaultNullable::NULLABLE_MARKER) {
				$default = MAIL_PROVIDER_KEY;
				$value = $this->ctx['provider'] == MAIL_PROVIDER_DEFAULT ? CliParser::parseArgs($default) : null;
			}

			$provider = $this->ctx['provider'];

			if (Mail::providerHasHelper($provider)) {
				return Mail::getProviderHelper($provider)->valid($this->ctx, $value);
			}

			return parent::valid($value);
		}

		public function getDefault()
		{
			return MAIL_PROVIDER_KEY ?? '';
		}


	}
