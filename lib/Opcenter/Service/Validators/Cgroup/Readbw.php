<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
	 */


	namespace Opcenter\Service\Validators\Cgroup;

	use Opcenter\Service\ServiceValidator;

	class Readbw extends ServiceValidator
	{
		const DESCRIPTION_FRAGMENT = 'Read';
		const DESCRIPTION_UNITS = 'MB/second';
		const VALUE_RANGE = '[0, 4096]';

		public function valid(&$value): bool
		{
			if (!$value || !$this->ctx->getServiceValue('cgroup', 'enabled')) {
				$value = null;

				return true;
			}

			if (!\is_numeric($value) || $value < 0 || $value > 4096) {
				return error('%s must be between 0 and 4096 - %s seen', $this->getServiceName(), $value);
			}

			return true;
		}

		public function getDescription(): ?string
		{
			return static::DESCRIPTION_FRAGMENT . ' rate in ' . static::DESCRIPTION_UNITS;
		}


	}
