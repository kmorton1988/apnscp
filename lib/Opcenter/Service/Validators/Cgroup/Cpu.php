<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Cgroup;

	use Opcenter\Service\ServiceValidator;

	class Cpu extends ServiceValidator
	{
		const DESCRIPTION = 'Maximal 24-hour CPU usage (seconds)';

		public function valid(&$value): bool
		{
			if (!$value || !$this->ctx->getServiceValue('cgroup', 'enabled')) {
				$value = null;

				return true;
			}

			if (!\is_int($value) || $value <= 1) {
				return error('CPU time limit must be a positive integer >= 1');
			}

			return true;
		}
	}
