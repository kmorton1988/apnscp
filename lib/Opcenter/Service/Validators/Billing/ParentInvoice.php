<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Billing;

	class ParentInvoice extends Invoice
	{
		const DESCRIPTION = 'Account is subordinate to parent whose "invoice" matches this';

		public function valid(&$value): bool
		{
			if ($value && $this->ctx['invoice']) {
				return error('parent_invoice and invoice may not both be set');
			}
			if (!$this->ctx->getServiceValue('billing', 'enabled')) {
				$value = null;

				return true;
			}

			if (ctype_digit($value)) {
				return error("Invoice may not only consist of numbers");
			}

			if ($value && !$this->mapKeyInUse($value, static::MAP_FILE)) {
				// accounts perform platform migration, parent remains behind
				warn("Invoice `%s' is not attached to an account yet - attach to an account by setting billing,invoice first",
					$value);
			} else if ($value && 0 === strncmp($value, 'site', 4) && ctype_digit(substr($value, 4))) {
				return error('billing identifier cannot follow form siteXX');
			}

			return true;
		}
	}
