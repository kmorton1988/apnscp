<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Dns;

	use Opcenter\Dns;
	use Opcenter\Service\Contracts\DefaultNullable;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\Service\Validators\Common\GenericProvider;
	use Opcenter\SiteConfiguration;

	class Provider extends GenericProvider implements ServiceReconfiguration, DefaultNullable
	{

		public function valid(&$value): bool
		{
			if ($value === DefaultNullable::NULLABLE_MARKER) {
				$value = $this->getDefault();
			}

			if ($value === null) {
				// -c dns,provider=null gets converted to literal type
				$value = 'null';
			}

			if (!$value || !\Opcenter\Dns::providerValid($value)) {
				return error("Unknown dns provider `%s'", $value);
			}

			if (!$this->ctx->isRevalidation() && $this->hasChange()) {
				$this->ctx->revalidateService();
			}

			return true;
		}

		public function getDescription(): ?string
		{
			return 'Assign DNS handler for account';
		}

		public function getValidatorRange()
		{
			return \Opcenter\Dns::providers();
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if ($old === $new) {
				return true;
			}
			$this->freshenSite($svc);
			warn('DNS provider migration is not supported - manually migrate DNS records');

			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return true;
		}

		public function getDefault()
		{
			return Dns::default();
		}


	}
