<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Diskquota;

	use Opcenter\Service\ServiceValidator;

	class Fquota extends ServiceValidator
	{
		const DESCRIPTION = 'Account inode quota';

		public function valid(&$value): bool
		{
			if (!$value || !$this->ctx->getServiceValue('diskquota', 'enabled')) {
				$value = null;
			}

			if ($value !== null && !is_numeric($value)) {
				return error("inode quota threshold must be numeric, `%s' given", $value);
			}

			if ($value !== null && $value < 1) {
				return error("diskquota must be a non-negative number, `%s' found", $value);
			}

			return true;
		}

		public function getValidatorRange()
		{
			// @todo get available storage from device∞
			return '[null,0-∞]';
		}

	}