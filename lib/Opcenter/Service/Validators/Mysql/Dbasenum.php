<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Mysql;

	use Opcenter\Service\ServiceValidator;

	class Dbasenum extends ServiceValidator
	{
		const VALUE_RANGE = '[null, 0-999]';

		public function valid(&$value): bool
		{
			if ($value === null) {
				return true;
			} else if ($value === -1) {
				$value = null;
			} else if (!\is_int($value) || $value < -1 || $value > 999) {
				return error("dbasenum outside value range, must be positive integer >= 0 and < 999, `%s' given", $value);
			} else if (!$value) {
				warn('dbasenum value 0 converted to unlimited');
				$value = null;
			}

			return true;
		}

		public function getDescription(): ?string
		{
			return 'Limit total database count';
		}
	}
