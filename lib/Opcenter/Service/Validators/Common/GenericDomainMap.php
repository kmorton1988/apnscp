<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Common;

	abstract class GenericDomainMap extends GenericMap
	{
		use \NamespaceUtilitiesTrait;
		const MAP_FILE = null;
		// remap domain on change for these service vars
		const REMAP_DOMAIN = [
			'mailserver',
			'ftpserver',
			'smtpserver',
			'webserver'
		];

		public function valid(&$value): bool
		{
			if (!$this->ctx->getServiceValue(null, 'enabled')) {
				$value = null;

				return true;
			}
			$value = strtolower((string)$value);
			$svcvar = strtolower(static::getBaseClassName());
			$default = $this->ctx->getDefaultServiceValue($this->ctx->getModuleName(), $svcvar);
			if (\in_array($svcvar, static::REMAP_DOMAIN, true) && null !== $this->ctx->getNewServiceValue('siteinfo',
					'domain') &&
				null === $this->ctx->getNewServiceValue(null, $svcvar)) {
				// domain name change, reset
				$value = $default;
			}
			if ($value === $default) {
				$value .= $this->ctx->getServiceValue('siteinfo', 'domain');
			}

			// fix-up domain, make FQDN as necessary
			if (!parent::valid($value)) {
				return false;
			}
			if (!preg_match(\Regex::DOMAIN, $value)) {
				return error("verify conf failed: domain `%s' is not valid", $value);
			}

			return true;
		}

		protected function mapKeyInUse(string $key, string $map, $checker = null): bool
		{
			$checker = $checker ?? $this->site;

			return parent::mapKeyInUse($key, $map, $checker);
		}
	}
