<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */


	namespace Opcenter\Service\Validators\Pgsql;

	use Opcenter\Service\ServiceValidator;

	/**
	 * Class Tablespace
	 *
	 * @TODO    generate random hash for each site
	 *
	 * @package Opcenter\Service\Validators\Pgsql
	 */
	class Tablespace extends ServiceValidator
	{
		public function valid(&$value): bool
		{
			if ($value) {
				return error('not implemented yet');
			}

			return true;
		}

	}