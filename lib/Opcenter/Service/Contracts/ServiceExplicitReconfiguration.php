<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
	 */

	namespace Opcenter\Service\Contracts;

	/**
	 * Interface ServiceExplicitReconfiguration
	 *
	 * Alter behavior to always run reconfiguration hooks if service
	 * value changes. Ignores case when reconfiguration yields to
	 * populate/depopulate on service,enabled=1
	 *
	 *
	 * @package Opcenter\Service\Contracts
	 */
	interface ServiceExplicitReconfiguration extends ServiceReconfiguration
	{
	}