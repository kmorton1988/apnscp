<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace Opcenter\Http\Apache;

	use Tivie\HtaccessParser\HtaccessContainer;
	use Tivie\HtaccessParser\Token\BaseToken;
	use Tivie\HtaccessParser\Token\Block;

	class UnboundedHtaccessContainer extends HtaccessContainer
	{
		/**
		 * @param string $name
		 * @param null   $type
		 * @param bool   $deepSearch
		 * @return \Tivie\HtaccessParser[]
		 */
		public function search($name, $type = null, $deepSearch = true)
		{
			$tokens = [];
			/** @var TokenInterface[] $array */
			$array = $this->getArrayCopy();

			foreach ($array as $token) {
				if (fnmatch($name, $token->getName())) {
					if ($type === null || $token->getTokenType() === $type) {
						$tokens[] = $token;
					}
				} else if ($token instanceof Block && $token->hasChildren() && $deepSearch) {
					if ($res = $this->deepSearch($token, $name, $type)) {
						$tokens = array_merge($tokens, $res);
					}
				}
			}

			return $tokens;
		}

		private function deepSearch(Block $parent, $name, $type)
		{
			$tokens = [];
			foreach ($parent as $token) {
				if (fnmatch($name, $token->getName())) {
					if ($type === null || $token->getTokenType() === $type) {
						$tokens[] = $token;
					}
				} else if ($token instanceof Block && $token->hasChildren()) {
					if ($res = $this->deepSearch($token, $name, $type)) {
						$tokens = array_merge($tokens, $res);
					}
				}
			}

			return $tokens;
		}

		public function replaceWith(BaseToken $old, $new)
		{
			$index = $this->getIndex($old);
			$this->offsetSet($index, $new);
		}

	}

