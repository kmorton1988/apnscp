<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, January 2020
	 */

	namespace Opcenter\Http\Php\Fpm;

	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Filesystem;
	use Opcenter\Versioning;

	class MultiPhp
	{

		const MULTIBASE = FILESYSTEM_SHARED . '/php/multiphp';

		/**
		 * List all PHP builds
		 * @return array
		 */
		public static function list(): array
		{
			$cfg = new Config();
			return [
				Versioning::asMinor((string)$cfg['system_php_version']) => 'system'
			] + array_fill_keys(static::listNative(), 'native') + array_fill_keys(static::listPackaged(), 'package');
		}

		/**
		 * Get native builds
		 *
		 * @return array
		 */
		public static function listNative(): array
		{
			return self::readWrapper(static::getMultiPhpPath('native'));
		}

		/**
		 * Get packaged builds
		 *
		 * @return array
		 */
		public static function listPackaged(): array
		{
			return self::readWrapper(static::getMultiPhpPath('package'));
		}

		private static function readWrapper(string $path): array
		{
			if (!file_exists($path)) {
				return [];
			}

			return Filesystem::readdir($path, static function ($f) {
				if (is_numeric($f)) {
					return $f;
				}
				if (0 !== strncmp($f, 'php', 3)) {
					// garbage entry
					return null;
				}

				$f = substr($f, 3);
				$splitOffset = 1;
				if ($f[0] === '1') {
					++$splitOffset;
				}
				return substr($f, 0, $splitOffset) . '.' . substr($f, $splitOffset);
			});
		}

		/**
		 * Get multiPHP location
		 *
		 * @param string $type native or package
		 * @return string
		 */
		protected static function getMultiPhpPath(string $type): string
		{
			if ($type === 'native') {
				return static::MULTIBASE . '/native';
			}

			return static::MULTIBASE . '/opt/remi';
		}
	}