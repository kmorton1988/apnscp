<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
	 */

	namespace Opcenter\Http\Php\Fpm;

	use Opcenter\Contracts\Virtualizable;
	use Opcenter\Http\Php\Fpm;
	use Opcenter\Role\User;
	use Opcenter\SiteConfiguration;

	class Configuration implements Virtualizable
	{
		const CONFIGURATION_BASE_PATH = '/etc/php-fpm.d/sites';

		// @var string group name
		protected $group;

		// @var string worker name
		protected $name;

		// @var string
		protected $sysuser = APACHE_UID;

		// @var string
		protected $sysgroup = \Web_Module::WEB_USERNAME;

		protected $phpConfiguration = [
			'memory_limit' => -1
		];

		// @var string
		protected $binary = '/usr/sbin/php-fpm';

		// @var ResourceManager
		protected $resourceManager;
		/**
		 * @var string account root
		 */
		protected $root;
		/**
		 * @var SiteConfiguration
		 */
		protected $svc;

		public function __construct(string $root = '/')
		{
			$this->root = $root;
		}

		public static function bindTo(string $root = '/')
		{
			return new static($root);
		}

		/**
		 * Set site configuration template
		 *
		 * @param SiteConfiguration $svc
		 * @return Configuration
		 */
		public function setServiceContainer(SiteConfiguration $svc): self
		{
			$this->svc = $svc;
			return $this;
		}

		/**
		 * Get site service container
		 *
		 * @return SiteConfiguration
		 */
		public function getServiceContainer(): SiteConfiguration
		{
			return $this->svc;
		}

		/**
		 * Get system root
		 *
		 * @return string
		 */
		public function getRoot(): string
		{
			return $this->root;
		}

		/**
		 * Set group name
		 *
		 * @param string $group
		 * @return self
		 */
		public function setGroup(string $group): self
		{
			if (!preg_match('/^[a-zA-Z0-9_-]{3,}$/', str_replace(['.', '_'], '-', strtolower($group)))) {
				fatal("Invalid worker group `%s'", $group);
			}
			$this->group = $group;

			return $this;
		}

		/**
		 * @return string
		 */
		public function getGroup(): string
		{
			return $this->group;
		}

		/**
		 * Get system owner
		 *
		 * @return int
		 */
		public function getSysUser(): int
		{
			return $this->sysuser;
		}

		/**
		 * Set system owner
		 *
		 * @param int|string $sysuser
		 * @return self
		 */
		public function setSysUser($sysuser): self
		{
			if (!\is_int($sysuser)) {
				$pwd = User::bindTo($this->getRoot())->getpwnam($sysuser);
				if (null === $pwd) {
					fatal("Unknown sysuser `%s'", $sysuser);
				}
				$sysuser = $pwd['uid'];
			}
			$this->sysuser = (int)$sysuser;

			return $this;
		}

		/**
		 * @param mixed $name
		 * @return Configuration
		 */
		public function setName($name): self
		{
			if (!preg_match('/^[a-zA-Z0-9_-]{3,}$/', str_replace(['.','_'], '-', strtolower($name)))) {
				fatal("Invalid worker name `%s'", $name);
			}
			$this->name = $name;

			return $this;
		}

		/**
		 * Get worker name
		 *
		 * @return string
		 */
		public function getName(): string
		{
			return $this->name;
		}

		/**
		 * Set worker group
		 *
		 * @param mixed $sysgroup
		 * @return self
		 */
		public function setSysGroup($sysgroup): self
		{
			if (\is_int($sysgroup)) {
				if (false === ($grp = posix_getgrgid($sysgroup))) {
					fatal("Unknown sysgroup specified `%d'", $sysgroup);
				}
				$sysgroup = $grp['name'];

			} else if (false === posix_getgrnam($sysgroup)) {
				fatal("Unknown sysgroup `%s'", $sysgroup);
			}

			$this->sysgroup = $sysgroup;

			return $this;
		}

		/**
		 * @return string
		 */
		public function getSysGroup(): string
		{
			return $this->sysgroup;
		}

		/**
		 * Get PHP-FPM socket path
		 *
		 * @return string
		 */
		public function getSocketPath(): string
		{
			// Prefer /run globally, an ephemeral mount
			return '/var/run/php-fpm/' . $this->getGroup() . '-' . $this->getName() . '.socket';
		}

		/**
		 * Number of threads per server dedicated to proxy
		 *
		 * @return int
		 */
		public function getProxyThreadsPerServer(): int
		{
			return 3;
		}

		/**
		 * Get accept() timeout to proxy backend
		 *
		 * @return string|int
		 */
		public function getProxyConnectionTimeout() {
			return '5s';
		}


		/**
		 * Get PHP-FPM pid path
		 *
		 * @return string
		 */
		public function getPidPath(bool $jailed = false): string
		{
			return ($jailed ? '' : $this->getRoot()) . '/var/run/php-fpm/' . $this->getName() . '.pid';
		}

		/**
		 * Get systemd service name
		 *
		 * @return string
		 */
		public function getServiceName(): string
		{
			return Fpm::SERVICE_NAMESPACE . $this->getGroup() . '-' . $this->getName() . '.service';
		}

		/**
		 * Get service group name
		 *
		 * @return string
		 */
		public function getServiceGroupName(): string
		{
			if (!$this->getGroup()) {
				fatal('Group not defined for FPM configuration!');
			}
			return Fpm::SERVICE_NAMESPACE . $this->getGroup() . '.service';
		}

		/**
		 * Get systemd service name
		 *
		 * @return string
		 */
		public function getSocketServiceName(): string
		{
			return Fpm::SERVICE_NAMESPACE . $this->getGroup() . '-' . $this->getName() . '.socket';
		}

		/**
		 * Get PHP-FPM configuration path
		 *
		 * @param bool $jailed
		 * @return string
		 */
		public function getFpmConfigurationPath(bool $jailed = false): string {
			return ($jailed ? '' : $this->getRoot()) . static::CONFIGURATION_BASE_PATH . '/' . $this->getName() . '.conf';
		}

		/**
		 * Get PHP-FPM binary
		 *
		 * @return string
		 */
		public function getBinary(): string
		{
			return $this->binary;
		}

		/**
		 * Enable resource accounting
		 *
		 * @return bool
		 */
		public function hasResourceAccounting(): bool
		{
			return null !== $this->resourceManager;
		}

		/**
		 * Get cgroup resource manager for PHP-FPM instance
		 *
		 * @return ResourceManager|null
		 */
		public function getResourceManager(): ?ResourceManager
		{
			return $this->resourceManager;
		}

		/**
		 * Create a resource manager, implicitly assigning it
		 *
		 * @return ResourceManager
		 */
		public function useResourceManager(): ResourceManager
		{
			if (!isset($this->resourceManager)) {
				$this->resourceManager = new ResourceManager();
			}
			return $this->resourceManager;
		}

		public function getLog(): string
		{
			if (!$this->getName()) {
				fatal('name must be set first');
			}
			return '/var/log/php-fpm/' . $this->getName() . '.log';
		}
	}
