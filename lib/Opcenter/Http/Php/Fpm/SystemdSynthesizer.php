<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
 */

namespace Opcenter\Http\Php\Fpm;

use Opcenter\Http\Php\Fpm;
use Opcenter\System\Contracts\SystemdStatusReporting;
use Opcenter\System\GenericSystemdService;

class SystemdSynthesizer {

	/**
	 * Create lightweight systemd service object from service
	 *
	 * @param string $name
	 * @return GenericSystemdService
	 */
	public static function mock(string $name): GenericSystemdService {
		return new class($name) extends GenericSystemdService implements SystemdStatusReporting
		{
			protected const STATUS_REGEX = '';
			protected const SERVICE = '';
			private static $svc;

			public function __construct($service)
			{
				if (0 !== strpos($service, Fpm::SERVICE_NAMESPACE)) {
					$service = Fpm::SERVICE_NAMESPACE . $service;
				}
				static::$svc = $service;
			}

			protected static function getService(): string
			{
				return static::$svc;
			}

			public static function getReportedServiceStatus(): ?array
			{
				$data = (array)static::status(['StatusText']);
				$data['Id'] = static::class;
				return (new PoolStatus($data))->getMetrics();
			}


		};
	}

}