<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Provisioning\Traits;

	use Opcenter\Map;

	trait MapperTrait
	{
		/**
		 * Add a domain to domain database
		 *
		 * @param string $site
		 * @param string $domain
		 * @return bool
		 */
		public static function map(string $site, string $domain): bool
		{
			return static::mapWrap('add', $site, $domain);
		}

		/**
		 * Remove domain from db
		 *
		 * @param string      $domain
		 * @param bool|string $challenge optional challenge to match before deleting
		 * @return bool
		 */
		public static function unmap(string $domain, $challenge = ''): bool
		{
			return static::mapWrap('remove', $domain, (string)$challenge);
		}

		/**
		 * Add or remove a domain
		 *
		 * @param string      $mode add or remove
		 * @param string      $kv   value (add) or key (remove)
		 * @param null|string $misc key (add) to insert or challenge to confirm (remove). Specify $misc as "" to bypass
		 * @param string      $db   optional map otherwise class constant MAP_FILE
		 * @return bool
		 */
		protected static function mapWrap(string $mode, string $kv, string $misc, $db = null): bool
		{
			if (!($db = $db ?? static::MAP_FILE)) {
				fatal('no map file specified');
			}
			$map = Map::load($db, 'cd');

			if ($mode === 'add') {
				return $map->insert($misc, $kv);
			}
			if ($mode !== 'remove') {
				return error("unknown map mode `%s'", $mode);
			}
			// prevent accidentally deleting non-owned entries
			if ('' !== $misc) {
				$match = $map->fetch($kv);
				if (false !== $match && $misc !== $match) {
					return warn("not removing `%s' from `%s': key `%s' does not match challenge `%s'",
						$kv,
						$db ?? static::MAP_FILE,
						$match,
						$misc
					);
				}
			}

			return !$map->exists($kv) || $map->delete($kv);
		}
	}