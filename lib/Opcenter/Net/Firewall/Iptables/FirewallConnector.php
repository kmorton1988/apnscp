<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */


	namespace Opcenter\Net\Firewall\Iptables;

	use Opcenter\Net\Firewall\Contracts\FirewallInterface;
	use Opcenter\Net\Firewall\Iptables;

	class FirewallConnector implements FirewallInterface
	{

		/**
		 * Get entries from group
		 *
		 * @param string|null $group
		 * @return Rule[]
		 */
		public static function getEntriesFromGroup(string $group = null): array
		{
			if (!$group) {
				$groups = array_fill_keys(self::groups(), []);
			} else {
				$groups = [
					$group => []
				];
			}
			foreach (Iptables::getEntriesFromChain($group) as $gname => $entries) {
				foreach ($entries as $entry) {
					if ($entry['target'] !== 'DROP' && $entry['target'] !== 'REJECT') {
						continue;
					}
					$rule = new Rule([
						'source'      => $entry['source'],
						'destination' => $entry['destination'],
						'target'      => $entry['target'],
						'record'      => $entry['record'],
						'proto'       => $entry['proto'],
						'opt'         => $entry['opt'],
						'type'        => $entry['type']
					]);
					$groups[$gname][] = $rule;
				}
			}

			return $group ? $groups[$group] ?? [] : $groups;
		}

		/**
		 * Firewall groups
		 *
		 * @return array
		 */
		public static function groups(): array
		{
			return Iptables::chains();
		}

		public static function append(\Opcenter\Net\Firewall\Rule $rule): bool
		{
			return Iptables::append($rule->getGroup(), $rule->getHost(), 'INPUT', $rule->getAction());
		}

		public static function insert(\Opcenter\Net\Firewall\Rule $rule): bool
		{
			return Iptables::insert($rule->getGroup(), $rule->getHost(), 'INPUT', $rule->getAction());
		}

		public static function remove(\Opcenter\Net\Firewall\Rule $rule): bool
		{
			return Iptables::remove($rule->getGroup(), $rule->getIndex());

		}

	}