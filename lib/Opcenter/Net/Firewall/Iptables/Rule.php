<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Net\Firewall\Iptables;

	use Opcenter\Net\Firewall\Iptables;

	class Rule extends \Opcenter\Net\Firewall\Rule
	{
		protected $target = 'DROP';
		protected $action = 'append';
		protected $chain;
		protected $opt;
		protected $source;
		protected $destination;
		protected $record;
		protected $proto;
		protected $type;

		public function setRecord(int $val)
		{
			if ($val < 0) {
				return false;
			}
			$this->record = $val;
		}

		public function setTarget(string &$name): bool
		{
			$name = strtoupper($name);
			if (!\in_array($name, Iptables::TARGETS, true)) {
				return error("Unknown iptables target `%s'", $name);
			}

			return true;
		}

		public function setAction(string $action): bool
		{
			if ($action !== 'append' && $action !== 'insert' && $action !== 'delete') {
				return error("Unknown iptables action `%s'", $action);
			}

			return true;
		}

		public function isBlocked(): bool
		{
			return $this->target === 'DROP' || $this->target === 'REJECT';
		}

		public function getGroup(): string
		{
			return $this->chain;
		}

		public function getHost(): string
		{
			return $this->source;
		}

		public function setHost($val)
		{
			$this->host = $val;

			return null;
		}
	}

