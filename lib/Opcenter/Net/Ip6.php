<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Net;

	class Ip6 extends IpCommon
	{
		const IP_DISCOVERY = 'dig +short -6 myip.opendns.com aaaa @resolver1.ipv6-sandbox.opendns.com';
		const NB_POOL = 'namebased_ip6_addrs';
		// @link https://www.iana.org/assignments/iana-ipv6-special-registry/iana-ipv6-special-registry.xhtml
		const RESERVED_BLOCKS = [
			'::1/128',
			'::/128',
			'::ffff:0:0/96',
			'64:ff9b:1::/48',
			'100::/64',
			'2001::/23',
			'2001::/23',
			'2001::/32',
			'2001:1::1/128',
			'2001:1::2/128',
			'2001:2::/48',
			'2001:2::/48',
			'2001:3::/32',
			'2001:4:112::/48',
			'2001:10::/28',
			'2001:20::/28',
			'2001:db8::/32',
			'2002::/16',
			'2620:4f:8000::/48',
			'fc00::/7',
			'fe80::/10'
		];

		public static function announce($ip, $iface = null)
		{
			if (null === $iface) {
				$iface = Iface::ip_iface($ip);
			}
			if (OPCENTER_IP_BIND_CHECK && !$iface) {
				fatal("could not detect interface for IP address `%s' - is IP bound?", $ip);
			} else if (!$iface) {
				$iface = Iface::detect();
			}
			$proc = new \Util_Process_Safe('now');
			$newpath = implode(PATH_SEPARATOR, array('/sbin', getenv('PATH')));
			$proc->setEnvironment('PATH', $newpath);
			$ret = $proc->run(
				'ndptool -U -t na -i %s -T %s send',
				$iface,
				$ip
			);

			return $ret['success'] ?: error($ret['stderr']);
		}

		public static function bind($ip, $iface)
		{
			$ret = \Util_Process::exec(
				'ip -6 neighbor add %s dev %s',
				$ip, $iface
			);
			return $ret['success'] ?: error($ret['stderr']);
		}

		public static function unbind($ip)
		{
			if (!($iface = Iface::ip_iface($ip))) {
				return false;
			}

			$ret = \Util_Process::exec(
				'ip -6 neighbor del %s dev %s',
				$ip, $iface
			);

			return $ret['success'] ?: error($ret['stderr']);
		}

		public static function nb_pool(): array
		{
			$path = \Opcenter::mkpath(self::NB_POOL);
			if (file_exists($path)) {
				return file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
			}

			return [static::my_ip()];
		}

		/**
		 * Get primary server IP address
		 *
		 * @return string
		 */
		public static function my_ip(): string
		{
			if (self::enabled()) {
				return \constant('DNS_MY_IP6');
			}

			$cmd = static::IP_DISCOVERY;
			$ret = \Util_Process::exec($cmd);
			if ($ret['success']) {
				return trim($ret['stdout']);
			}

			fatal('ipv6 requires manual configuration in config.ini ([dns] => my_ip6)');
		}

		/**
		 * IPv6 support enabled
		 *
		 * @return bool
		 */
		public static function enabled(): bool
		{
			return file_exists('/proc/net/tcp6');
		}

		public static function valid(string $address): bool
		{
			return false === ip2long($address) && false !== inet_pton($address);
		}
	}