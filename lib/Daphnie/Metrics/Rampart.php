<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2020
	 */

	namespace Daphnie\Metrics;

	use Daphnie\Contracts\MetricProvider;
	use Daphnie\Metric;
	use Daphnie\MutableMetricTrait;

	class Rampart extends Metric implements MetricProvider
	{
		use MutableMetricTrait;

		public static function getAttributes(): array {
			$jails = \apnscpFunctionInterceptor::init()->rampart_get_jails();
			return array_combine($jails, $jails);
		}

		protected function getBinding(string $metric): array
		{
			return array_get(self::getAttributeMap(), $metric, []);
		}

		public static function getAttributeMap(): array
		{
			return array_build(self::getAttributes(), static function ($k, $attr) {
				return [
					$attr, [
						'label' => $attr . ' bans',
						'type'  => MetricProvider::TYPE_VALUE
					]
				];
			});
		}
	}
