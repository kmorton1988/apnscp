<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2020
	 */

	namespace Daphnie\Metrics;

	use Daphnie\Contracts\MetricProvider;
	use Daphnie\Metric;
	use Daphnie\MutableMetricTrait;

	class Php extends Metric implements MetricProvider
	{
		use MutableMetricTrait;

		public const ATTRVAL_MAP = [
			'active'    => 'active',
			'idle'      => 'idle',
			'requests'  => 'requests',
			'slow'      => 'slow',
			'traffic'   => 'traffic',
			'uptime'    => 'uptime',
			'cused'     => 'cache-used',
			'ctotal'    => 'cache-total',
			'chits'     => 'cache-hits',
			'cmisses'   => 'cache-misses'
		];

		private const ATTR_BINDINGS = [
			'active'    => [
				'label' => 'Active pool workers',
				'type'  => MetricProvider::TYPE_VALUE
			],
			'idle'      => [
				'label' => 'Idle pool workers',
				'type'  => MetricProvider::TYPE_VALUE
			],
			'requests'  => [
				'label' => 'Total requests',
				'type'  => MetricProvider::TYPE_MONOTONIC
			],
			'slow'      => [
				'label' => 'Slow requests',
				'type'  => MetricProvider::TYPE_MONOTONIC
			],
			'traffic'    => [
				'label'  => 'Traffic rate',
				'unit'   => 'req/sec',
				'type'   => MetricProvider::TYPE_VALUE
			],
			'uptime'    => [
				'label' => 'Pool uptime',
				'unit'  => 'sec',
				'type'  => MetricProvider::TYPE_MONOTONIC
			],
			'cused'     => [
				'label' => 'Cache used',
				'unit'  => 'KB',
				'type'  => MetricProvider::TYPE_MONOTONIC
			],
			'ctotal' => [
				'label' => 'Cache total',
				'unit'  => 'KB',
				'type'  => MetricProvider::TYPE_MONOTONIC
			],
			'chits'  => [
				'label' => 'Cache hits',
				'type'  => MetricProvider::TYPE_MONOTONIC
			],
			'cmisses'   => [
				'label' => 'Cache misses',
				'type'  => MetricProvider::TYPE_MONOTONIC
			]

		];
	}
