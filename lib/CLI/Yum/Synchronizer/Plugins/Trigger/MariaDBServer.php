<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	use CLI\Yum\Synchronizer\Plugins\Trigger;

	/**
	 * Class MariaDBServer
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 * Remove "mysql" alias that can cause
	 * conflict with systemd forwarding
	 *
	 */
	class MariaDBServer extends Trigger
	{
		public function install(string $package): bool
		{
			$svc = '/etc/init.d/mysql';
			if (file_exists($svc) && is_file($svc)) {
				unlink($svc);
			}
			return parent::install($package);
		}

		public function update(string $package): bool
		{
			$this->install($package);
			$proc = new \Util_Process();
			$proc->setEnvironment('HOME', '/root');
			// mysqld must be restarted first otherwise
			$ret = $proc->run('/usr/bin/mysql_upgrade -k --upgrade-system-tables');
			if (!$ret['success']) {
				warn('Failed to run mysql_upgrade: %s', coalesce($ret['stderr'], $ret['stdout']));
			}
			return true;
		}

	}