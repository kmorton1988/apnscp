<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace CLI\Yum\Synchronizer;

	use CLI\Yum\Synchronizer;

	/**
	 * Class Clean
	 *
	 * Generate an index of RPM files
	 */
	class Clean extends Synchronizer
	{
		public function __construct()
		{

		}

		public function run()
		{
			$db = \PostgreSQL::initialize()->getHandler();

			$res = pg_query($db, 'SELECT package_name, version, service FROM site_packages');

			while (false !== ($rs = pg_fetch_object($res))) {
				$package = $rs->package_name;
				$ret = \Util_Process_Safe::exec('rpm -ql %s', $package);
				if (!$ret['success']) {
					pg_query($db,
						"DELETE FROM site_packages WHERE package_name = '" . pg_escape_string($package) . "'");
					info("removed orphaned package `%s' from `%s'", $package, $rs->service);
					continue;
				}
			}

			return true;
		}
	}