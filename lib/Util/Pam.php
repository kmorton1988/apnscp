<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Util_Pam
	{
		use apnscpFunctionInterceptorTrait;
		use FilesystemPathTrait;
		const SERVICE_MAP = [
			'ftp'     => 'ftp',
			'proftpd' => 'ftp',
			'imap'    => 'email',
			'smtp'    => 'email',
			'pop3'    => 'email',
			'ssh'     => 'ssh',
			'cp'      => 'auth',
			'dav'     => 'auth',
		];

		protected $ctx;

		public function __construct(\Auth_Info_User $context)
		{
			$this->ctx = $context;
			$this->site = $context->site;
			$this->setApnscpFunctionInterceptor(\apnscpFunctionInterceptor::factory($context));
		}

		/**
		 * Rename user in all PAM services
		 *
		 * @param string $old
		 * @param string $new
		 * @return int number of services user changed
		 */
		public function renameUser(string $old, string $new): int
		{
			$count = 0;
			foreach ($this->enrolled($old) as $svc) {
				if ($this->change($old, $new, $svc)) {
					$count++;
				}
			}

			return $count;
		}

		/**
		 * Get all services for which user is permitted
		 *
		 * @param string $user
		 * @return array
		 */
		public function enrolled(string $user): array
		{
			$services = [];
			foreach ($this->services() as $svc) {
				if ($this->check($user, $svc)) {
					$services[] = $svc;
				}
			}

			return $services;
		}

		/**
		 * Get all services installed on an account
		 *
		 * @return array
		 */
		public function services(): array
		{
			return array_map(static function ($f) {
				return basename($f, '.pamlist');
			}, glob($this->domain_fs_path('/etc/*.pamlist')));
		}

		/**
		 * Check if user is permitted to use PAM service
		 *
		 * @param string $user    username
		 * @param string $pam_svc pam service name
		 * @return bool
		 */
		public function check(string $user, string $pam_svc): bool
		{
			$pam_svc = $this->fixup($pam_svc);
			$pam_file = $this->getControlFile($pam_svc);
			if (!file_exists($pam_file)) {
				return error($pam_svc . ': listfile for pam service does not exist');
			}

			return $this->canLogin() && preg_match('!\b' . $user . '\b!', file_get_contents($pam_file)) > 0;
		}

		/**
		 * Platform PAM service renames
		 *
		 * @param string $svc
		 * @return string
		 */
		private function fixup(string $svc): string
		{
			if ($svc === 'smtp' || $svc === 'smtp_relay') {
				return platform_is('7.5') ? 'smtp' : 'smtp_relay';
			}
			if ($svc === 'proftpd' || $svc === 'ftp') {
				return platform_is('7.5') ? 'ftp' : 'proftpd';
			}

			return $svc;
		}

		public function getControlFile(string $svc): string
		{
			return $this->domain_fs_path('/etc/' . $svc . '.pamlist');
		}

		/**
		 * Users can login
		 *
		 * @return bool
		 */
		public function canLogin(): bool
		{
			return !file_exists($this->domain_fs_path('/etc/nologin'));
		}

		/**
		 * Change a PAM entry
		 *
		 * @param string $olduser
		 * @param string $newuser
		 * @param string $pam_svc
		 * @return bool
		 */
		public function change(string $olduser, string $newuser, string $pam_svc): bool
		{
			$pam_svc = $this->fixup($pam_svc);
			if (!$this->check($olduser, $pam_svc)) {
				return true;
			}
			$this->remove($olduser, $pam_svc);
			if ($this->check($newuser, $pam_svc)) {
				return warn("user `%s' already permitted to use service `%s'", $newuser, $pam_svc);
			}

			return $this->add($newuser, $pam_svc);
		}

		/**
		 * Deny user from PAM service
		 *
		 * @param string $user    username
		 * @param string $pam_svc pam service
		 * @return bool
		 */
		public function remove(string $user, string $pam_svc): bool
		{
			$pam_svc = $this->fixup($pam_svc);
			if (posix_getuid()) {
				// don't worry if done via hooks as hooks occur as root
				$funcs = debug_backtrace();
				$class = $funcs[1]['class'];
				$method = $funcs[1]['function'];
				$argc = count($funcs[1]['args']);
				$signature = strtolower(substr($class, 0, strpos($class, '_')) . '_' . $method);
				// additional arguments will get ignored
				if ($argc > 1) {
					return DataStream::get($this->ctx)->query($signature, $user, $pam_svc);
				}

				return DataStream::get($this->ctx)->query($signature, $user);
			}

			$pam_file = $this->getControlFile($pam_svc);
			if (!file_exists($pam_file)) {
				return error($pam_svc . ': listfile for pam service does not exist');
			}

			$contents = array();
			if (file_exists($pam_file)) {
				$contents = file($pam_file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
			}
			$idx = array_search($user, $contents);
			if (false === $idx) {
				return true;
			}
			unset($contents[$idx]);
			file_put_contents($pam_file, join("\n", $contents) . "\n");

			return true;
		}

		/**
		 * Permit user to use PAM service
		 *
		 * @param  string $user    username
		 * @param  string $pam_svc pam service
		 * @return bool
		 */
		public function add(string $user, string $pam_svc): bool
		{
			$pam_svc = $this->fixup($pam_svc);
			if (posix_getuid()) {
				// don't worry if done via hooks as hooks occur as root
				$funcs = debug_backtrace();
				$class = $funcs[1]['class'];
				$method = $funcs[1]['function'];
				$argc = count($funcs[1]['args']);
				$signature = strtolower(substr($class, 0, strpos($class, '_')) . '_' . $method);
				// additional arguments will get ignored
				if ($argc > 1) {
					return DataStream::get($this->ctx)->query($signature, $user, $pam_svc);
				}

				return DataStream::get($this->ctx)->query($signature, $user);

			}

			$pam_file = $this->getControlFile($pam_svc);
			if (!file_exists($pam_file)) {
				return error($pam_svc . ': pam service does not exist');
			}
			if (!$this->user_exists($user)) {
				warn($user . ': user does not exist');
			}

			$contents = array();
			if (file_exists($pam_file)) {
				$contents = file($pam_file, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);
			}

			if (in_array($user, $contents, true)) {
				return info($user . ': user already permitted for ' . $pam_svc);
			}

			$contents[] = $user;
			file_put_contents($pam_file, join("\n", $contents) . "\n");

			return true;
		}
	}