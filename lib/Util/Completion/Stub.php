<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/*
	 * IntelliSense generator
	 *
	 * Compiles all callable modules into a single file
	 * for use with an IDE
	 *
	 * To pull up a function list, type the function as if it were a function,
	 * not a method:
	 *
	 * RIGHT
	 * crm_get_ <key combo for popup>
	 *
	 * WRONG
	 * $this->crm_get_ <key combo for popup>
	 */

	class Util_Completion_Stub
	{
		/**
		 * Util_Completion_Stub constructor.
		 *
		 * @param bool $useclass generate as class
		 */
		protected $useclass = true;
		private $_afi;

		private function __construct()
		{
			$this->_afi = apnscpFunctionInterceptor::init();
		}

		public static function generate(string $file)
		{
			if (file_exists($file)) {
				unlink($file);
			}
			$class = static::class;
			$stub = new $class($file);
			$commands = $stub->collect();

			return $stub->write($commands, $file);
		}

		public function disableClassGeneration()
		{
			$this->useclass = false;
		}

		public function collect()
		{
			$afi = $this->_afi;
			$exported_commands = $afi->get_loaded_modules();
			$reflection_cache = $afi->get_reflection_cache();
			$baseMethods = new ReflectionClass('Module_Skeleton');

			$inheritedMethods = array();
			foreach ($baseMethods->getMethods() as $method) {
				$inheritedMethods[$method->getName()] = 1;
			}

			$stub = array();
			foreach ($reflection_cache as $module => $class) {
				/** nothing to expose */
				$exports = $exported_commands->getModule($module)->getExportedFunctions();
				if (sizeof($exports) == 0) {
					continue;
				}

				$default = $exports['*'] ?? 0;
				$classSignature = ucwords($module);
				foreach ($class->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
					/**
					 * @var $method ReflectionMethod
					 */
					$funcName = $method->getName();
					$perms = $exports[$funcName] ?? $default;
					if ($funcName[0] == '_' || isset($inheritedMethods[$funcName]) || $perms & PRIVILEGE_SERVER_EXEC ||
						0 !== strpos($class->getName(), $method->getDeclaringClass()->getName())) {
						continue;
					}
					$qualifiedName = $module . '_' . $funcName;
					$props = array(
						'params'   => array(),
						'comments' => null
					);

					$comments = $method->getDocComment();
					$comments = preg_replace('/^(?:\s*(?=\/)|\s*(?=\s))/m', '', $comments);
					$props['comments'] = $comments;

					// get parameter info
					$parameters = $method->getParameters();
					$params = array();
					foreach ($parameters as $p) {
						$name = $p->getName();
						$tmp = array(
							'default'  => null,
							'required' => true
						);
						$tmp['required'] = !$p->isOptional();
						if (!$tmp['required']) {
							if ($p->isVariadic()) {
								$tmp['default'] = 'mixed';
								break;
							}

							$val = $p->getDefaultValue();
							if (is_null($val)) {
								$val = 'null';
							} else if (is_bool($val)) {
								$val = !$val ? 'false' : 'true';
							} else if (is_numeric($val)) {
								if (!$val) {
									$val = '0';
								}
							} else if (is_string($val)) {
								$val = "'" . $val . "'";
							} else if (is_array($val)) {
								$val = 'array()';
							} else {
								dlog('WARN: found unknown type - ' . gettype($val));
								$val = "''";
							}
							$tmp['default'] = $val;
						}
						$params[$name] = $tmp;
					}
					$props['params'] = $params;
					$stub[$qualifiedName] = $props;
				}
			}

			if (!$this->useclass) {
				foreach ($this->_skipList as $skip) {
					if (array_key_exists($skip, $stub)) {
						unset($stub[$skip]);
					}
				}
			}

			return $stub;
		}

		public function write(array $commands, string $file): bool
		{
			$contents = array();
			$contents[] = 'class apnscpIntellisenseDeclaration extends ' . self::getScopedSkeleton() . ' {';
			foreach ($commands as $command => $props) {
				$intellisense = '';
				$comments = $props['comments'];
				if ($comments) {
					$intellisense = $comments . "\n";
				}
				$intellisense .= ($this->useclass ? 'public ' : '') . 'function ' . $command . ' (';
				$paramsig = array();
				foreach ($props['params'] as $paramname => $paraminfo) {
					$tmp = '$' . $paramname;
					if (!$paraminfo['required']) {
						$tmp .= ' = ' . $paraminfo['default'];
					}
					$paramsig[] = $tmp;
				}
				$intellisense .= join(', ', $paramsig) . ') {}';

				$contents[] = $intellisense;
			}
			if ($this->useclass) {
				$contents[] = '}';
			}
			if (!$this->useclass) {
				$contents[] = 'class Module_Skeleton extends ' . self::getScopedSkeleton() . '{}';
			} else {
				$contents[] = 'class Module_Skeleton extends apnscpIntellisenseDeclaration {}';
			}
			$str = '<?php ' . "\n\n" . join("\n\n", $contents) . "\n ?>\n";
			$str .= $this->getConstants();

			return file_put_contents($file, $str);
		}

		public static function getScopedSkeleton(): string
		{
			$reflection = new ReflectionClass('Module_Skeleton');

			return $reflection->getName();
		}

		protected function getConstants(): string
		{
			return (string)file_get_contents(storage_path('constants.php'));
		}
	}