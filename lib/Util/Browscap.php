<?php

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	use BrowscapPHP\Browscap;

	class Util_Browscap
	{
		const FILE_LOCATIONS = [
			'/etc/browscap.ini',
			FILESYSTEM_TEMPLATE . '/siteinfo/etc/browscap.ini'
		];
		private const SCRATCH_PATH = 'tmp/browscap.ini';

		/**
		 * browscap.org is mostly broken
		 */
		public const CACHE_VALIDITY = 432000;

		/**
		 * Update wrapper
		 *
		 * @return bool
		 * @throws ReflectionException
		 */
		public static function update(): bool
		{
			$uid = array_get(posix_getpwnam(APNSCP_SYSTEM_USER), 'uid', 0);
			$oldUid = posix_geteuid();
			try {
				$savePath = storage_path(self::SCRATCH_PATH);
				$mtime = file_exists($savePath) ? filemtime($savePath) : 0;
				posix_seteuid($uid);
				$ret = self::updateWrapper();
				posix_seteuid($oldUid);
				if (!$ret) {
					return false;
				}
				// update file removed by tmpfiles.d
				if (!file_exists($savePath) || filemtime($savePath) === $mtime) {
					return true;
				}
				$srcfp = fopen($savePath, 'r');
				foreach (static::FILE_LOCATIONS as $copy) {
					rewind($srcfp);
					$destfp = fopen($copy, 'w');
					if (stream_copy_to_stream($srcfp, $destfp)) {
						info("Copied browscap.ini to `%s'", $copy);
					}
					fclose($destfp);
				}
				fclose($srcfp);
			} finally {
				posix_seteuid($oldUid);
			}

			return true;
		}

		private static function updateWrapper(): bool
		{
			$file = storage_path('cache/browscap');
			$marker = $file . '/.marker';
			$cacheAdapter = new \Doctrine\Common\Cache\FilesystemCache($file);
			//$cache = new \BrowscapPHP\Cache\BrowscapCache($cacheAdapter);
			$cache = new \Roave\DoctrineSimpleCache\SimpleCacheAdapter($cacheAdapter);
			$logger = new \Monolog\Logger('stdout');
			$release = (new \BrowscapPHP\Cache\BrowscapCache($cache, $logger))->getReleaseDate();
			if ($release && file_exists($marker) && (time() - filemtime($marker)) < self::CACHE_VALIDITY) {
				return true;
			}

			$logger->info('browscap: started fetching remote file');
			$updater = new BrowscapPHP\BrowscapUpdater($cache, $logger);
			$savePath = storage_path(self::SCRATCH_PATH);
			try {
				$updater->fetch($savePath);
				$logger->info('browscap: finished fetching remote file');

				if (!file_exists($savePath)) {
					return true;
				}
				$updater->convertFile($savePath);
			} catch (\BrowscapPHP\Exception $e) {
				return error('Failed to update browscap.ini: %s', $e->getMessage());
			}

			if (!file_exists($savePath)) {
				return warn("browscap.ini missing in `%s'", $file);
			}

			touch($marker);
			return true;
		}

		public static function getBrowser() {
			$file = storage_path('cache/browscap');
			$cacheAdapter = new \Doctrine\Common\Cache\FilesystemCache($file);
			//$cache = new \BrowscapPHP\Cache\BrowscapCache($cacheAdapter);
			$cache = new \Roave\DoctrineSimpleCache\SimpleCacheAdapter($cacheAdapter);
			$logger = new \Monolog\Logger('stdout');
			return (new BrowscapPHP\Browscap($cache, $logger))->getBrowser();
		}
	}
