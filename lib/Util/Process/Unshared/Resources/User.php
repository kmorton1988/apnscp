<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2020
 */


namespace Util\Process\Unshared\Resources;

use Util\Process\Unshared\Contracts\Unshareable;

class User implements Unshareable {

	public function __construct()
	{ }

	public function __toString(): string
	{
		return ' -r';
	}

	public function getInitializer(): ?string
	{
		return null;
	}

}