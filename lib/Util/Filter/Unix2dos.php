<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Util_Filter_Unix2dos extends php_user_filter
	{

		function filter($in, $out, &$consumed, $closing)
		{
			$last = null;
			$return = ord("\r");
			while (false != ($bucket = stream_bucket_make_writeable($in))) {
				$tokens = explode("\n", $bucket->data);
				$data = array();
				$it = new ArrayIterator($tokens);
				while ($it->valid()) {
					$token = $it->current();
					$len = strlen($token);
					if ($len-- > 0 && $return === ord($token[$len])) {
						$token = substr($token, 0, $len);
						$it->offsetSet($it->key(), $token);
						$bucket->datalen++;
					}
					$it->next();
				}
				$bucket->data = join("\r\n", iterator_to_array($it));
				$consumed += $bucket->datalen;

				stream_bucket_append($out, $bucket);
			}

			return PSFS_PASS_ON;
		}
	}

?>