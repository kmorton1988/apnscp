<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Util_PHP
	{
		/**
		 * @var string
		 */
		private static $key = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

		public static function array_diff_assoc_recursive($a1, $a2)
		{
			return static::array_udiff_assoc_recursive($a1, $a2, static function ($a, $b) {
				return $a <=> $b;
			});
		}

		public static function is_resource($sock): bool
		{
			if (PHP_VERSION_ID < 80000) {
				return \is_resource($sock);
			}

			return is_object($sock) && $sock instanceof Socket;
		}

		public static function array_udiff_assoc_recursive($a1, $a2, \Closure $cb) {
			$d = array();
			foreach ($a1 as $k => $v) {
				if (is_array($v)) {
					if (!isset($a2[$k]) || !is_array($a2[$k])) {
						$d[$k] = $v;
					} else {
						$new_diff = self::array_udiff_assoc_recursive($v, $a2[$k], $cb);
						if (!empty($new_diff)) {
							$d[$k] = $new_diff;
						}
					}
				} else if (!array_key_exists($k, $a2) || $cb($v, $a2[$k])) {
					$d[$k] = $v;
				}
			}

			return $d;
		}

		public static function array_diff_key_recursive($a1, $a2)
		{
			$d = array();
			foreach ($a1 as $k => $v) {
				if (is_array($v)) {
					if (!isset($a2[$k]) || !is_array($a2[$k])) {
						$d[$k] = $v;
					} else {
						$new_diff = self::array_diff_key_recursive($v, $a2[$k]);
						if (!empty($new_diff)) {
							$d[$k] = $new_diff;
						}
					}
				} else if (!array_key_exists($k, $a2)) {
					$d[$k] = $v;
				}
			}

			return $d;
		}

		/**
		 * Safe unserialize
		 *
		 * Deny all class unserialization unless opted in
		 * @{link https://www.owasp.org/index.php/PHP_Object_Injection}
		 *
		 * @param      $data
		 * @param bool $allowed_classes
		 * @return mixed
		 */
		public static function unserialize($data, $allowed_classes = false)
		{
			return unserialize($data, ['allowed_classes' => $allowed_classes]);
		}

		/**
		 * PHP 7.1.3 bug prevents lchown/lchgrp from working in ZTS
		 *
		 * @param string $filename
		 * @param string $user
		 * @return mixed
		 */
		public static function lchown($filename, $user)
		{
			if (!PHP_ZTS) {
				return lchown($filename, $user);
			}

			return Util_Process_Safe::exec('chown -h %s %s', $user, $filename)['success'];
		}

		public static function lchgrp($filename, $user)
		{
			if (!PHP_ZTS) {
				return lchgrp($filename, $user);
			}

			return Util_Process_Safe::exec('chgrp -h %s %s', $user, $filename)['success'];
		}

		public static function random_int(int $min = PHP_INT_MIN, int $max = PHP_INT_MAX): int
		{
			return random_int($min, $max);
		}

		/**
		 * PHP 7.x ZTS regression
		 *
		 * @param string $filename
		 * @return bool
		 *
		 * . will always refer to the current directory and a link
		 * cannot be a directory, thus \Util_PHP::is_link("/home/symlink/.")
		 * should be false as in non-ZTS builds
		 */
		public static function is_link(string $filename): bool
		{
			clearstatcache(true, $filename);
			if (!PHP_ZTS) {
				return is_link($filename);
			}

			return $filename[-1] !== '.' && is_link($filename);
		}

		/**
		 * Compares array1 against array2 and returns values in
		 * array1 not present in array2 with a fill value
		 *
		 * Functionally equivalent to array_diff_key ->
		 *  array_filter(is_bool) -> array_fill($default)
		 *
		 * @param array $array1
		 * @param array $array2
		 * @param mixed $default
		 * @return array
		 */
		public static function array_diff_flag_fill(array $array1, array $array2, $default): array
		{
			return array_fill_keys(array_keys(array_filter(
				array_diff_key($array1, $array2), '\is_bool')), $default
			);
		}

		/**
		 * Convert value into arbitrary base
		 *
		 * @param string $value
		 * @param int    $base
		 * @return string
		 */
		public static function base_encode(string $value, int $base = 62): string
		{
			$r = (int)$value % $base;
			$result = self::$key[$r];
			$q = floor((int)$value / $base);
			while ($q) {
				$r = $q % $base;
				$q = floor($q / $base);
				$result = self::$key[$r] . $result;
			}

			return $result;
		}

		/**
		 * Decode value from base
		 *
		 * @param string $value
		 * @param int    $base
		 * @return string
		 */
		public static function base_decode(string $value, int $base = 62): string
		{
			$limit = strlen($value);
			$result = strpos(self::$key, $value[0]);
			for ($i = 1; $i < $limit; $i++) {
				$result = $base * $result + strpos(self::$key, $value[$i]);
			}

			return $result;
		}
	}