<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Module\Support\Personality;

	class Pagespeed extends Helpers\Base
	{
		const PRIORITY = 5;
		const NAME = 'pagespeed';

		protected $_tokens = array(
			'ModPagespeedEnableFilters'  => array(
				'help' => 'Add additional PageSpeed filters.'
			),
			'ModPagespeedAnalyticsID'    => array(
				'help' => 'Setup a Google Analytics ID.'
			),
			'ModPagespeedDisableFilters' => array(
				'help' => 'Remove enabled PageSpeed filters.'
			),
			'ModpageSpeed'               => array(
				'help' => 'Toggle PageSpeed support on or off.'
			)
		);

		public function test($directive, $val = null)
		{
			$dirtest = strtolower($directive);
			if ($dirtest === 'modpagespeed') {
				return parent::_testFlag($directive, $val);
			}
		}
	}