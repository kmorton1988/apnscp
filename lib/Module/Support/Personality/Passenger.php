<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Module\Support\Personality;

	class Passenger extends Helpers\Base
	{

		const PRIORITY = 2;
		const NAME = 'passenger';

		protected $_tokens = array(
			'PassengerAppEnv' => [
				'help'   => 'Application environment to pass to app. Usually "production" or "development".',
			],
			'PassengerAppRoot' => array(
				'help' => 'Configure the application root for this particular Passenger application. Roots are usually one directory below public/.'
			),
			'PassengerAppType' => [
				'help' => 'Set Passenger application type.',
				'values' => ['rack', 'wsgi', 'node', 'meteor']
			],
			'PassengerEnabled' => [
				'help' => 'Enable or disable Passenger, on or off.',
				'values' => ['on', 'off']
			],
			'PassengerNodejs' => [
				'help'   => 'Full path to Node interpreter',
			],
			'PassengerStartupFile' => [
				'help'   => 'Application startup file relative to application root.',
			],
			'PassengerRuby'    => array(
				'help' => 'Set the Ruby interpreter to use for this application.'
			),
			'PassengerNodejs'  => array(
				'help' => 'Set the Node interpreter to use for this application.'
			),
			'PassengerPython'  => array(
				'help' => 'Set the Python interpreter to use for this application.'
			),
		);

		public function personalityName()
		{
			return self::NAME;
		}

		public function getPriority()
		{
			return self::PRIORITY;
		}

		public function resolves($token)
		{
			return \array_key_exists($token, $this->_tokens);
		}
	}
