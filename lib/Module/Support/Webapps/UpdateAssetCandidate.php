<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2018
	 */

	declare(strict_types=1);

	namespace Module\Support\Webapps;

	use Lararia\Jobs\Job;

	class UpdateAssetCandidate extends UpdateCandidate
	{
		/**
		 * known asset types + function maps
		 */
		const KNOWN_ASSETS = [
			'plugin' => [
				'update' => 'update_plugins',
				'query'  => 'plugin_status'
			],
			'theme'  => [
				'update' => 'update_themes',
				'query'  => 'theme_status'
			]
		];

		/**
		 * @var string asset type
		 */
		protected $assetType;

		/**
		 * @var string asset name
		 */
		protected $assetName;

		/**
		 * Set asset type to upgrade
		 *
		 * @param string $type
		 */
		public function setAssetType(string $type): void
		{
			if (!isset(self::KNOWN_ASSETS[$type])) {
				fatal("unknown asset type `%s'", $type);
			}
			$this->assetType = $type;
		}

		public function getAssets(): array
		{
			$cmd = $this->appType . '_' .
				array_get(static::KNOWN_ASSETS, $this->assetType . '.query');
			$info = $this->__call($cmd, [$this->getHostname(), $this->getPath()]);
			if (!\is_array($info)) {
				warn("failed to query %s %s data on `%s/%s'",
					$this->appType,
					$this->assetType,
					$this->getHostname(),
					$this->getPath()
				);

				return [];
			}

			uksort($info, static function ($a, $b) {
				return strnatcmp($a, $b);
			});

			return $info;
		}

		public function logJob(string $baseversion)
		{
			info('%s %s batch: new asset upgrade task - %s (%s, %s) %s %s -> %s',
				Job::ICON_INFO,
				$this->getSite(),
				$this->getBaseUri(),
				parent::getAppType(),
				$this->assetType,
				$this->getAssetName(),
				$baseversion,
				$this->getVersion()
			);
		}

		/**
		 * Get asset name
		 *
		 * @return null|string
		 */
		protected function getAssetName(): ?string
		{
			return $this->assetName;
		}

		/**
		 * Set name of asset to upgrade
		 *
		 * @param string $name
		 */
		public function setAssetName(string $name)
		{
			$this->assetName = $name;
		}

		public function getAppType(): string
		{
			return $this->getAssetName();
		}

		public function isUpdatable(): bool
		{
			if (!parent::isUpdatable()) {
				return false;
			}

			return !$this->{$this->appType . '_asset_skipped'}(
				$this->getHostname(),
				$this->getPath(),
				$this->assetName,
				$this->assetType
			);
		}


		/**
		 * Run update
		 *
		 * @return bool
		 */
		public function process(): bool
		{
			if (!$this->getNextVersion()) {
				return error('unable to process upgrade, next version could not be calculated');
			}

			if (!$this->assetType) {
				return error('no asset type set');
			}

			debug('Upgrade command: %s %s %s [name:%s,version:%s]',
				$this->getUpgradeCommand(),
				$this->getHostname(),
				$this->getPath(),
				$this->getAssetName(),
				$this->getNextVersion()
			);

			return $this->apnscpFunctionInterceptor->call(
				$this->getUpgradeCommand(),
				[
					$this->getHostname(),
					$this->getPath(),
					[['name' => $this->assetName, 'version' => $this->getNextVersion()]]
				]
			);
		}

		/**
		 * Get upgrade command
		 *
		 * @return string
		 */
		public function getUpgradeCommand(): string
		{
			return $this->appType . '_' . array_get(static::KNOWN_ASSETS, $this->assetType . '.update');
		}
	}