<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2017
	 */

	namespace Module\Support\Webapps;

	use Module\Support\Webapps;

	abstract class Magento extends Webapps
	{
		use \FilesystemPathTrait;

		public function disableSecureHttp(): bool
		{
			$ret = \a23r::init()->pman_run('composer config --global secure-http false');

			return $ret['success'];
		}

		public function useSecureHttp(): bool
		{
			$ret = \a23r::init()->pman_run('composer config --global secure-http');

			return (bool)trim($ret['output']);
		}

		public function enableSecureHttp(): bool
		{
			$ret = \a23r::init()->pman_run('composer config --global secure-http true');

			return $ret['success'];
		}

		protected function guessMajor(string $docroot): ?int
		{
			$approot = \dirname($docroot);
			if (file_exists($this->domain_fs_path($approot . '/bin/magento'))) {
				return 2;
			}
			// Mage 1.7.x, Magento >= 1.8.x
			if (file_exists($this->domain_fs_path($docroot . '/lib/Magento')) || file_exists($this->domain_fs_path($docroot . '/lib/Mage'))) {
				return 1;
			}

			return null;
		}

		protected function fixSymlinkTraversal(string $path): bool
		{
			$file = $this->domain_fs_path() . $path . '/.htaccess';
			if (!file_exists($file)) {
				return warn('.htaccess missing from Magento - bad install?');
			}
			$contents = file_get_contents($file);
			$contents = preg_replace('/^(\s*Options \+FollowSymLinks)\s*$/', '$1 -SymLinksIfOwnerMatch', $contents);

			return (bool)$this->file_put_file_contents($path . '/.htaccess', $contents);
		}

	}
