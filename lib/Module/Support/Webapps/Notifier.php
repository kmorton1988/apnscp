<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2020
 */

namespace Module\Support\Webapps;

class Notifier {
	use \apnscpFunctionInterceptorTrait;
	use \ContextableTrait;

	public const PREFKEY = 'webapps.notify';
	public const NOTIFICATION_MODES = [
		'none'     => 'Never',
		'always'   => 'Always',
		'fail'     => 'On failure'
	];

	protected function __construct()
	{ }

	public function getEmail(): ?string {
		if ($this->getAuthContext()->level & PRIVILEGE_ADMIN) {
			return CRM_COPY_ADMIN;
		}
		return $this->common_get_email() ?: null;
	}

	/**
	 * Should notify given permissions
	 *
	 * @param bool $success
	 * @return bool
	 */
	public function notify(bool $success): bool
	{
		$notifier = $this->getNotification();

		if ($notifier === 'always') {
			return true;
		}

		if ($notifier === 'none') {
			return false;
		}

		// $notifier = 'fail'
		return !$success;
	}

	/**
	 * Get notification type
	 *
	 * @return bool|string
	 */
	public function getNotification()
	{
		$prefs = \Preferences::factory($this->getAuthContext());
		return array_get($prefs, self::PREFKEY, 'always');
	}

	/**
	 * Set notification behavior
	 *
	 * @param string $param
	 * @return self
	 */
	public function setNotification($param): self
	{
		if (!isset(self::NOTIFICATION_MODES[$param])) {
			fatal("Unknown notification mode `%s'", $param);
		}
		$prefs = \Preferences::factory($this->getAuthContext())->unlock($this->getApnscpFunctionInterceptor());
		data_set($prefs, self::PREFKEY, $param);
		return $this;
	}
}