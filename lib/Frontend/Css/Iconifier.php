<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2020
 */

namespace Frontend\Css;

class Iconifier {

	/**
	 * Get process icon from name
	 *
	 * @param $name
	 * @return string
	 */
	public static function getProcessIconFromName($name)
	{
		switch ($name) {
			case 'imap':
			case 'pop3':
				return 'envelope';
			case 'bash':
			case 'ksh':
			case 'zsh':
				return 'terminal';
			case 'php-fpm':
				return 'php-w';
			case 'ruby':
				return 'ruby';
			case 'Passenge':
				return 'truck';
			case 'python':
				return 'python';
			case 'perl':
				return 'perl';
			case 'mysql':
				return 'mysql';
			case 'sshd':
				return 'ssh';
			case 'vsftpd':
				return 'cloud-upload';
			case 'redis-server':
				return 'redis';
			case 'postgres':
				return 'postgres-w';
			default:
				return 'cogs';
		}
	}
}
