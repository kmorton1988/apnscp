<?php
	require_once dirname(__FILE__) . '/../TestFramework.php';

	class RecursiveDiffTest extends TestFramework
	{
		public function testSimpleDiff()
		{
			$a = [
				'a' => ['b', 'c', 'd', 'e' => ['f'], 'g' => 'h'],
				'b' => 'c',
				0
			];
			$b = [
				'a' => ['a', 'c', 'd', 'e' => ['g'], 'h' => 'u'],
				'b' => 'c',
				2
			];
			$this->assertEquals(['a' => ['b', 'e' => ['f'], 'g' => 'h'], 0],
				\Util_PHP::array_diff_assoc_recursive($a, $b));
		}
	}


