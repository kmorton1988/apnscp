<?php

	use Opcenter\Account\Ephemeral;
	use Opcenter\Net\Firewall\Delegated;
	use Opcenter\Net\Firewall\Ipset;
	use Opcenter\SiteConfiguration;

	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class WhitelistTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

		/**
		 * Test basic rampart:whitelist usage
		 */
		public function testManipulation()
		{
			if (!SiteConfiguration::getModuleFromService('rampart')) {
				return $this->markTestSkipped('Rampart module missing');
			} else if (!version_compare(APNSCP_VERSION, '3.1', '>=')) {
				return $this->markTestSkipped('Delegated whitelisting v3.1+');
			}
			$addr1 = long2ip(mt_rand(1, 2**32-1));
			$addr2 = long2ip(mt_rand(1, 2 ** 32 - 1));

			$account = Ephemeral::create([
				'rampart' => [
					'enabled'   => true,
					'max'       => null,
					'whitelist' => [$addr1, $addr2]
				]
			]);
			$afi = $account->getApnscpFunctionInterceptor();
			if (!$afi->rampart_can_delegate()) {
				return $this->markTestSkipped('Platform or account version does not support delegated whitelisting');
			}
			$this->assertContains($addr1, $afi->rampart_get_delegated_list(), "Delegated whitelist contains ${addr1}");
			$addr3 = long2ip(mt_rand(1, 2**32-1));
			$this->assertTrue($afi->rampart_whitelist($addr3, 'add'), "${addr3} has been added to whitelist");
			$this->assertContains($addr3, array_column(Ipset::getSetMembers(Delegated::IPSET_NAME), 'host'), "${addr3} reflected in ipset");
			$this->assertTrue($afi->rampart_whitelist($addr3, 'remove'), "${addr3} has been removed from whitelist");
			for ($i = 0; $i < 10; $i++) {
				$set = array_column(Ipset::getSetMembers(Delegated::IPSET_NAME), 'host');
				if (!\in_array($addr3, $set, true)) {
					break;
				}
				sleep(1);
			}

			$this->assertNotContains($addr3, $set, "${addr3} not reflected in ipset");

			$account->destroy();
			$list = array_column(Ipset::getSetMembers(Delegated::IPSET_NAME), 'host');
			$this->assertNotContains($addr1, $list, "${addr1} has been removed from whitelist");
			$this->assertNotContains($addr2, $list, "${addr2} has been removed from whitelist");
		}

		/**
		 * Test rampart:whitelist handles CIDR
		 */
		public function testRangeValidity()
		{
			if (!SiteConfiguration::getModuleFromService('rampart')) {
				return $this->markTestSkipped('Rampart module missing');
			}
			// /32 converts to just the address, /31 requires even number
			$addr = long2ip(mt_rand(1, 2 ** 32 - 1) & 0xFFFE) . '/31';
			$context = \Auth::context(\Auth::get_admin_login());
			$afi = \apnscpFunctionInterceptor::factory($context);
			$this->assertEquals(PRIVILEGE_ADMIN, $context->level, 'Operating within admin context');
			$this->assertTrue($afi->rampart_whitelist($addr), 'Whitelist ' . $addr);
			$this->assertContains($addr, array_column(Ipset::getSetMembers(Delegated::IPSET_NAME), 'host'),
				"${addr} reflected in ipset");
			$this->assertTrue($afi->rampart_whitelist($addr, 'remove'), "Remove ${addr} from whitelist");
			$this->assertNotContains($addr, array_column(Ipset::getSetMembers(Delegated::IPSET_NAME), 'host'),
				"${addr} has been removed");
			try {
				$afi->rampart_whitelist("1.1.1.1/33");
				$this->fail("Unreachable condition");
			} catch (\apnscpException $e) {
				$this->assertContains('1.1.1.1', $e->getMessage());
			}
		}
	}

