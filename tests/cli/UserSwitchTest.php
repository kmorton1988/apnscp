<?php
    require_once dirname(__DIR__, 1) . '/TestFramework.php';

    class UserSwitchTest extends TestFramework
    {
        protected $filename;

        public function testSwitch()
        {
            // root, appl admin
            $c = \cli\cmd();
            $profile = \Auth::profile();
            $this->assertTrue($c->context_matches_id($profile->id), 'spawned afi ID matches active profile');
            // debug, site admin
            $new = \cli\cmd(null, array_get(Definitions::get(), 'auth.site.domain'));
            $newprof = \Auth::profile();
            $this->assertTrue($new->context_matches_id($newprof->id), 'new afi overwrites existing profile');
            $this->assertNotEquals($newprof->id, $profile->id, 'profiles differ');
            $this->assertNotEquals($newprof->level, $profile->level);
            $this->assertEquals($newprof->id, session_id(), 'session ID synchronized');

        }
    }

