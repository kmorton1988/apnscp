<p class="help">
	{{ $scope->getHelp() }}. Dangerous directives are marked <b>READONLY</b>. You may not edit
	these values from the frontend as a mistake could be catastrophic. Restart {{ PANEL_BRAND }}
	when changes are completed to activate.
</p>