@switch($setting)
	@case('bool')
	@case('boolean')
		@php $type = 'bool'; @endphp
		@break
	@case (\is_array($setting))
		@php
			$type = 'enum';
			$name = $scope->getName();
		@endphp
		@break
	@case (\is_array($value) || $setting === 'array')
		@php $type = 'array'; @endphp
		@break
	@case ('double')
	@case ('float')
		@php
			$type = 'decimal';
			$name = $name ?? 'args[]';
		@endphp

	@case ('integer')
	@case ('int')
		@php
			$type = 'numeric';
			$name = $name ?? 'args[]';
		@endphp
		@break
	@default
		@if (is_debug())
			UNKNOWN TYPE {{ $setting }}
		@endif
	@case('string')
		@php
			if (false !== strpos($value, "\n")) {
				$type = 'multiline';
			}
		@endphp
	@case('NULL')
	@case('mixed')
		@php
			$name = $name ?? 'args[]';
		@endphp
		@break
@endswitch

@includeFirst([
	'partials.scope-internals.type-' . ($type ?? ''),
	'partials.scope-internals.type-generic'
],
[
	'value' => $value,
	'name'  => $name ?? 'args',
	'attrs' => $attrs ?? ''
])