<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */

	namespace apps\scopes\models\scopes\Apache;
	use apps\scopes\models\Scope;
	use apps\scopes\RenderAsHtmlTrait;

	class PhpMulti extends Scope {
		use RenderAsHtmlTrait;
		// need better way of managing this dynamically...
		const VERSIONS = [
			'5.6', '7.0', '7.1', '7.2', '7.3', '7.4'
		];

		public function __construct(string $name)
		{
			parent::__construct($name);
			foreach (static::VERSIONS as $v) {
				if (!isset($this->value[$v])) {
					$this->value[$v] = 'disabled';
				}
			}
			ksort($this->value);
		}

		public function getHelp()
		{
			$help = parent::getHelp() .
				"<br /><span class='font-weight-bold'>Package builds are silently ignored</span>. See <a href='https://docs.apiscp.com/admin/PHP-FPM#remi-php'>Remi builds</a> in PHP-FPM.md.";
			return $this->asHtml($help);
		}
	}