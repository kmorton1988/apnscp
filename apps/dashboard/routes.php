<?php declare(strict_types=1);

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
	 */

	use Illuminate\Support\Facades\Route;
	use Lararia\Http\JsonResponse;


	Route::get('revertid/{id}', 'Page@revertHijack');

	Route::post('hijack', static function (Page $p) {
		return $p->hijack(request()->post('hijack'));
	});

	Route::get('', 'Page@index');

	Route::get('processes', 'Page@getProcesses');

	Route::prefix('rampart')->group(static function() {
		Route::post('render', static function() {
			return view('glances.rampart', [
				'entries' => request()->post('entries'),
				'full' => \Preferences::get('dashboard.rampart-full', false)
			]);
		});
		Route::post('flush', static function () {
			$jail = request()->post('jail');

			return new JsonResponse(\cmd('rampart_flush', $jail));
		});

		Route::post('unban', static function () {
			return new JsonResponse(\cmd('rampart_unban', request()->post('ip')));
		});

		Route::post('ban', static function () {
			return new JsonResponse(\cmd('rampart_ban', request()->post('ip'), 'blacklist'));
		});

		Route::get('all', static function() {
			return \cmd('rampart_get_jail_entries', null);
		});

	});

	Route::post('argos', static function() {
		return view('glances.argos');
	});

