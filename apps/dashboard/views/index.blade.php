@extends('theme::layout')

@section('content')
	@includeWhen(UCard::get()->hasPrivilege('admin') && \Opcenter\License::get()->isTrial(), 'partials.demo-expiration')
	@include('analytics-container')
	@includeWhen(UCard::get()->hasPrivilege('site') && SCREENSHOTS_ENABLED, 'glances.webapps')

	@includeWhen(!\Ucard::is('admin'), 'processes')
	@if (UCard::is('admin'))
		<div id="argos">
			@include('glances.argos')
		</div>
	@endif
	@include('overview')
	@include('partials.modals.overage')
    @include('partials.modals.blacklist')
@endsection