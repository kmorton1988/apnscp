<form method="post">
	<table width="100%" class="table">
		<thead>
		<tr>
			<th class="">
				Database Name
			</th>
			<th class="hidden-sm-down">
				Extension
			</th>
			<th class="hidden-sm-down">
				Frequency (days)
			</th>
			<th class="">
				Limit
			</th>
			<th class="">
				Next Backup
			</th>
			<th class="hidden-sm-down">
				E-mail
			</th>
			<th class="center">
				Actions
			</th>
		</tr>
		</thead>
		<?php
        $backups = $Page->list_backups();
		if ($backups):
		$i = 0;
		foreach ($backups as $db_name => $backup_params):
		?>
		<tr>
			<td class="">
				<?=$db_name?>
			</td>
			<td class="hidden-sm-down">
				<span editable="<?=$i?>"><?=$Page->backup_program($backup_params['extension'])?></span>
			</td>
			<td class="hidden-sm-down">
				<span editable="<?=$i?>"><?=$backup_params['span']?></span>
			</td>
			<td class="">
				<span editable="<?=$i?>"><?=$backup_params['hold']?></span>
			</td>
			<td class="">
				<span editable="<?=$i?>"><?=date('Y-m-d',$backup_params['next'])?></span>
			</td>
			<td class="hidden-sm-down">
				<span editable="<?=$i?>"><?=$backup_params['email'] ? "Yes" : "" ?></span>
			</td>
			<td class="center">
				<!--<a href="#" OnClick="return make_editable(this,<?=$i?>);"><img src="/images/actions/edit_icon.gif" alt="Edit" border="0" /></a>&nbsp;&nbsp;-->
				<a href="#" class="btn btn-primary ui-action-label btn warn ui-action ui-action-delete"
				   OnClick="return confirm('Are you sure you would like to delete the job on <?=$db_name?>?') && apnscp.assign_url('<?=\HTML_Kit::page_url()?>?d=<?=base64_encode($db_name)?>');">
					Delete
				</a>
			</td>
		</tr>
		<?php
                $i++;
            endforeach;
        ?>
		<?php
        endif;
    ?>
	</table>

	<h3>Add Task</h3>
	<?php

    $dbs = $Page->get_dbs();
	if (!$dbs):
	?>
	<div class="alert alert-info">
		No databases found - create one through
		<a class="ui-action ui-action-switch-app ui-action-label"
		   href="/apps/change<?=$Page->getMode()?>"><?=ucwords($Page->getMode())?> Manager</a>
	</div>
	<?php
    else:
?>
	<div id="add-backup" class="row mt-1">

		<fieldset class="form-group col-12 col-md-4 col-lg-2">
			<label for="database_name">Database</label>
			<select class="form-control custom-select" name="database_name" id="database_name">
				<?php foreach ($Page->get_dbs() as $db) { printf('
				<option value="%s">%s</option>
				'."\n",$db, $db); } ?>
			</select>
		</fieldset>

		<fieldset class="form-group col-12 col-md-4 col-lg-2">
			<label for="extension">Compression Type</label>
			<select name="extension" id="extension" class="form-control custom-select">
				<option value="bz">bzip</option>
				<option value="gz">gzip</option>
				<option value="zip" selected="SELECTED">zip</option>
				<option value="none">none</option>
			</select>
		</fieldset>

		<fieldset class="form-group col-12 col-md-4 col-lg-2">
			<label for="frequency">Backup Frequency</label>
			<div class="input-group">
				<input type="text" class="form-control" name="frequency" size="3" maxlength="3" id="frequency"
				       value="7"/>
				<span class="input-group-addon">days</span>
			</div>

		</fieldset>

		<fieldset class="form-group col-12 col-md-4 col-lg-2">
			<label for="hold">Maximum Backups</label>
			<input type="text" size="3" maxlength="3" name="hold" id="hold" value="3" class="form-control"/>

		</fieldset>

		<fieldset class="form-group col-12 col-md-4 col-lg-2">
			<label class="hidden-sm-down col-12">&nbsp;</label>
			<label for="email">
				<input class="checkbox-inline" data-toggle="tooltip"
				       title="Sent to email address on file (Acccount &gt; Settings)" type="checkbox" name="email"
				       id="email" value=""/>
				Email Confirmation
			</label>
		</fieldset>

		<fieldset class="form-group col-12 col-md-4 col-lg-2">
			<label class="hidden-sm-down col-12">&nbsp;</label>
			<input type="submit" name="Add" value="Add Backup" class="btn primary"/>
		</fieldset>
	</div>
	<?php
    endif;
?>
</form>
