<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\whois;

	use Page_Container;

	class Page extends Page_Container
	{
		protected $Whois_Record;

		public function __construct()
		{
			parent::__construct();
			$this->Whois_Record = null;
		}

		public function on_postback($params)
		{


		}

		public function getWhoisRecord($domain)
		{
			$output = $this->dns_get_whois_record($domain);
			$this->bind($output);
			if (is_string($output)) {
				return $output;
			}

		}
	}

	?>