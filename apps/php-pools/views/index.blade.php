@php
	// @var PoolStatus $pool
	$pool = $Page->activePool();
@endphp

<div class="mb-3">
	<form method="post">
		<input type="hidden" name="php-pool" value="{{ $Page->getName() }}" />
		<h4>{{ $Page->getName() }}</h4>
		<div class="btn-group" role="group" aria-label="Pool actions">
			<div class="btn-group" role="group">
				<button id="btnGroupState" type="button" class="btn btn-secondary dropdown-toggle"
				        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				</button>
				<div class="dropdown-menu" aria-labelledby="btnGroupState">
					<button type="submit"
					        class="dropdown-item ui-action btn-block ui-action-restore ui-action-label btn-block warn"
					        name="restart" value="{{ $Page->getName() }}">
						Restart
					</button>
					@if ($pool->running())
						<button type="submit"
						        class="dropdown-item ui-action btn-block ui-action-disable ui-action-label btn-block warn"
						        name="stop" value="{{ $Page->getName() }}">
							Stop
						</button>
					@else
						<button type="submit"
						        class="dropdown-item ui-action btn-block ui-action-enable ui-action-label btn-block"
						        name="start" value="{{ $Page->getName() }}">
							Start
						</button>
					@endif
				</div>
				<h6 class="mb-0 d-flex align-self-center ml-2 text-uppercase">
					@if ($pool->running())
						<i class="ui-action-is-enabled text-success"></i> Running (PID {{ $pool->getPid() }})
					@else
						<i class="ui-action-is-disabled"></i> Stopped
					@endif
				</h6>
			</div>
		</div>

		<table class="table table-condensed table-responsive">
			<tr>
				<th class="pt-4">Owner</th>
				<td>
					@if (HTTPD_FPM_USER_CHANGE)
						<div class="btn-group">
							<select name="pool-owner" class="custom-select">
								@php
									$activeUser = \cmd('user_get_username_from_uid', $pool->getUser());
								@endphp
								@foreach([cmd('common_get_service_value', 'siteinfo', 'admin_user'), cmd('web_get_sys_user')] as $user)
									@continue(!\cmd('user_exists', $user))
									<option value="{{ $user }}" @if ($activeUser === $user) selected="SELECTED" @endif>
										{{$user}}
									</option>
								@endforeach
							</select>
							<button type="submit" name="change-owner" class="btn btn-secondary ui-action ui-action-save ui-action-label ui-action-run" value="{{ $activeUser }}">
								Change Owner
							</button>
						</div>
					@else
						{{ \cmd('web_get_sys_user') }}
					@endif
				</td>
			</tr>
			@includeWhen(false, 'partials.cache-config', ['config' => $Page->getCacheConfig()])
			@if ($pool->running())
				<tr>
					<th>Uptime</th>
					<td>
						@php
							$duration = Formatter::time(time() - $pool->getStart());
						@endphp
						@if (!$duration)
							<span class="badge badge-success">NEW</span>
							<em>just started</em>
						@else
							{{ $duration }}
						@endif
					</td>
				</tr>
				@includeWhen($duration, 'partials.perf-data')
			@endif
		</table>
	</form>
</div>