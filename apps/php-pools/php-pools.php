<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
 */


	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\php_pools;


	use Opcenter\Http\Php\Fpm\PoolStatus;
	use Opcenter\Map;

	class Page extends \Page_Container
	{
		use \FilesystemPathTrait;

		// @var PoolStatus $pool
		protected $pool;

		/**
		 * Get active pool
		 *
		 * @return PoolStatus
		 */
		public function activePool(): PoolStatus
		{
			if (isset($this->pool)) {
				return $this->pool;
			}

			if (isset($_GET['pool'])) {
				$poolId = $_GET['pool'];
			} else {
				$poolId = array_get($this->php_pools(), 0, []);
			}

			$this->pool = new PoolStatus($this->php_pool_info($poolId));

			return $this->pool;
		}

		/**
		 * Get pool name formatted for site
		 *
		 * @return string
		 */
		public function getName(): string
		{
			return substr($this->activePool()->getName(), strlen($this->getAuthContext()->site . '-'));
		}

		public function getCacheConfig() {
			return $this->activePool()->getCacheMetrics($this->getAuthContext());
		}

		public function on_postback($params)
		{
			return parent::dispatchPostback($params, $params['php-pool']);
		}

		public function managePool(string $pool, array $params): bool
		{
			if (isset($params['restart'])) {
				return $this->getApnscpFunctionInterceptor()->call('php_pool_restart', [$pool]);
			}

			if (isset($params['start']) || isset($params['stop'])) {
				$state = isset($params['start']) ? 'start' : 'stop';

				return $this->getApnscpFunctionInterceptor()->call('php_pool_set_state', [$pool, $state]);
			}
			return false;
		}

		public function handleRestart(string $pool, array $params = []): bool
		{
			return $this->managePool($pool, $params);
		}

		public function handleStop(string $pool, array $params = []): bool
		{
			return $this->managePool($pool, $params);
		}

		public function handleChangeOwner(string $pool, array $params = []): bool
		{
			return $this->php_pool_change_owner($params['pool-owner']);
		}
	}