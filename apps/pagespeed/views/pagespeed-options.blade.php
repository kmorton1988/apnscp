<form method="POST" action="/apps/pagespeed/apply/{{ $Page->getDomain() }}" id="filterForm">
	<table class="table table-striped thead-dark w-100" id="superclass">
		<thead>
		<tr class="">
			<th colspan="2">
				<h3 class="mb-0 align-items-center d-flex">
					Composite Filters
				</h3>
			</th>
		</tr>
		</thead>
		<tbody>
			@foreach ($Page->getSuperclassFilters() as $filter => $superclass)
				<tr>
					<td colspan="2">
						<label class="custom-switch custom-control mb-0 pl-0 mr-0">
							<input type="checkbox" @if ($Page->filterEnabled($filter)) checked @endif
								class="custom-control-input" name="superclass" value="{{ $filter }}"
								data-filters="{{ implode(' ', $superclass['filters']) }}"/>
							<span class="custom-control-indicator"></span>
							{{ $superclass['name'] }}
						</label>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						{{ $superclass['help'] }}
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	@foreach ($Page->getAvailableFilters() as $group)
		@include('partials.filter-group')
	@endforeach
</form>