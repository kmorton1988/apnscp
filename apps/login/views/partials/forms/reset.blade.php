@if (!isset($_GET['challenge']))
	<div class="text-right mb-1">
		<i class="fa fa-user"></i>
		<a href="" class="" id="showUserTarget" data-toggle="collapse" data-target="#showUser">
			Reset Specific User
		</a>
	</div>
	<div class="form-group">
		<div class="input-icon fa fa-cloud"></div>
		<input autofocus="autofocus" class="form-control" id="domain" name="domain"
			tabindex="1" type="text"/>
		<label for="domain" class="placeholder">{{ _("Domain") }}</label>
	</div>
	<div class="collapse  w-100" id="showUser">
		<div class="alert alert-info">
			<i class="fa fa-sticky-note-o"></i>
			Specific username resets only work if the user has an
			email address configured via <b>Account</b> &gt; <b>Settings</b>.
		</div>
		<div class="form-group">
			<div class="input-icon fa fa-user"></div>
			<input class="form-control" id="username" name="username"
				 tabindex="1" type="text"/>
			<label for="username" class="placeholder">{{ _("Username") }}</label>
		</div>
	</div>
	<button class="btn btn-primary btn-lg btn-block" name="pw_req" tabindex="2" type="submit"
	        value="Log In">Submit
	</button>
@elseif ($Page->errors_exist() || $Page->get_severity() !== Error_Reporter::E_INFO)
	<div class="form-group">
		<label for="email">Token</label>
		<div class="input-icon fa fa-font"></div>
		<input autofocus="true" class="form-control" id="token" name="challenge"
		       placeholder="Challenge Token"
		       tabindex="1" type="text"/>
	</div>
	<button class="btn btn-primary btn-lg btn-block" name="pw_req" tabindex="2" type="submit"
	        value="Log In">Submit
	</button>
@endif