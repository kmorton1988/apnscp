var edit_ctr = 0;
var user_list = $('<select class="mailbox form-control new custom-select" name=""></select>');
var options = '';

$(document).ready(function () {
	// multiselect transforms the transport select control into
	// multiple inputs, save a copy
	apnscp.hinted();

	$('[data-toggle=tooltip]').tooltip();
	$('#bulk-add').click(function () {
		$('#bulk-add-form').slideToggle(function () {
		});
		return false;
	});

	$('#cloneMailboxes').click(function() {
		return clone_mail();
	});
	$('input.catchall').click(function () {
		if (parseInt($(this).val())) {
			$('#username, #forwarded-column input').prop('disabled', true);
			$('#type_single').triggerHandler('click');
		} else {
			$('#username, #forwarded-column input').prop('disabled', false);
		}
		return true;
	});
	$('option[value=""]', domains).remove();
	/**
	 * @TODO fix collapse event deselecting radio when radio control is
	 * clicked directly
	 */

	$('#deliveryType').click(function (e) {
		if (e.target.nodeName.toUpperCase() === "INPUT") {
			$(e.target).prop('checked', true).triggerHandler('change');
		} else {
			$(':input', e.target).prop('checked', true).triggerHandler('change');
		}
		return true;
	});
	$('input[type=radio][data-toggle=radio-collapse]').radioCollapse();
	$('#alias_local').multiSelect({
		noneSelected: "select user(s)",
		oneOrMoreSelected: "% user(s) included"
	});
	$('#transport').multiSelect({
		noneSelected: "select domain(s)",
		oneOrMoreSelected: "% domain(s) selected"
	});

	$('#type_forwarded').one('focus', function () {
		$('#alias_remote').html("");
		$('#alias_remote').css('color', '#333');
	});
	for (user in users)
		options += '<option value="/home/' + users[user] + '/Mail">' + users[user] + '</option>';
	user_list.html(options);

	$('.filter-mail').click(function (e) {
		if (e.target.id == "do_filter") {
			var field = $('#filter_spec').val(),
				pattern = $('#filter').val(),
				re = new RegExp(pattern, "i"),
				selector = 'td.' + field;
			$('#mailbox_routes tr.entry').filter(function () {
				return re.test($(this).children(selector).text()) == false;
			}).fadeOut('fast', function () {
				$(this).removeClass('ui-highlight').find(':checkbox').prop('checked', false);
			});
			var $el = $('<li class="filter-' + field + '" />').text(pattern).prepend($('<i class="ui-action ui-action-delete"></i>')).click(function () {
				return remove_filter.apply(this, [pattern, $(this).data("selector")]);
			});
			$el.data("selector", selector);
			$(this).find('.ui-active-filters').append($el);
			if ($('#reset_filter:hidden').length > 0) {
				$('#reset_filter').fadeIn();
			}
			return false;
		}
		if (e.target.id == "reset_filter") {
			$(this).find('.ui-active-filters').empty();
			$('#reset_filter').hide();
			return $('#mailbox_routes tr.entry:hidden').fadeIn();
		}
	}).bind('keydown', function (e) {
		if (e.keyCode === $.ui.keyCode.ESCAPE) {
			$('#reset_filter').click();
			$('#filter').val('');
		} else if (e.keyCode === $.ui.keyCode.ENTER) {
			$(this).triggerHandler('click');
		}
	});
	;
	$('#username').focus();
});

$(window).on('load', function () {

	$('#mailbox_routes tbody tr, #disabled_mailbox_routes tbody tr').highlight();
	$('#mailbox_routes tbody tr').mouseenter(function () {
		if ($('td span.type', $(this)).text() == "F") {
			var $dest = $('td.mailbox span span', $(this));
			recs = $dest.text().split(/,\s*/g);
			if (recs.length < 2) return;
			var $fl = $('<ul class="forward-list" ><li class="fl-head">User</li><li class="fl-head">Domain</li></ul>');
			var $expanded = $('<span class="forward-overflow"/>').append($fl);
			for (i = 0; i < recs.length; i++) {
				var domain = "";
				var user = recs[i];
				var isep = recs[i].indexOf('@');
				if (isep > 0) {
					domain = recs[i].substring(isep + 1);
					user = recs[i].substring(0, isep);
				}
				$fl.append($('<li />').text(user)).append($('<li/>').text(domain));
			}
			var pos = $dest.position();
			$expanded.css({top: pos.top, left: pos.left});
			$dest.addClass('forward-hide').after($expanded);
			$(this).one('mouseleave', {
				$d: $dest
			}, function (ev) {
				var $d = ev.data.$d;
				$d.removeClass('forward-hide').next().empty().remove();
				$d = null;
			});
		}
	});
	$('#disabled_mailbox_routes tbody tr td:last').unbind('click');


	$("a.ui-action-edit").click(function (e) {
		return edit_row(e);
	});
	var toggled = 1;
	$('#select-all').click(function () {
		var $el = $('#mailbox_routes tbody tr td :checkbox');
		if (toggled) {
			$el.prop('checked', true)
			$el.parent().parent().addClass('ui-highlight');
		} else {
			$el.prop('checked', false);
			$el.parent().parent().removeClass('ui-highlight');
		}
		toggled ^= 1;
		return true;
	});


	$('#edit-save').click(function () {
		var usernew = $(SELECTORS.user.set).val(),
			domainnew = $(SELECTORS.domain.set).val(),
			destination = $(SELECTORS.destination.set + ':visible :input').val(),
			type = $(SELECTORS.type.set).find(':input:checked').val();

		var params = [originalValues.user, originalValues.domain, usernew, domainnew, destination, type];
		$('#editModal').find('.text-danger').remove()
		apnscp.cmd('email_rename_mailbox', params).fail(function (data) {
			var tmp = data.responseText, errs = $.parseJSON(tmp);
			$('#edit-save').after($('<div class="ajax-error text-xs-left text-danger">Failed: ' + (errs ? errs.errors.join('<br />') : tmp) + '</div>'));

		}).done(function (data) {

			$('#editModal').modal('hide');
			var $msg = $("<span><i class=\"ui-ajax-indicator ui-ajax-success\" style='float:left;'></i> address changed</span>");
			apnscp.addMessage($msg, 'success', '');
			$activeRow.find(SELECTORS.user.get).text(usernew);
			$activeRow.find(SELECTORS.domain.get).text(domainnew);
			$activeRow.find(SELECTORS.destination.get).text(destination).data('mailbox-type', type).removeClass('ui-action-email-forward ui-action-email-local ui-action-email-special').addClass(typeToClass(type));
		});

		return false;
	});

	$('#mailbox_routes, #disabled_mailbox_routes').tablesorter();
	/* {headers: {5: { sorter: false}, 0: {sorter: false}}} );*/
});

function clone_mail() {

	var $btn, dialog = apnscp.modal($('#cloneMailboxModal'), {
		buttons:
			[
				$btn = $('<button>').attr({
					'type': 'submit',
					name: 'clone-mailboxes',
					'class': 'btn btn-primary',
					value: 1,
					'id': 'confirmRecordSubmit'
				}).prop({
					'disabled': true
				}).text("Clone Mailboxes").click(function () {
					$('.modal-content form').submit();
				})
			]
	});

	dialog.on('show.bs.modal', function () {
		$('select', this).on('change', function () {
			if (this.name === "target-domain") {
				$('#cloneMailboxModal .dest').text(this.value);
			} else {
				$('#cloneMailboxModal .source').text(this.value);
			}
			$('#cloneMailboxModal #confirmRecordImport').prop(
			{
				'checked': false,
				'disabled': $('#cloneMailboxModal .dest').text() == $('#cloneMailboxModal .source').text()
			});
			if ($btn) {
				$btn.prop('disabled', true);
			}

		}).each(function() {
			$(this).triggerHandler('change');
		});

		var $indicator = $('<i class="ml-2"></i>'),
			$btn = dialog.find('.btn-primary').find('.ui-ajax-indicator').remove().end().append($indicator);
		dialog.find('.ui-ajax-error-msg').remove();

		$(':checkbox', dialog).on('change', function () {
			var $btn = $('.modal-footer :submit');
			$btn.prop('disabled', !$(this).prop('checked'));
		});
		$('form', dialog).on('submit', function () {
			$btn.data('old-title', $(this).text()).text('Processing').append($indicator.addClass('ui-ajax-loading'));
		});
	}).modal('show');
}

function typeToClass(type) {
	type = type.toLowerCase();
	if (type == TYPES.special) {
		return 'ui-action-email-special';
	} else if (type == TYPES.local) {
		return 'ui-action-email-local';
	} else {
		return 'ui-action-email-forward';
	}
}

function remove_filter(pattern, selector) {
	var re = new RegExp(pattern, "i")
	$('tr.entry:hidden').filter(function () {
		return re.test($(this).children(selector).text()) == false;
	}).fadeIn('fast');

	$(this).remove();
	if ($('.ui-active-filters li').length == 0) {
		$('#reset_filter').fadeOut();
	}
	return false;
}

var SELECTORS = {
	user: {
		get: '.user span',
		set: '#username'
	},
	domain: {
		get: '.domain span',
		set: '#domain'
	},
	type: {
		get: 'td.mailbox',
		set: '#deliveryType'
	},
	destination: {
		get: '.mailbox span',
		set: '#deliveryOptions .panel'
	}
};

var TYPES = {
	forward: 'a',
	local: 'v',
	special: 's'
};

var hideMe = null, $activeRow = null;

var originalValues = {
	user: null,
	domain: null
};

function edit_row(e) {
	$activeRow = $(e.currentTarget).closest('.entry');
	var user = $activeRow.find(SELECTORS.user.get).text().trim(),
		domain = $activeRow.find(SELECTORS.domain.get).text().trim(),
		email = user + '@' + domain,
		type = $activeRow.find(SELECTORS.type.get).data('mailbox-type').toLowerCase().trim(),
		typelc = type.toLowerCase(),
		destination = $activeRow.find(SELECTORS.destination.get).text().trim(),
		$modal = $('#editModal').find('.text-danger').remove().end();
	$('#edit-email-name').text(email);
	originalValues.user = user;
	originalValues.domain = domain;
	$modal.find(SELECTORS.user.set).val(user);
	$modal.find(SELECTORS.domain.set).find('[value="' + domain + '"]').prop('selected', true);
	var show = $modal.find(SELECTORS.type.set + ' :radio[value="' + typelc + '"]').click().data('target');
	if (hideMe !== null) {
		$(hideMe).removeClass('in');
	}
	$(show).addClass('in');
	hideMe = $(show);
	if (typelc === TYPES.forward) {
		$modal.find(SELECTORS.destination.set + ' .forward-delivery').val(format_aliases(destination));
	} else {
		$modal.find(SELECTORS.destination.set + ' .local-delivery').find(':selected').prop('selected', false).end().find('[value="' + destination + '"]').prop('selected', true);
	}

	$modal.on('shown.bs.modal', function () {
		$(SELECTORS.destination.set + ' :input:visible').focus()
	});
	return true;
}

function format_aliases(aliases) {
	aliases = aliases.replace(/,/g, "\r\n").replace(/\s\s+/g, "\r\n");
	return aliases;
}
