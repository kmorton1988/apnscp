<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\bandwidthstat;

	use DateTime;
	use Page_Container;
	use PHPStats\ProbabilityDistribution\StudentsT as StudentsT;
	use PHPStats\Stats as Stats;

	class Page extends Page_Container
	{
		// minimum cycles to switch to monthly analysis
		const MINIMUM_CYCLES = 5;

		// various intervals of confidence
		private $_intervals = array(
			'.95' => "Probable",
			'.85' => "Best-case",
			'.99' => "Worst-case",


			'.80'   => "Lower Extreme",
			'.9999' => "Upper Extreme",
		);

		private $_statMode = 'month';

		private $_bandwidth = array();

		public function __construct()
		{
			parent::__construct();

			$this->init_js('flot', 'flot.errorbars', 'flot.navigate', 'flot.dashed', 'flot.grow', 'flot.time');
			$this->add_css('bandwidthstat.css');
			$this->add_javascript('bandwidthstat.js');
			$stats = $this->getStats($this->getBandwidthData());
			// pull from \UCard, which is cached
			$bw = \UCard::get()->getBandwidth();
			$statJS = array(
				'mode'      => $this->_statMode,
				'stats'     => $stats,
				'limit'     => $this->getBandwidthQuota() / 1024 / 1024,
				'predictor' => '.95',
				'cycle'     => $this->common_get_service_value('bandwidth', 'rollover'),
				'used'      => $bw['used']
			);
			$code = 'var stats = ' . json_encode($statJS) . ';';
			$this->add_javascript($code, 'internal', false, false);
		}

		public function getStats(array $bwdata, $mode = null)
		{
			if (count($bwdata) < self::MINIMUM_CYCLES) {
				return array();
			}
			$statsClass = new Stats;
			if (!$mode) {
				$mode = $this->_statMode;
			}
			$stats = array();
			$pick = function (array $a1, $what) {
				$new = array();
				while (false !== ($tmp = current($a1))) {
					$new[] = $tmp[$what];
					next($a1);
				}

				return $new;
			};

			$sigmain = $pick($bwdata, "in");
			$sigmaout = $pick($bwdata, "out");
			$sigmatotal = $pick($bwdata, "total");
			$n = count($bwdata);
			$multiplier = 1;
			if ($mode == 'day') {
				$multiplier = 365.25 / 12;
			}
			$stats = array(
				'total'    => $sigmatotal,
				'df'       => $n - 1,
				'n'        => $n,
				'mean'     => $statsClass->average($sigmatotal) * $multiplier,
				's'        => $statsClass->sampleStddev($sigmatotal) * $multiplier,
			);

			$tests = new StudentsT($stats['df']);
			$cdfs = array();
			foreach ($this->getIntervals() as $interval) {
				$t = $tests->ppf($interval + (1 - $interval) / 2);
				$norm = $stats['s'] / sqrt($stats['n']);
				$cdfs[$interval] = array(
					'tcrit' => $t,
					'lower' => ($stats['mean'] - $t * $norm),
					'upper' => ($stats['mean'] + $t * $norm)
				);
			}

			$stats['tscores'] = $cdfs;

			return $stats;
		}

		public function getIntervals()
		{
			return array_keys($this->_intervals);
		}

		/**
		 * Fetch and detect appropriate interval for bandwidth analysis
		 *
		 * @param bool $fillgaps zero missing contiguous data
		 *
		 * @return array
		 */
		public function getBandwidthData($fillgaps = false)
		{
			if ($this->_bandwidth) {
				return $this->_bandwidth;
			}
			// period collections fewer than 5 months results in less data
			// in calculation, fallback to day
			$cycles = $this->bandwidth_get_cycle_periods();
			if (count($cycles) < self::MINIMUM_CYCLES) {
				$grouping = 'day';
			} else {
				$grouping = 'month';
			}
			$this->_statMode = $grouping;
			$bw = $this->bandwidth_get_all_composite_bandwidth_data($grouping);
			$bw = $this->_populatePoints($bw, $fillgaps);
			$this->_bandwidth = $bw;

			return $bw;
		}

		/**
		 * Get all bandwidth points from first span to last span
		 *
		 * @param array $data
		 */
		private function _populatePoints(array $data, $fillgaps = false)
		{
			if (count($data) < 1) {
				return array();
			}

			// final, populated points
			$populated = array();
			$populated[] = array(
				'end'   => $data[0]['end'],
				'begin' => $data[0]['begin'],
				'in'    => $data[0]['in_bytes'] / 1024 / 1024,
				'out'   => $data[0]['out_bytes'] / 1024 / 1024,
				'total' => ($data[0]['in_bytes'] + $data[0]['out_bytes']) / 1024 / 1024,
			);
			$bwprev = array();
			for ($i = 1, $n = count($data); $i < $n; $i++) {
				$bwprev = $data[$i - 1];
				$bw = $data[$i];
				// gap should be 0 indicating cycle began
				// as soon as last ended
				$gap = $bw['begin'] - $bwprev['end'];
				if ($gap > 0 && $fillgaps) {
					$populated = array_merge($populated, $this->_zeroPoints($bw, $bwprev));
				}

				$in = $bw['in_bytes'] / 1024 / 1024;
				$out = $bw['out_bytes'] / 1024 / 1024;
				$begin = $bw['begin'];
				$end = $bw['end'];
				$populated[] = array(
					'in'    => $in,
					'out'   => $out,
					'begin' => $begin,
					'end'   => $end,
					'total' => $in + $out
				);
			}

			return $populated;
		}

		/**
		 * Fill in missing bandwidth spans
		 *
		 * @param array $bwts
		 * @param array $bwtsold
		 * @return array
		 */
		private function _zeroPoints($bwts, $bwtsold)
		{
			if ($this->_statMode == 'month') {
				$step = '+1 month';
			} else {
				$step = '+1 day';
			}
			$beginDate = new DateTime('@' . $bwtsold['end']);
			$endDate = new DateTime('@' . $bwtsold['end']);
			$endDate->modify($step);
			while (true) {
				$points[] = array(
					'in'    => 0,
					'out'   => 0,
					'begin' => $beginDate->getTimestamp(),
					'end'   => $endDate->getTimestamp(),
					'total' => 0,
				);
				$beginDate->setTimestamp($endDate->getTimestamp());
				$endDate->modify($step);
				if ($beginDate->getTimestamp() > $bwts['begin']) {
					break;
				}
			}

			return $points;
		}

		/**
		 * Get bandwidth threshold in bytes
		 *
		 * @return int
		 */
		public function getBandwidthQuota()
		{
			return \Formatter::changeBytes(
				(int)$this->common_get_service_value('bandwidth', 'threshold'),
				'MB',
				$this->common_get_service_value('bandwidth','units')
			);
		}

		public function getIntervalsWithLabels()
		{
			return $this->_intervals;
		}

		/**
		 *
		 * @return string mode (month or day)
		 */
		public function getMode()
		{
			return $this->_statMode;
		}

		/**
		 *
		 * @return DateTime object
		 */
		public function getMonthChangeOverPoint()
		{
			$periods = $this->bandwidth_get_cycle_periods();
			$first = array_shift($periods);
			$dt = new DateTime('@' . $first['begin']);
			$dt->modify('+' . self::MINIMUM_CYCLES . ' months');

			return $dt;

		}
	}

?>
