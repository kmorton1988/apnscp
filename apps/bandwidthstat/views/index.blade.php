<?php
$bandwidth = $Page->getBandwidthData() ;
$stats = $Page->getStats($bandwidth);
$bwquota = $Page->getBandwidthQuota();
$color = 'listodd';
?>
<!-- main object data goes here... -->

<?php if (count($bandwidth) < \apps\bandwidthstat\Page::MINIMUM_CYCLES): ?>
<div class="alert alert-info text-lg missing-data">
	<i class="fa fa-calendar fa-2x"></i>
	Oops, there isn't enough data on this account yet!
	Come back next month after we have collected some data for you.
</div>
<?php return; endif; ?>

<h3 class="center">Bandwidth Usage</h3>
<div id="bandwidth-graph" class="mb-3"></div>
<?php
            if ($Page->getMode() == 'day') {
?>
<p class="note mt-1 d-block mb-2">
	Bandwidth predictions will improve on <?php print $Page->getMonthChangeOverPoint()->format('F
	j\<\s\u\p\>S\<\/\s\u\p\>, Y'); ?>
</p>
<?php
            }
        ?>

<h5>Based upon your statistics, the projected bandwidth for this month is:</h5>

<table width="100%" class="table">
	<thead>
	<tr>
		<th class="left">Probability</th>
		<th class="right">Range (MB)</th>
		<th class="right">% Utilized</th>
		<th class="right hidden-md-down minor-info">Standard Error</th>
		<th class="right hidden-md-down minor-info">Conf. Interval</th>
		<th class="right hidden-md-down minor-info">t-critical</th>
	</tr>
	</thead>
	<?php
        $size = sizeof($bandwidth);
        foreach ($Page->getIntervalsWithLabels() as $ci => $label) {
	$tstats = $stats['tscores'][$ci];
	$tcrit = $tstats['tcrit'];
	$lower = $tstats['lower'];
	$upper = $tstats['upper'];
	$cssLabel = str_replace(' ','-',strtolower($label));
	?>
	<tr class="<?php print($cssLabel.' '.$color); ?>">
		<td class="left"><?php print($label); ?></td>
		<td class="right">
			<?php
                    if ($lower <= 0 ) {
                        print '&lt; ';
                    } else {
                        print number_format($lower,2).' MB &ndash; ';
                    }
                    print number_format($upper,2) . ' MB';
                ?>
		</td>
		<td class="right">
			<?php
                    if ($lower <= 0 ) {
                        print '&lt; ';
                    } else {
                        print number_format($lower/$bwquota*100,2) . '% &ndash; ';
                    }
                    print number_format($upper/$bwquota*100,2) . '%';
                ?>
		</td>
		<td class="right hidden-md-down minor-info">
			<?php print(\Formatter::commafy(sprintf("%.2f MB",$tcrit*$stats['s']/sqrt($size)))); ?>
		</td>
		<td class="right hidden-md-down minor-info"><?php printf("%.2f %%",$ci*100); ?></td>
		<td class="right hidden-md-down minor-info">
			<?php printf("%.2f",$tcrit); ?>
		</td>
	</tr>
	<?php
            if ($color == 'listodd') $color = 'listeven'; else $color = 'listodd';
        }
    ?>
</table>

<h5><i class="fa fa-calendar"></i> Historic bandwidth data</h5>
<table class="table">
	<thead>
	<tr>
		<th width=150 class="">Date Span</th>
		<th align=center class="hidden-md-down right ">In (MB)</th>
		<th align=center class="hidden-md-down right">Out (MB)</th>
		<th align=center class="right">Aggregate (MB)</th>
		<th align=center class="right">Change (MB)</th>
		<th align=center class="hidden-md-down right">t-score</th>
	</tr>
	</thead>
	<tbody>
	<?php
        $previous = array();
        $change = null;
        for($i = 0, $n = sizeof($bandwidth); $i < $n; $i++) {
            $in    = $bandwidth[$i]['in'];
            $out   = $bandwidth[$i]['out'];
            $total = $in + $out;
            $begin = date('Y-m-d', $bandwidth[$i]['begin']);
            $end   = date('Y-m-d', $bandwidth[$i]['end']);
            if ($i > 0) {
	$change = ($total-$previous['in']-$previous['out']);
	}
	?>
	<tr class="<?php print($color); ?>">
		<td class="left" width=30%">
			<a href="/apps/bandwidthbd?span=<?=$bandwidth[$i]['begin'].'%2C'.$bandwidth[$i]['end']?>"><?php print($begin.' &ndash; '.$end); ?></a>
		</td>
		<td class="hidden-md-down right">
			<?php print(\Formatter::commafy(sprintf("%.2f MB",$in))); ?>
		</td>
		<td class="hidden-md-down right">
			<?php print(\Formatter::commafy(sprintf("%.2f MB",$out))); ?>
		</td>
		<td class="right">
			<?php print(\Formatter::commafy(sprintf("%.2f MB",$total))); ?>
		</td>
		<td class="right <?php if ($previous) print ($change > 0 ? 'positive' : 'negative'); ?>">
			<?php
                        if ($previous) {
                            print(\Formatter::commafy(sprintf("%.2f MB", $change)));
                            if ($change > 0) {
			print '<i class="fa fa-caret-up"></i>';
			} else {
			print '<i class="fa fa-caret-down"></i>';
			}
			} else {
			print "&ndash;";
			}
			?>
		</td>
		<td class="right hidden-md-down ">
			<?php ($stats['s'] > 0 ) ? printf("%.2f",($total - $stats['mean'])/$stats['s']/sqrt($n)) : print('N/A'); ?>
		</td>
	</tr>
	<?php
            if ($color == 'listodd') $color = 'listeven'; else $color = 'listodd';
            $previous = array(
                'begin' => $begin,
	'end' => $end,
	'in' => $in,
	'out' => $out
	);
	}
	?>
	</tbody>
</table>

<h5 class="text-center">
	Additional statistics from calculation
</h5>
<div class="row center">
	<div class="col-4 head1">
		&mu;
	</div>
	<div class="col-4 head1">
		s
	</div>
	<div class="col-4 head1">
		df
	</div>
</div>
<div class="row center">
	<div class="col-4">
		<?php print(\Formatter::commafy(sprintf("%.2f MB",
            $stats['mean'])));
        ?>
	</div>

	<div class="col-4">
		<?php print(\Formatter::commafy(sprintf("%.2f MB",
            $stats['s'])));
        ?>
	</div>

	<div class="col-4">
		<?php print(\Formatter::commafy(sprintf("%.2f MB",
            $stats['df'])));
        ?>
	</div>
</div>
