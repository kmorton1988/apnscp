<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\sitemap;

	use Page_Container;

	class Page extends Page_Container
	{

		public function __construct()
		{
			parent::__construct();
			$this->setTitle("Application Index");
			$this->add_css('/apps/sitemap/sitemap.css');
			\Page_Renderer::hide_breadcrumbs();
		}

		public function on_postback($params)
		{

		}

		public function fetch_te()
		{
			return $this->templateClass;
		}

		public function app_short2()
		{
			$apps = $this->app_short();
			$pageviews = \UCard::get()->getPref('pageview');
			$appnew = array();
			foreach ($apps as $appcat) {
				foreach ($appcat['apps'] as $app) {
					$views = 0;
					$appname = $app->getName();
					$appid = $app->getId();
					if (isset($pageviews[$appid])) {
						$views = $pageviews[$appid];
					}
					$appnew[] = array(
						'cat'  => $appcat['name'],
						'name' => $appname,
						'view' => $views,
						'tag'  => $app['tag'],
						'link' => $app['link']
					);
				}
			}
			uasort($appnew, function ($a, $b) {
				if ($a['view'] == $b['view']) {
					$sort = strcmp($a['cat'], $b['cat']);
					if ($sort != 0) {
						return $sort;
					}

					return strcmp($a['name'], $b['name']);
				}

				return $a['view'] < $b['view'];
			});
			$appsorted = array();
			foreach ($appnew as $appsort) {
				$appsorted[] = array(
					'name' => $appsort['cat'],
					'apps' => array($appsort)
				);
			}

			return $appsorted;
		}

		public function app_short()
		{
			$apps = array();
			while (false !== ($cat = $this->getCategory())) {
				$catName = $cat->getName();
				if (!$catName) {
					$catName = 'Main';
				}
				$catRef = $cat->getInternal();
				$catApps = array('name' => $catName, 'apps' => array());
				while (false !== ($app = $this->getLink($catRef))) {
					$appRef = $app->getInternal();
					$appLink = $app->getLink();
					$appName = $app->getName();
					$appTag = $this->getHelpTagline($appRef);
					$catApps['apps'][] = array('id'   => $appRef,
											   'name' => $app->getName(),
											   'tag'  => $appTag,
											   'link' => $appLink
					);
				}
				$apps[] = $catApps;
			}

			return $apps;
		}

		public function getCategory()
		{
			return $this->templateClass->get_category();
		}

		public function getLink($category)
		{
			return $this->templateClass->get_link($category);
		}
	}

	?>