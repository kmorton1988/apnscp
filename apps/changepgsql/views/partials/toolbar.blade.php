<div class="row">
	<div class="col-12 text-right">
		<label>Change Mode:</label>
		@if ($mode === 'add')
			<a class="ui-action ui-action-switch-app ui-action-label"
			   href="{{ \HTML_Kit::page_url_params(array('mode' => 'list')) }}"
				>List Users and Databases</a>
		@else
			<a class="ui-action ui-action-switch-app ui-action-label" href="{{ \HTML_Kit::page_url() }}"
				>Create Users and Databases</a>
		@endif
	</div>
</div>