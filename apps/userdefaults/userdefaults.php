<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\userdefaults;

	use Page_Container;

	include_once(INCLUDE_PATH . '/lib/html/userdefaults-loader.php');

	class Page extends Page_Container
	{

		private $def_options;
		// merged options -- default + local (/etc/usertemplate)
		private $options;

		public function __construct()
		{
			parent::__construct();
			$this->load_defaults();

			$this->init_js('multiselect', 'slider');

			$this->add_javascript('userdefaults.js');
			$this->add_css('/apps/useradd/useradd.css');
			$max = floor(\Formatter::changeBytes((int)$this->common_get_service_value('diskquota',
				'quota'), 'MB', $this->common_get_service_value('diskquota', 'units')));
			$this->add_javascript('var quota = {"max":' . $max . ', "default":' . $this->options['disk_quota'] . '};' . "\n" .
				'var user  = {home:"' . \Util_Conf::home_directory() . '"};',
				'internal',
				false);
		}

		protected function loadBlade(string $template = 'index', string $path = null): \BladeLite
		{
			$path = webapp_path('useradd/views');

			return parent::loadBlade($template, $path);
		}

		private function load_defaults()
		{
			$this->options = \User_Defaults::load_defaults();

		}

		public function get_options()
		{
			return $this->options;
		}

		public function get_option($name, $type = 'text', $opt_cond = null)
		{
			if (isset($this->options[$name]))
				switch ($type) {
					case 'text':
						return $this->options[$name];
					case 'checkbox':
						return $this->options[$name] ? 'checked="1"' : '';
					case 'option':
						if (is_array($this->options[$name])) {
							return in_array($opt_cond, $this->options[$name]) ? 'SELECTED' : '';
						} else {
							return $this->options[$name] == '*' ||
								($this->options[$name] === $opt_cond ? 'SELECTED' :
									'');
						}
				}

			return '';

		}

		public function get_mode()
		{
			return 'defaults';
		}

		protected function on_postback($params)
		{
			if (isset($params['save'])) {
				$this->options = array_merge(
					$this->options,
					\User_Defaults::set_defaults($params));
			}
		}

		private function save_options($params)
		{

		}
	}

?>
