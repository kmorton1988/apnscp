<div class="row">
	<div class="col-12">
		<h4 class="step">Miscellaneous Options</h4>
		<ol class="pl-0 ml-3">
			<li>
				<label class="custom-control custom-checkbox mb-0">
					<input type="checkbox" name="bayesian_filtering" value="1" class="custom-control-input"
					       @if ($bayesian_filtering) CHECKED @endif onChange="updateOpts(this);"/>

					<span class="custom-control-indicator"></span>
					<span>
						Enable <a href="{{ HTML_Kit::page_url() }}/tip/bayesian"
							class="popover-trigger" id="bayesian">Bayesian filtering</a>. <b
							class="badge badge-info text-uppercase">recommended</b>
					</span>
				</label>
			</li>

			<li>
				<label class="custom-control custom-checkbox mb-0">
					<input type="checkbox" name="bayes_auto_learn" onChange="updateOpts(this);" value="1"
					       @if ($bayes_auto_learn) CHECKED @endif class="custom-control-input" />

					<span class="custom-control-indicator"></span>
					<span>
						Enable Bayesian <a href="{{ HTML_Kit::page_url() }}/tip/auto-learn"
					        class="popover-trigger" id="auto-learn">auto-learning</a>.
					</span>
				</label>
			</li>

			<li>
				<label class="">
					<div class="form-inline">
						<span>
							Automatically learn mail as ham if it scores below
						</span>
						<input type="number" step="any"  class="mx-1 form-control" name="bayes_min_ham"
						       value="{{ $bayes_min_ham }}"
						       maxlength=5 size=5/>
						<span>
							and automatically learn mail as spam if it scores above
						</span>
						<input type="number" step="any" class="mx-1 form-control" name="bayes_min_spam" size=5 maxlength=5
						       value="{{ $bayes_min_spam }}"/>
					</div>
				</label>
			</li>

			<li>
				<label class="custom-control custom-checkbox mb-0">
					<input type="checkbox" name="bayesian_rules" value="1" class="form-control custom-control-input"
					       @if ($bayesian_rules) CHECKED @endif />

					<span class="custom-control-indicator"></span>
					<span>
						Use Bayesian
						<a href="{{ HTML_Kit::page_url() }}/tip/bayesian-rules"
					        class="popover-trigger" id="bayesian-rules">classifier points</a> in
							determining whether a message is spam or ham.
					</span>
				</label>
			</li>

			<li>
				<label>
					<div class="form-inline">
						<span>
							Activate Bayesian filtering after my database has learned
						</span>
						<input type="number" min="50" class="form-control mx-1" name="bayes_spam_activate"
						       size=3 maxlength=3 value="{{ $bayes_spam_activate }}"/>
						<span>
							spams and
						</span>
						<input type="number" min="50" class="form-control mx-1" name="bayes_ham_activate" size=3 maxlength=4
						       value="{{ $bayes_ham_activate }}"/>
						<span>
							hams.
						</span>
					</div>
				</label>
			</li>

			<li>
				<label>
					Enable <a href="{{ HTML_Kit::page_url() }}/tip/preview-safe" class="popover-trigger" id="preview-safe">safe previews</a>
					of mail that is considered spam?
				</label>
				<select name="use_safe" class="form-control custom-select">
					<option value="1" @if ($use_safe === 1) SELECTED @endif>Yes, as attachments</option>
					<option value="2" @if ($use_safe === 2) SELECTED @endif>Yes, inline</option>
					<option value="0" @if (!$use_safe) SELECTED @endif>No, display as-is</option>
				</select>
			</li>

			<li>
				<label class="custom-control custom-checkbox mb-0">
					<input type="checkbox" name="use_pyzor" value="1" @if ($use_pyzor) CHECKED
					       @endif onChange="" class="custom-control-input"/>

					<span class="custom-control-indicator"></span>
					<span>
						Enable <a href="{{ HTML_Kit::page_url() }}/tip/pyzor"
					        class="popover-trigger" id="razor">Pyzor</a> if available.
					</span>
				</label>
			</li>

			<li>
				<label class="custom-control custom-checkbox mb-0">
					<input type="checkbox" name="use_razor" value="1" @if ($use_razor) CHECKED
					       @endif onChange="" class="custom-control-input"/>

					<span class="custom-control-indicator"></span>
					<span>
						Enable <a href="{{ HTML_Kit::page_url() }}/tip/razor"
					        class="popover-trigger" id="razor">Razor</a> if available.
					</span>
				</label>
			</li>
		</ol>
	</div>
</div>