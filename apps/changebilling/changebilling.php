<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\changebilling;

	use Error_Reporter;
	use Page_Container;

	class Page extends Page_Container
	{
		const CC_TRUNCATE = 4;
		protected $postback_params = [];
		private $billing_information;
		private $card_information;

		public function __construct()
		{
			parent::__construct();
			$this->billing_information = $this->billing_get_billing_information();
			$this->card_information = $this->billing_get_credit_card_information();
			if (Error_Reporter::get_severity() == Error_Reporter::E_ERROR) {
				error(Error_Reporter::get_last_msg());
			}

			$this->add_javascript('$(\'[data-toggle="tooltip"]\').tooltip(); $("#cc_delete").click(function () {
        	    $("#cc_number").val("").prop("disabled",!!$(this).prop("checked"));
        	    return true;
        		});', 'internal');
			$this->add_css('#label-cc_delete { font-weight:normal;} .optional { color: red;}', 'internal');

		}

		public function on_postback($params)
		{
			$this->postback_params = $params;
			if (isset($params['submit'])) {
				if (!isset($params['cc_number'])) {
					$params['cc_number'] = '';
				}
				$params['cc_number'] = str_replace(' ', '', trim($params['cc_number'], 'x '));

				if (!$params['cc_number']) {
					$this->billing_change_credit_card_information((int)$params['exp_year'],
						(int)$params['exp_month'],
						$params['cvv2']
					);
					return true;
				} else if (strlen($params['cc_number']) != self::CC_TRUNCATE ||
					$params['exp_year'] . $params['exp_month'] != $params['old_exp']) {
					$this->billing_change_credit_card_information((int)$params['exp_year'],
						(int)$params['exp_month'],
						$params['cvv2'],
						$params['cc_number'],
						$params['cc_type']
					);

				}
				if ($params['info_hash'] != $this->get_hash()) {
					$this->billing_change_billing_information([
						'firstname' => $this->get_first_name(),
						'lastname'  => $this->get_last_name(),
						'company'   => $this->get_company(),
						'address'   => rtrim($this->get_address(1) . ' ' . $this->get_address(2), ' '),
						'city'      => $this->get_city(),
						'state'     => $this->get_state(),
						'zip'       => $this->get_zip(),
						'country'   => $this->get_country(),
						'phone'     => $this->get_phone(),
						'email'     => $this->get_email()
					]);
				}
			}
		}

		public function get_hash()
		{
			return sha1(serialize(array(
				'f'  => $this->get_first_name(),
				'l'  => $this->get_last_name(),
				'cp' => $this->get_company(),
				'a'  => $this->get_address(1) . ' ' . $this->get_address(2),
				'ci' => $this->get_city(),
				's'  => $this->get_state(),
				'cy' => $this->get_country(),
				'z'  => $this->get_zip(),
				'p'  => $this->get_phone(),
				'e'  => $this->get_email()
			)));
		}

		public function get_first_name()
		{
			return $this->postback_params['fname'] ?? ($this->billing_information['first_name'] ?? null);
		}

		public function get_last_name()
		{
			return $this->postback_params['lname'] ?? ($this->billing_information['last_name'] ?? null);
		}

		public function get_company()
		{
			return $this->postback_params['company'] ?? ($this->billing_information['company'] ?? null);
		}

		public function get_address($mLine)
		{
			$address = isset($this->billing_information['address']) ? explode("\n",
				wordwrap($this->billing_information['address'], 32, "\n")) : "";
			if (is_array($address)) {
				$address2 = $address[1] ?? null;
				$address1 = $address[0];
			} else {
				$address2 = null;
				$address1 = $address;
			}

			switch ($mLine) {
				case 1:
					return $this->postback_params['address1'] ?? $address1;
				default:
					return $this->postback_params['address2'] ?? $address2;
			}
		}

		public function get_city()
		{
			return $this->postback_params['city'] ?? ($this->billing_information['city'] ?? null);
		}

		public function get_state()
		{
			if (strtolower($this->get_country()) == 'us') {
				return $this->postback_params['state1'] ?? (isset($this->billing_information['state']) ? strtoupper($this->billing_information['state']) : null);
			} else {
				return $this->postback_params['state2'] ?? (isset($this->billing_information['state']) ? strtoupper($this->billing_information['state']) : null);
			}
		}

		public function get_country()
		{
			return strtolower($this->postback_params['country'] ?? ($this->billing_information['country'] ?? null));
		}

		public function get_zip()
		{
			return ($this->postback_params['zip'] ?? ($this->billing_information['zip_code'] ?? null));
		}

		public function get_phone()
		{
			return ($this->postback_params['phone'] ?? ($this->billing_information['phone'] ?? null));
		}

		public function get_email()
		{
			return ($this->postback_params['email'] ?? ($this->billing_information['email'] ?? null));
		}

		public function subscription_number()
		{
			return $this->billing_get_hosting_subscription();
		}

		public function get_payment_method()
		{
			return $this->billing_get_payment_method();
		}

		public function get_billing_information()
		{
			return $this->billing_get_billing_information();
		}

		public function get_credit_card_information()
		{
			return $this->billing_get_credit_card_information();
		}

		public function get_card_type()
		{
			return ($this->postback_params['cc_type'] ?? ($this->card_information['cc_type'] ?? null));
		}

		public function get_card_exp($mType = null)
		{
			if (isset($this->postback_params['exp_month'])) {
				$exp_month = $this->postback_params['exp_month'];
				$exp_year = $this->postback_params['exp_year'];
			} else {
				$exp_year = isset($this->card_information['cc_exp']) ? substr($this->card_information['cc_exp'], 2,
					2) : null;
				$exp_month = isset($this->card_information['cc_exp']) ? substr($this->card_information['cc_exp'],
					-2) : date('m');
			}

			switch ($mType) {
				case 'month':
					return (int)$exp_month;
				case 'year':
					return (int)$exp_year;
				default:
					return $exp_year . $exp_month;
			}
		}

		public function get_card_number()
		{
			if (isset($this->postback_params['cc_number'])) {
				return !$this->errors_exist() ? 'xx' . substr($this->postback_params['cc_number'], -self::CC_TRUNCATE,
						self::CC_TRUNCATE) : $this->postback_params['cc_number'];
			}

			return isset($this->card_information['cc_number']) ? ($this->card_information['cc_number'] ? 'xx' : '') . $this->card_information['cc_number'] : null;
		}

		public function get_cvv2()
		{
			if ($this->is_postback && (\strlen($this->postback_params['cc_number']) !== self::CC_TRUNCATE) && !$this->postback_params['cvv2']) {
				return false;
			}
			if (isset($this->postback_params['cvv2'])) {
				return $this->postback_params['cvv2'];
			}

			return (isset($this->card_information['cvm']) && $this->card_information['cvm'] ? $this->card_information['cvm'] : false);
		}

		public function renewalUri()
		{
			return $this->billing_get_renewal_link();

		}
		public function renewalHash()
		{
		    return $this->billing_get_renewal_hash();
		}
	}
