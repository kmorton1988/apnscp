<form method="post" name="toggle_cronjob">
	<h3>Service Settings</h3>
	<div class="row">
		<div class="col-12">
			<label class="custom-control align-items-center custom-switch form-control-lg pt-0 pl-0 mb-0 mb-0">
				<input type="hidden" name="toggle_cronjob" value="0" />
				<input type="checkbox" class="custom-control-input" id="disableCrons" data-toggle="collapseCheckbox"
				       data-target="#activeTasks"
				       aria-expanded="{{ $status ? 'false' : 'true' }}"
				       @if ($status) checked @endif
				       name="toggle_cronjob" value="1" />
				<span class="custom-control-indicator"></span>
				Enable Scheduled Tasks
			</label>

			<p class="">
				@if (!$status)
					Scheduled task support is currently disabled. Enable to schedule tasks
					to run in the background on your account.
				@else
					Scheduled tasks are currently enabled on this account. Disable the task scheduler
					to disable background processing of all tasks for all users.
				@endif
			</p>
		</div>

		<div class="col-12">
			<label class="form-control-label mr-3">
				Email
				<div class="input-group">
					<span class="input-group-addon fa fa-envelope-o"></span>
					<input type="text" name="mailto" class="form-control" id="mailto" value="{{ $Page->getMailto() }}" />
				</div>
			</label>
			<label class="custom-control custom-checkbox mb-0">
				<input type="checkbox" class="custom-control-input" id="disableMail"
				       name="mailto" value="" @if (!$Page->getMailto()) checked @endif />
					<span class="custom-control-indicator"></span>
					Disable email
			</label>
			<p>
				All cron output will be sent to this email address. Separate multiple addresses with a comma (",").
				Cron output may be optionally suppressed as well by disabling mail.
			</p>
			<button class="btn btn-primary" type="submit" name="save">
				Save Changes
			</button>
		</div>
	</div>
</form>