<form method="post" name="cronjobs" id="addCronjob">
	<h3>Add Task</h3>

	<h4 class="mb-0">Command</h4>
	<fieldset class="form-group">
		<div class=" row">
			<div class="col-12 col-md-7 col-lg-6 mt-2">
				<div class="input-group">
					<div class="input-group-addon">
						<i class="fa fa-terminal"></i>
					</div>
					<input type="text" name="cmd" id="cmd" class="form-control" placeholder="echo 'Hello World!'"/>
				</div>
			</div>
			<div class=" col-12 col-md-5 col-xl-3 mt-2">
				<div class="btn-group">
					<button class="btn btn-primary" type="submit" name="Add_Cronjob" value="1" id="add">
						<i class="ui-action-add ui-action"></i> Add Task
					</button>
					<button class="btn btn-secondary" type="button" name="run" id="run">
						<i class="fa fa-cogs"></i> Run Now
					</button>
				</div>
			</div>
		</div>
	</fieldset>

	<h4>Scheduling</h4>
	<fieldset class="form-inline times">
		<div class="input-group">
			<div class="input-group-addon">
				<i class="fa fa-clock-o"></i>
			</div>
			<select name="time" class="form-control custom-select" id="selectTime">
				<option value="*/5 * * * *">Every 5 Minutes</option>
				<option value="*/30 * * * *">Every 30 Minutes</option>
				<option value="@hourly">Hourly</option>
				<option value="@daily">Daily</option>
				<option value="@weekly">Weekly</option>
				<option value="@monthly">Monthly</option>
				<option value="@reboot">Server Start</option>
				<option value="@custom" id="schedule-custom"
				        href="#custom-time" value="custom" aria-expanded="false" aria-controls="custom-time">
					Use Custom Time
				</option>
			</select>
		</div>
	</fieldset>

	<div class="collapse mt-1" id="custom-time">
		<h5>Custom Times</h5>
		<p class="">
			<i class="fa fa-sticky-note-o"></i>
			Time format follows <a href="https://apisnetworks.com/linux-man/man5/crontab.5.html">crontab syntax</a>.
			Jobs will be calibrated to your
			<a href="/apps/changeinfo" class="ui-action-switch-app ui-action-label ui-action"
				>active timezone</a>.
		</p>

		<div class="row">
			<div class="col-12 col-sm-4 col-md-3 col-lg-2">
				<label for="add-minute" class="hinted">0,5,10</label>
				<label class="">
					Minute
				</label>
				<input type="text" name="minute" id="add-minute" class="form-control"/>
			</div>

			<div class="col-12 col-sm-4 col-md-3 col-lg-2">
				<label for="add-hour" class="hinted">*/2</label>
				<label class="">
					Hour
				</label>
				<input type="text" name="hour" id="add-hour" class="form-control"/>
			</div>

			<div class="col-12 col-sm-4 col-md-3 col-lg-2">
				<label for="add-dom" class="hinted">1-31/5</label>
				<label class="">
					Day of Month
				</label>
				<input type="text" name="dom" id="add-dom" class="form-control"/>
			</div>

			<div class="col-12 col-sm-4 col-md-3 col-lg-2">
				<label for="add-month" class="hinted">1-12</label>
				<label class="">
					Month
				</label>
				<input type="text" name="month" id="add-month" class="form-control"/>
			</div>

			<div class="col-12 col-sm-4 col-md-3 col-lg-2">
				<label for="add-dow" class="hinted">1-7</label>
				<label class="">
					Day of Week
				</label>
				<input type="text" name="dow" id="add-dow" class="form-control"/>
			</div>
		</div>
	</div>

	<div class="row small mt-1">
		@include('partials.timezone')
	</div>
</form>