$('document').ready(function () {
	$('#ui-app input[data-toggle="collapseCheckbox"]').each(function (index, item) {
		var $item = $(item),
			$target = $($item.data('target'));
		$('input:checkbox[name="' + item.name + '"]').on('change', function () {
			var $that = $(this);
			if ($item.is(':checked')) {
				$target.collapse('show').on('shown.bs.collapse', function () {
					$that.prop('checked', true);
				});
			} else {
				$target.collapse('hide');
			}
		});
	});
});
$(window).on('load', function () {
	$('.task .ui-action-run').ajaxWait({reset: false}).click(function (e) {
		var cmd = $(this).data('command') ?
			$(this).data('command') : $($(this).data('src')).text();
		return runCmd(cmd);
	});

	$('#disableMail').change(function() {
		$('#mailto').prop('disabled', $(this).prop('checked'));
		return true;
	}).change();
	apnscp.hinted();

	$('tr.entry').highlight();
	var SCHEDULE_CUSTOM = 'schedule-custom';
	$('#selectTime').change(function (e) {
		var selected = $(':selected', this).get(0);
		if (selected.id != SCHEDULE_CUSTOM) {
			$($('#' + SCHEDULE_CUSTOM).attr('href')).collapse('hide');
			return true;
		}
		$($(':selected', this).attr('href')).collapse('show');
		return true;
	});

	$('.actions button').click(function (e) {
		var name = $(e.target).attr('name');
		if (name == "edit") {
			make_editable($(this).closest('.row'));
			return false;
		} else if (name == "delete") {
			$row = $(this).closest('.row');
			apnscp.call_app(null, "deleteCronjob", [$row.find(':input[name=time]').val(), $row.find(':input[name=cmd]').val()]).done(function (ret) {
				$row.fadeOut();
			});
			return false;
		}
	});
	$('#add').click(function () {
		if ($('input[name=time]:checked').val() == 'custom') {
			fields = $('input.custom-time.hinted');
			for (var i = 0; i < fields.length; i++) {
				alert("Missing value for " + $(fields[i]).attr('name'));
				$(fields[i]).focus();
				return false;
			}
		}
		return true;
	});

	$('#run').click(function () {
		return runCmd($('#cmd').val());
	});
});

function runCmd(cmd) {
	if (!cmd) {
		return apnscp.addMessage("No command specified", 'error');
	}
	var ajax, modal, indicator = apnscp.indicator();
	modal = $('#prgModal').modal();
	modal.one('hide.bs.modal', function () {
		ajax.abort();
	}).one('shown.bs.modal', function () {
		var $container = $('.output_container', modal).empty().addClass('prg-output');
		modal.find('.ui-ajax-indicator').remove().end().find('.modal-body').prepend(indicator);
		$('.cmd', modal).text(cmd);
		apnscp.call_app(null, 'reserve_tee').done(function (ret) {
			ajax = $.ajax_unbuffered({
				output: $container,
				url: '/ajax?engine=cmd&fn=pman_run',
				file: ret,
				type: "POST",
				data: {
					args: [cmd, [], [], {'tee': ret}],
					s: session.id
				},
				indicator: indicator,
				onUpdate: function (lines) {
					$('.output_container', modal).get(0).scrollTop += 100000;
				},
			});
		});
	});
	modal.modal('show');


	return;
}

function make_editable($row) {
	var $found = $row.find(':input[readonly]').each(function () {
		$(this).data('original', $(this).val()).prop('readonly', false).on('keydown', function (e) {
			switch (e.keyCode) {
				case 13:
					$row.find('.btn-primary').click();
					return false;
				case 27:
					$row.find('.ui-warn').click();
					return false;
				default:
					return true;
			}
			return false;
		});
	});
	$row.find('.spec :input').setCursorPosition(0, -1);

	var $undo = $('<button />').attr({
			'name': "undo",
			'type': 'button',
			'class': "ui-warn btn btn-secondary mr-1"
		}).click(function () {
			$row.find(':input').each(function (e) {
				if (!(data = $(this).data('original'))) {
					return true;
				}
				$(this).val(data).prop('readonly', true).removeData('original');
			});
			$('.actions', $row).children().remove('button:visible').end().children('.input-group').show();
			return false;
		}).html("<i class='fa fa-undo'></i> Undo"),
		$save = $('<button />').attr({
			'name': 'save',
			'type': 'button',
			'class': 'btn btn-primary'
		}).html("<i class='fa fa-check'></i> Save").click(function () {
			var $time = $row.find('.spec input'),
				$cmd = $row.find('.cmd input');
			$row.find('.msg').empty().hide();
			$row.removeClass('has-danger').removeClass('has-success');
			apnscp.call_app(null, 'deleteCronjob', [$time.data('original'), $cmd.data('original')]).done(function (ret) {
				apnscp.call_app(null, 'addCronjob', [$time.val(), $cmd.val()]).done(function () {
					$found.each(function () {
						$(this).prop('readonly', true).attr('data-original', $(this).val());
					});
					$row.find('.ui-action-run').data('command', $cmd.val());
					$save.remove();
					$undo.remove();
					$('.input-group', $row).show();
					apnscp.call_app(null, 'formatCron', [$time.val()]).done(function (ret) {
						if (ret && ret.next) {
							$('.next', $row).hide().text(ret.next).fadeIn('fast').show();
						}
					});
				}).fail(function () {
					$row.removeClass('has-success').addClass('has-danger').find('.time').removeClass('has-success').addClass('form-control-danger');
					$row.find('.msg').text("Failed to update task. Invalid time?").show().addClass("alert alert-danger");
					apnscp.call_app(null, 'addCronjob', [$time.data('original'), $cmd.data('original')]);
				});
			}).fail(function () {
				$row.removeClass('has-success').addClass('has-danger').find('.time').removeClass('has-success').addClass('form-control-danger');
				$row.find('.msg').text("Failed to update task - cannot find task in cron").show().addClass("alert alert-danger");
				return false;
			})
		});
	$('.actions .input-group', $row).hide().parent().append([$undo, $save]);
}
