<h3>No Certificate Detected</h3>
<div class="row">
	<p class="col-12">
		You must install a certificate to utilize SSL on your site. There are a few flavors of SSL to meet your needs:
	</p>
	<div class="col-12">
		@if ($Page->useLetsEncrypt())
			<h4>Let's Encrypt certificate
				<small class="badge badge-success">
					<i class="fa fa-star"></i>
					Best Option
				</small>
			</h4>
			<ul>
				<li>Free SSL certificates provided by <a href="http://www.letsencrypt.org">Let's Encrypt!</a></li>
				<li>Provides same level of encryption as commercial alternatives</li>
				<li>Recognized by all <a
							href="https://community.letsencrypt.org/t/which-browsers-and-operating-systems-support-lets-encrypt/4394">modern
						browsers</a></li>
				<li>Renewed automatically every 90 days</li>
				<li>Limited to 100 hostnames + 5 certificate adjustments/week</li>
			</ul>
		@else
			<h4>Self-signed certificate</h4>
			<ul>
				<li>No cost</li>
				<li>Certificate will not be accepted by any browser without warning</li>
			</ul>
			<h4>Normal SSL certificate</h4>
			<ul>
				<li>$30/year</li>
				<li>Certificate is accepted by 99.9% of browsers</li>
				<li>All HTTP data encrypted, but only supports 1 domain</li>
			</ul>

			<h4>UCC (multiple domain) certificate</h4>
			<ul>
				<li>$60/year</li>
				<li>Encrypts up to 5 domains</li>
				<li>More domains may be purchased with different UCC certificates</li>
			</ul>

			<h4>Extended validation (EV) certificate</h4>
			<ul>
				<li>$100+/year</li>
				<li>Encrypts single domain</li>
				<li>Fosters buyer trust by displaying additional corporate validation in browser</li>
				<li><em>Requestor must be business entity</em></li>
			</ul>
			<h4>Wildcard certificate</h4>
			<ul>
				<li>$200/year</li>
				<li>Encrypts an unlimited number of subdomains</li>
				<li>Valid only for one domain</li>
			</ul>
			<p class="col-12">
				All certificates may be purchased through any sponsporing CA, including
				<a class="ui-action ui-action-visit-site ui-action-label" href="http://domains.apisnetworks.com">domains.apisnetworks.com</a>.
			</p>
		@endif
	</div>

	<div class="col-12">
		<h3>Actions</h3>
		To get started, either <b>Import Certificate + Key</b> to open a ticket with your existing certificate and key,
		<b>Generate Request</b> to begin certificate
		purchase process, or
		@if ($Page->useLetsEncrypt())
			<b>Install Let's Encrypt Certificate</b> to request and install a signed certificate
		@else
			<b>Generate Self-Signed Certificate</b> to create and install a self-signed certificate
		@endif
	</div>
</div>

<fieldset class="form-group mt-3">
	<div class="button-actions">
		@if ($Page->useLetsEncrypt())
			<button id="lecert" name="lecert" class="btn btn-secondary mt-3" value="self"><i
						class="fa fa-user-secret"></i> Install Let's Encrypt<span
						class="hidden-xs-down"> Certificate</span></button>
		@else
			<button id="selfsign" name="selfsign" class="btn btn-secondary mt-3" data-toggle="collapse"
			        data-target="#create-certificate" aria-expanded="false" aria-controls="create-certificate"
			        value="self"><i class="fa fa-user-secret"></i> Install Self-Signed Certificate
			</button>
		@endif
		<button id="generate-newkey" name="generate-newkey" data-toggle="collapse" data-target="#create-certificate"
		        aria-expanded="false" aria-controls="create-certificate"
		        class="btn btn-secondary mt-3" value="new"><i class="fa fa-file-o"></i> Generate Request (CSR)
		</button>

		@if (is_debug())
			<button id="import" name="import" class="mt-3 btn btn-secondary" value="new"><i
						class="fa fa-upload"></i>
				Import Certificate + Key
			</button>
		@endif

	</div>
</fieldset>