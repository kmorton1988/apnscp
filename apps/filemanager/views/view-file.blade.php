@include('partials.manager-header')
@php
	$file = urldecode($_GET['f']);
	$props = $Page->getProperties($file);
	$mime = $props['mime'];
	$eol = 'unix';
	/**
	 * Initially preferred a JS approach, but Chrome interprets
	 * data in a textarea as \n even if the spec calls for \r\n
	 */
	if (false !== strpos($mime, ' CRLF ')) {
		$eol = 'windows';
	} else if (false !== strpos($mime, ' CR ')) {
		$eol = 'mac';
	}

	if (strstr($mime, 'text'))
		$contents = htmlentities($Page->getFileContents($file));
	else if (strstr($mime, 'empty'))
		$contents = '';
	if ($Page->errors_exist()) return;
	$stats = $Page->statFile($file);
	$mode = $Page->editor_mode();
@endphp
@if (strstr($mime, "text") || strstr($mime, "empty"))
<tr>
	<td class="cell1" colspan="6" align="">
		<form action="filemanager.php?cwd=<?php print($Page->getCurrentPath()); ?>&editor=<?=$Page->editor_mode()?>"
		      method="POST">


			<?php
			if (!strstr($mime, "text") && !strstr($mime, "empty")):
				print("<p align='center'>Binary file.  Preview not available</>");
			else: ?>
			@if ($stats['can_write'])
			<h4>Editor Options</h4>
			<div class="row">
				<div class="d-flex form-group form-inline col-12 justify-content-between">
					<button type="submit" id='save' name="Save_Changes" class="btn btn-primary">
						Save Changes
					</button>
					<button type="button" name="editor[<?=$mode == "raw" ? 'rich' : 'raw' ?>]" id="editor"
					        class="btn btn-secondary">
						<i class="fa fa-<?=$mode == "raw" ? 'paint-brush' : 'pencil-square-o'?>"></i> Switch
						to <?=$mode == "raw" ? 'WYSIWYG' : 'Basic'?> Editor
					</button>

					<div class="input-group float-right eol-container block-right">
						<div class="btn-group text-right hidden-sm-down">
							<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
							        aria-haspopup="true" aria-expanded="false">
								<span class="marker"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<div class="dropdown-menu dropdown-menu-right eol-types">
								<label class="windows dropdown-item">
									<input class="radio-inline" type="radio" name="eol" value="windows"/>
									Win (\r\n)
								</label>
								<label class="unix dropdown-item">
									<input class="radio-inline" type="radio" name="eol" value="unix"/>
									Unix (\n)
								</label>
								<label class="mac dropdown-item">
									<input class="radio-inline" type="radio" name="eol" value="mac"/>
									Mac (\r)
								</label>
								<div class="dropdown-divider"></div>
								<small class=" dropdown-item">Detected <span id="EOL"><?=ucwords($eol)?></span></small>
								<input type="hidden" name="file[eol]" id="eol-original" value="<? print $eol; ?>"/>
							</div>
						</div>

					</div>
				</div>
			</div>
			@endif

			<textarea rows="25" cols="100" name="code" id="code" class="form-control @if ($Page->editor_mode() === 'raw')text-monospace @endif" WRAP="OFF"><?php
				$charset = $props['charset'];
				if ($charset && strtoupper($charset) !== "UTF-8") {
					echo mb_convert_encoding($contents, 'UTF-8', $charset);
				} else {
					echo $contents;
				}
				?></textarea>

			<input type="hidden" name="file[charset]" value="<?php print $charset; ?>"/>
			<input type="hidden" name="file[name]" value="<?php print $file; ?>"/>
			<?php endif; ?>
		</form>

	</td>
</tr>
@else
	<tr>
		<td colspan="6">
			<p>Mime type: <?=$mime?> </p>
			<a href="<?=\HTML_Kit::new_page_url_params(null, array('download' => $file))?>" class="btn btn-secondary btn-lg mr-3">
				<i class="fa fa-download"></i> Download
			</a>
			@if ($Page->isCompressedFile($file))
				<a href="<?=\HTML_Kit::new_page_url_params(null, array('co' => $file))?>"
				   class="btn btn-secondary btn-lg">
					<i class="ui-action ui-action-label ui-action-open-archive"></i> Open Archive
				</a>
			@endif
			</form>

		</td>
	</tr>
@endif
