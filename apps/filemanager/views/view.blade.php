<?php
namespace apps\filemanager;
?>
<tr id="dir-hdr">
	<td class="head1 center" width="25" align="center">
		<?php if ($Page->getState() == 'list'): ?>
		<input type="checkbox" class="checkbox" id="select_all"/>
		<?php endif; ?>
	</td>
	<td class="head1 name">Name</td>
	<td class="head1 right hidden-xs-down">Size</td>
	<td class="head1 left hidden-sm-down">Owner</td>
	<td class="head1 center actions">Actions</td>
</tr>
<?php
$parent = null;
if ($Page->getCurrentPath() != '/'):
$parent = $Page->statParent();
?>
<tr id="file-root">
	<td class="" align="center">
		&nbsp;
	</td>
	<td class=" name" align="left">

		<a class="node node-parent-dir"
		   href="filemanager?cwd=<?=\HTML_Kit::url_encode($Page->getParentDirectory()); ?>">
			Parent Directory
		</a>
	</td>
	<td class="right hidden-xs-down"></td>
	<td class="left owner hidden-sm-down"><?php print ($parent['owner']); ?></td>
	<td class="action center"></td>
</tr>

<?php
endif;

$contents = $Page->getDirectoryContents();
if (!isset($contents[0]) && !is_array($contents)) return;
$parent_stat = $Page->statParent();
$size = $i = 0;
$reported = false;
foreach ($contents as $content):
$filename = str_replace('//', '/', basename(urldecode($content['file_name'])));
if (strlen($filename) >= Page::SHADOW_MIN_LEN) {
	$filename = '<span class="filename expanded">' . ($filename) . '</span>' .
		'<span class="filename_truncated">' . (substr_replace($filename,
			' ... ',
			Page::SHADOW_MIN_LEN / 2 - 2,
			strlen($filename) - (Page::SHADOW_MIN_LEN / 2 - 2) * 2)) . '</span>';
} else {
	$filename = '<span class="filename">' . ($filename) . '</span>';
}
$is_compressed = $Page->isCompressedFile($content['file_name']);
?>
<tr class="entry ftype-<?=$content['file_type']?>">
	<td class="" align="center">
		<?php if ($parent_stat['can_write'] && $parent_stat['can_execute']): ?>
		<input type="checkbox" name="file[]" class="checkbox-inline" value="<?php print($content['file_name']); ?>"/>
		<?php endif; ?>
	</td>
	<td class="name " align="left">
		<?php
		if (!isset($content['can_read']) && !$reported) {
			$reported = true;
			$data = "WANTED: " . var_export($content, true) . "\r\n" . var_export($contents, true) . "\r\n";
			Error_Reporter::report("MISSING!!!! " . $data);
		}
		if ($content['can_read'] || (($content['link'] === 1 || $content['file_type'] == 'dir') && $content['can_execute'])):
		$icon_type = $Page->getIconType(
			$content['file_type'],
			$content['link'],
			$content['can_read'],
			$content['can_execute'],
			$content['referent'],
			$content['portable']
		);

		print '<img src="' . $icon_type . '" alt=""  align="middle" />';

		if ($content['file_type'] != 'dir' && $content['link'] != 2):
		if ($content['can_read']): ?>
		<span class="name"><a
					href="{{ \HTML_Kit::page_url_params(array('f' => $content['file_name'])) }}">{{ $filename }}</a></span>
		<?php else:
			print $filename;
		endif;


		else: ?>
		<span class="name">
                    	<?php if ($content['can_read'] && $content['can_execute']): ?>
			<a href="filemanager?cwd={{ (!$content['link'] ? $content['file_name'] : $content['referent']) }}">
                    	<?php endif;
				print $filename;
				if ($content['can_read'] && $content['can_execute']): ?>
                            </a>
			<?php endif; ?>
                    </span>
		<?php
		endif;
		?>

		<?php
		else:
			printf('<img src="%s" alt=""  align="middle" />&nbsp;',
				$Page->getIconType($content['file_type'],
					$content['link'],
					$content['can_read'],
					$content['can_execute'],
					$content['referent'],
					$content['portable']));
			print $filename;
		endif;?>
	</td>
	<td class="right size hidden-xs-down"><?php if ($content['file_type'] == 'file' || $content['file_type'] == 'link' && $content['link'] == 1) print(\Formatter::reduceBytes($content['size'])); ?></td>
	<td class="left owner  hidden-sm-down"><?php print($content['owner']); ?></td>
	<td class=" actions center">
		<div class="input-group justify-content-center">
			<div class="btn-group">
				<button class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
				        aria-expanded="false"></button>

				<div class="dropdown-menu dropdown-menu-right">
					<?php if ($is_compressed && $content['can_read']): ?>
					<a title="Open" class="ui-action ui-action-label btn btn-block ui-action-open-archive dropdown-item"
					   href="filemanager?co={{ $content['file_name'] }}">Open</a>
					<?php elseif ($content['file_type'] != 'dir' && $content['can_write'] && $content['can_read'] && !$is_compressed && !$content['link'] ): ?>
					<a title="Edit" class="ui-action-label btn btn-block ui-action ui-action-edit dropdown-item"
					   href="{{ \HTML_Kit::page_url_params(array('f' => $content['file_name'])) }}">Edit</a>
					<?php endif; ?>

					<?php if ($content['file_type'] != 'dir' && $content['link'] != 2 && $content['can_read']): ?>
					<a title="Download" class="btn-block ui-action ui-action-label btn ui-action-download dropdown-item"
					   href="?download={{ $content['file_name'] }}">Download</a>
					<?php endif; ?>

					@if ($content['can_chown'])
						<a title="Properties" href="{{ \HTML_Kit::page_url() }}?ep={{ $content['file_name'] }}" class="ui-action ui-action-label btn btn-block ui-action-properties dropdown-item">
							Properties
						</a>
					@endif

					<?php if ($parent_stat['can_write']): ?>
					<a href="#" title="Rename"
					   class="ui-action btn ui-action-label btn-block ui-action-rename dropdown-item">
						Rename
					</a>
					<?php endif; ?>

					<?php if ($parent_stat['can_write'] && $parent_stat['can_execute']): ?>
					<a title="Delete"
					   class="ui-action ui-action-label btn btn-block warn ui-action-delete dropdown-item"
					   href="?d=<?php print(base64_encode($content['file_name']));?>"
					   onClick="return confirm('Are you sure you want to delete <?php print($content['file_name']); ?>?');">
						Delete
					</a>
					<?php endif; ?>

				</div>
			</div>
		</div>


	</td>
</tr>
<?php
$size += $content['size'];
$i++;
endforeach;
?>
<tfoot>


<tr>
	<td class=" right disk-folder" colspan="2">
        <span id="folder-selected">
        Selected Files: <span id="selected-size"></span> MB (<span id="selected-count"></span> items)
        </span>

		<span id="folder-unselected">
        Current Folder:
        <span id="folder-size"><?php print \Formatter::reduceBytes($size); ?></span> (<span
					id="folder-count"><?=$i?></span> items)
        </span>
	</td>
	<td class="right disk-total" colspan="3">
		<?php $account = $Page->diskUsage(); printf("<span id='disk-free'>%s</span> Free :: <span id='disk-total'>%s</span> Total",
			$account['free'], $account['total']);?>
	</td>
</tr>
</tfoot>
</table>

