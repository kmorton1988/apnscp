<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\domainmanager;

	use Error_Reporter;
	use Lararia\Jobs\Job;
	use Lararia\Jobs\SimpleCommandJob;
	use Module\Support\Webapps\App\Installer;

	class Page extends \Page_Container
	{
		const CACHE_KEY = 'domainman.domains';
		// = array('active' => array('add' => null, 'remove' => null), 'inactive' => array('add' => null, 'remove' => null));
		private $_domains;

		public function __construct()
		{
			parent::__construct();
			$this->add_javascript('domainmanager.js');
			$this->init_js('browser', 'sorter');
			$this->add_css('domainmanager.css');
			$this->_populateDomainCache();
			if (isset($_GET['mode']) && $_GET['mode'] == 'view') {
				$this->add_javascript("var users=['" . implode("','", array_keys($this->user_get_users())) . "'];",
					'internal', false, true);
			}
		}

		private function _populateDomainCache(bool $freshen = false)
		{
			if (!$freshen && \Session::has('mdm')) {
				$this->_domains = \Session::get('mdm');
				return;
			}
			$this->_domains = array(
				'active'   => $this->list_shared_domains(),
				'inactive' => $this->aliases_list_unsynchronized_domains(),
				'mdm'      => array()
			);
		}

		public function __destruct()
		{
			parent::__destruct();
			\Session::set('mdm', $this->_domains);
		}

		public function list_shared_domains()
		{
			if (!isset($this->_domains['active'])) {
				$domains = $this->aliases_list_shared_domains();
				\Util_Conf::sort_domains($domains, 'key');
				for ($i = 0, $k = array_keys($domains), $n = sizeof($k); $i < $n; $i++) {
					$domain = $k[$i];
					$path = $domains[$domain];
					$owner = null;
					$prefix = $this->getAuthContext()->domain_fs_path();
					if (file_exists($prefix . rtrim($path, '/'))) {
						$owner = $this->file_stat($path);
						$owner = $owner['owner'];
					}
					$domains[$domain] = array(
						'path'  => $path,
						'owner' => $owner
					);
				}
				$this->_domains['active'] = $domains;
			}
			return $this->_domains['active'];
		}

		/** postback function handler */
		public function on_postback($params)
		{
			if (isset($params['mode']) && count($params) == 1) {
				$this->hide_pb();
			}

			if (isset($params['edit'])) {
				$domain = $params['domain-old'];
				$editparams = array();
				if ($params['domain'] != $params['domain-old']) {
					$editparams['domain'] = $params['domain'];
				}

				if ($params['path'] != $params['path-old']) {
					$editparams['path'] = $params['path'];
				}

				$ret = $this->aliases_modify_domain($domain, $editparams);
				$this->bind($ret);
				unset($this->_domains['active']);
				if ($ret && $params['user'] != $params['user-old']) {
					$path = $this->web_get_docroot($params['domain']);
					$oldUser = array_get($this->file_stat($path), 'owner', $this->getAuthContext()->username);
					$this->file_chown($path, $params['user']);
					(Job::create(new SimpleCommandJob(
						$this->getAuthContext(),
						'file_takeover_user',
						$oldUser,
						$params['user'],
						$path
					)))->
						setTags([$this->getAuthContext()->site, 'file_takeover_user'])->
						dispatch();

				}
				$this->_populateDomainCache(true);
				return $this->_populateDomainCache(true);

			}
			if (isset($params['file-manager'])) {
				header('Location: ' . \HTML_Kit::new_page_url_params('/apps/filemanager',
						array('f' => $params['file-manager'])));
				exit();
			}
			if (isset($params['synchronize'])) {
				// Activate Changes
				// @TODO rewrite, dog slow with third-party DNS providers
				if (!$this->aliases_synchronize_changes()) {
					return;
				}

				$this->_activateDomains();
				foreach ($this->_domains['mdm'] as $domain => $options) {
					if (!empty($options['email'])) {
						if ($this->email_transport_exists($domain)) {
							$this->email_remove_virtual_transport($domain);
						}
						$this->email_add_virtual_transport($domain);
						if ($options['clone']) {
							$this->email_import_from_domain($domain, $options['clone']);
						}
					} else if ($this->email_transport_exists($domain)) {
						$this->email_remove_virtual_transport($domain);
					}
					unset($this->_domains['mdm'][$domain]);
				}
				// account meta is imported during page load
				// force a reset to prevent recently modified
				// domains from appearing in the add/remove cache
				$this->_populateDomainCache(true);
			} else if (isset($params['add'])) {
				// Add a new domain
				$domain = $params['domain'];
				// *sigh* people
				if (!strncmp($domain, 'http://', 7)) {
					$domain = substr($domain, 6);
					warn("Stripped non-domain protocol, http://, from domain");
				} else if (!strncmp($domain, 'https://', 8)) {
					$domain = substr($domain, 7);
					warn("Stripped non-domain protocol, https://, from domain");
				}

				$type = $params['shared_type'];
				if ($type == 'doc_root') {
					$path = '/var/www/' . ($params['doc_root_path'] ? $params['doc_root_path'] : $domain);
				} else if ($type == 'user_dir') {
					if (!isset($params['user_home'])) {
						return error("no user selected for addon domain");
					}
					$path = $params['user_home'] . '/' . ($params['user_dir'] ? $params['user_dir'] : $domain);
				} else {
					if (!isset($params['subdomain_path'])) {
						return error("no subdomain selected for addon domain");
					}
					$path = $params['subdomain_path'];
				}
				$ret = $this->aliases_add_domain($domain, $path);
				if (!$ret) {
					return false;
				}

				$this->_domains['mdm'][$domain] = [
					'clone' => false,
					'email' => false
				];
				// email cloning and transport setup
				if (isset($params['enable_email'])) {
					$this->_domains['mdm'][$domain]['email'] = true;
					if (isset($params['clone_domain']) && isset($params['domain_clone'])) {
						$this->_domains['mdm'][$domain]['clone'] = $params['domain_clone'];
					} else {
						$this->_domains['mdm'][$domain]['clone'] = false;
					}
				}

				$this->_addInactiveDomain($params['domain'], 'add');

				$nameservers = (array)$this->_getNameservers($domain);
				$hostingns = $this->dns_get_hosting_nameservers($domain);
				if (empty($nameservers) || $nameservers === false) {
					warn("Domain `%s' appears to be unregistered.", $domain);
				} else if ($hostingns && !in_array($hostingns[0], $nameservers)) {
					if ($nameservers) {
						warn("Nameservers for domain `%s' assigned to %s. Change nameservers " .
							"to %s.", $domain, join(", ", $nameservers), join(", ", $hostingns));
					} else {
						warn('No nameservers configured. Change nameservers to %s.', implode(", ", $hostingns));
					}

				}

				if (isset($params['app-preload']) && $params['app-preload']) {
					$preload = $params['app-preload'];
				} else {
					$preload = '';
				}

				// webapp preload
				// @TODO make this cleaner...
				if ($preload) {
					$c = new Installer($domain);
					if (0 === strpos($preload, 'drupal')) {
						$type = 'drupal';
						if ($preload === 'drupal8') {

							$c->setOption('version', 8);
						} else {
							$c->setOption('version', 7);
						}
					} else {
						$type = 'wordpress';
						if ($preload === 'wpperfstack' || $preload === 'wprectack') {
							$c->setOption('performance-stack', true);
						}
						if ($preload == 'wpsecstack' || $preload == 'wprectack') {
							$c->setOption('security-stack', true);
						}
					}
					$c->dispatch($type);

				}

			} else if (isset($params['delete'])) {
				$logs = $this->logs_list_logfiles();
				foreach (array_keys($params['delete']) as $domain) {
					if (!$this->aliases_remove_domain($domain)) {
						continue;
					}
					if (isset($logs[$domain])) {
						foreach ($logs[$domain] as $subdomain => $log) {
							$this->logs_remove_logfile($domain, $subdomain);
						}
					}
					$this->_addInactiveDomain($domain, 'remove');
				}
			} else if (isset($params['rename'])) {

			}
		}

		private function _activateDomains()
		{
			// @XXX this should share the same context with afi module invocation
			$this->getAuthContext()->reset();
			$this->_domains['active'] = null;
			$this->_domains['inactive'] = $this->aliases_list_unsynchronized_domains();
		}

		private function _addInactiveDomain($domain, $mode = 'add')
		{
			$haystack = 'remove';
			if ($mode != 'add') {
				$mode = 'remove';
				$haystack = 'add';
			}
			$domains = $this->_domains['inactive'];
			$domains[$mode][] = $domain;
			$needle = array_search($domain, $domains[$haystack]);
			if ($needle !== false) {
				unset($domains[$haystack][$needle]);
			}
			if ($mode == 'remove') {
				if (isset($this->_domains['active'][$domain])) {
					unset($this->_domains['active'][$domain]);
				}
				if (isset($this->_domains['mdm'][$domain])) {
					unset($this->_domains['mdm'][$domain]);
				}
			}
			$this->_domains['inactive'] = $domains;
		}

		private function _getNameservers($domain)
		{
			$nameservers = array();
			Error_Reporter::suppress_php_error('dns_get_record');
			$dns = dns_get_record($domain, DNS_NS);
			if ($dns) {
				for ($i = 0, $n = sizeof($dns); $i < $n; $i++) {
					if (isset($dns[$i]['target'])) {
						$nameservers[] = $dns[$i]['target'];
					}
				}
			} else if ($dns === false) {
				return false;
			}
			natsort($nameservers);

			return $nameservers;
		}

		public function list_inactive_domains()
		{
			if (!isset($this->_domains['inactive'])) {
				$this->_domains['inactive'] = $this->aliases_list_unsynchronized_domains();
			}

			return $this->_domains['inactive'];
		}

		public function get_subdomains()
		{
			return $this->web_list_subdomains();
		}

		public function list_users()
		{
			return $this->user_get_users();
		}

		public function list_all_domains()
		{
			$domains = array_keys($this->list_shared_domains());
			$domains[] = \Util_Conf::get_svc_config('siteinfo', 'domain');

			return $domains;
		}

		public function getMode()
		{
			if (!isset($_GET['mode']) ||
				$_GET['mode'] == 'add') {
				return 'add';
			}

			return 'view';
		}

		public function list_transports()
		{
			return $this->email_list_virtual_transports();
		}
	}
