<fieldset class="form-group">
	<label class="form-label">
		<a href="/apps/webapps" class="ui-action ui-action-label ui-action-switch-app">
			Webapp
		</a> Preload</label>
	<select class="form-control custom-select" name="app-preload">
		<option value="">None</option>
		<option value="wordpress">Wordpress</option>
		<option value="wpperfstack">Wordpress + Performance Stack</option>
		<option value="wprectack">Wordpress + Security + Performance Stack</option>z
		<option value="drupal">Drupal 7</option>
		@if (\Util_Conf::call('sql_mysql_version') >= 50503)
			<option value="drupal8">Drupal 8</option>
		@endif
	</select>
</fieldset>