<?php
    $processor = $Page->getProcessors();
?>

<div id="accordion" role="tablist" aria-multiselectable="true">
	<div class="card">
		<div class="card-header" role="tab" id="headingOS">
			<h5 class="mb-0">
				<a class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#os" aria-expanded="true"
				   aria-controls="os">
					OS Information
				</a>
			</h5>
		</div>

		<div id="os" class="collapse show" role="tabpanel" aria-labelledby="headingOs">
			<div class="card-block">
				<table width="100%">
					<tr>
						<td>
							<h4>Canonical Hostname</h4>
						</td>
						<td>
							<?=$Page->get_canonical_hostname()?>
						</td>
					</tr>
					<tr>
						<td class="">
							<h4>Primary IP Address</h4>
						</td>
						<td>
							<?=$Page->get_primary_ip_addr()?>
						</td>
					</tr>
					<tr>
						<td class="">
							<h4>Kernel</h4>
						</td>
						<td>
							<?=$Page->get_kernel()?>
						</td>
					</tr>
					<tr>
						<td class="">
							<h4>Distro</h4>
						</td>
						<td>
							<?=$Page->get_os_release()?>
						</td>
					</tr>
					<tr>
						<td class="">
							<h4>Uptime</h4>
						</td>
						<td>
							<?=$Page->get_uptime()?>
						</td>
					</tr>
					<tr>
						<td class="">
							<h4>Load Average</h4>
						</td>
						<td>
							<?=join(',', $Page->get_load_average())?>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="card">
		<div class="card-header" role="tab" id="headingHardware">
			<h5 class="mb-0">
				<a class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#hardware"
				   aria-expanded="true" aria-controls="hardware">
					Hardware
				</a>
			</h5>
		</div>

		<div id="hardware" class="collapse" role="tabpanel" aria-labelledby="headingHardware">
			<div class="card-block">
				<table width="100%">
					<thead>
					<tr>
						<th class="">
							# Processors
						</th>
						<th class="">
							Model Name
						</th>
						<th class="">
							CPU Speed
						</th>
						<th class="">
							Cache Size
						</th>
						<th class="">
							Bogomips
						</th>
					</tr>
					</thead>
					<tr>
						<td>
							<?=$processor['count']?>
						</td>
						<td>
							<?=$processor['model']?>
						</td>
						<td>
							<?=\Formatter::commafy($processor['speed'])?>  MHz
						</td>
						<td>
							<?=\Formatter::commafy($processor['cache'])?> KB
						</td>
						<td>
							<?=\Formatter::commafy($processor['bogomips'])?>
						</td>
					</tr>
					<tr>
						<td class="" colspan="5">
							<h4>PCI Devices</h4>
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<div style="overflow-x:scroll;">
								<pre><?=ltrim($Page->getPCIDevices());?></pre>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="card">
		<div class="card-header" role="tab" id="headingNetwork">
			<h5 class="mb-0">
				<a class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#network"
				   aria-expanded="true" aria-controls="network">
					Network
				</a>
			</h5>
		</div>

		<div id="network" class="collapse" role="tabpanel" aria-labelledby="headingNetwork">
			<div class="card-block">
				<table width="100%">
					<thead>
					<tr>
						<th class="">
							Device
						</th>
						<th class="">
							Received (packets)
						</th>
						<th class="">
							Sent (packets)
						</th>
						<th class="">
							Err/Drop
						</th>
					</tr>
					</thead>
					<?php
			            $netDevs = $Page->network_information();
					foreach ($netDevs as $dev => $info):
					?>
					<tr>
						<td align="center">
							<?=$dev?>
						</td>
						<td>
							<?=\Formatter::reduceBytes($info['rx_bytes'],3)?>
							(<?=\Formatter::commafy($info['rx_packets'])?>)

						</td>
						<td>
							<?=\Formatter::reduceBytes($info['tx_bytes'],3)?>
							(<?=\Formatter::commafy($info['tx_packets'])?>)
						</td>
						<td>
							<?=\Formatter::commafy($info['rx_errs']+$info['tx_errs'])?>
							/<?=\Formatter::commafy($info['rx_drop']+$info['tx_drop'])?>
						</td>

					</tr>
					<?php
			            endforeach;
		            ?>
				</table>
			</div>
		</div>
	</div>

	<div class="card">
		<div class="card-header" role="tab" id="headingMemory">
			<h5 class="mb-0">
				<a class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#memory" aria-expanded="true"
				   aria-controls="memory">
					Memory
				</a>
			</h5>
		</div>

		<div id="memory" class="collapse" role="tabpanel" aria-labelledby="headingMemory">
			<div class="card-block p-0">
				<table width="100%" class="table">
					<thead>
					<tr>
						<th>
							Type
						</th>
						<th>
							Percent Capacity
						</th>
						<th class="right">
							Free
						</th>
						<th class="right">
							Used
						</th>
						<th class="right">
							Size
						</th>
					</tr>
					</thead>
					<?php
			            $meminfo = $Page->memory_information();
					$info = $meminfo['ram'];
					?>
					<tr>
						<td>
							Physical Memory
						</td>
						<td>
							<?php printf("<div class='ui-gauge' style='display:block;'>
							<div class='ui-gauge-slice ui-gauge-used gauge-used' style='width:%d%%'></div>
							<div class='ui-gauge-slice ui-gauge-free gauge-free' style='width:%d%%'></div>
			</div>
			<span class='ui-gauge-label'>%.2f%%</span>",
			floor($info['t_used']/$info['total']*100),
			ceil((1-$info['t_used']/$info['total'])*100),
			$info['t_used']/$info['total']*100) ?>
			</td>
			<td class="right">
				<?php printf("%.2f MB",$info['t_free']/1024.); ?>
			</td>
			<td class="right">
				<?php printf("%.2f MB",$info['t_used']/1024.) ?>
			</td>
			<td class="right">
				<?=\Formatter::commafy(sprintf("%.2f MB",($info['total']/1024))); ?>
			</td>
			</tr>
			<tr>
				<td>
					&nbsp;&nbsp;- Kernel + applications
				</td>
				<td>
					<?php printf("<div class='ui-gauge' style='display:block;'>
					<div class='ui-gauge-slice ui-gauge-used gauge-used' style='width:%d%%'></div>
					<div class='ui-gauge-slice ui-gauge-free gauge-free' style='width:%d%%'></div>
		</div>
		<span class='ui-gauge-label'>%.2f%%</span>",
		floor($info['app']/$info['total']*100),
		ceil((1-$info['app']/$info['total'])*100),
		$info['app']/$info['total']*100) ?>
		</td>
		<td class="right">
			&nbsp;
		</td>
		<td class="right">
			<?php printf("%.2f MB",$info['app']/1024.);?>
		</td>
		<td class="right">
			&nbsp;
		</td>
		</tr>
		<tr>
			<td>
				&nbsp;&nbsp;- Buffers
			</td>
			<td>
				<?php printf("<div class='ui-gauge' style='display:block;'>
				<div class='ui-gauge-slice ui-gauge-used gauge-used' style='width:%d%%'></div>
				<div class='ui-gauge-slice ui-gauge-free gauge-free' style='width:%d%%'></div>
	</div>
	<span class='ui-gauge-label'>%.2f%%</span>",
	floor($info['buffers']/$info['total']*100),
	ceil((1-$info['buffers']/$info['total'])*100),
	$info['buffers']/$info['total']*100) ?>
	</td>
	<td class="right">
		&nbsp;
	</td>
	<td class="right">
		<?php printf("%.2f MB",$info['buffers']/1024); ?>
	</td>
	<td class="right">
		&nbsp;
	</td>
	</tr>
	<tr>
		<td>
			&nbsp;&nbsp;- Cached
		</td>
		<td>
			<?php printf("<div class='ui-gauge' style='display:block;'>
			<div class='ui-gauge-slice ui-gauge-used gauge-used' style='width:%d%%'></div>
			<div class='ui-gauge-slice ui-gauge-free gauge-free' style='width:%d%%'></div>
</div> <span class='ui-gauge-label'>%.2f%%</span>",
floor($info['cached']/$info['total']*100),
ceil((1-$info['cached']/$info['total'])*100),
$info['cached']/$info['total']*100) ?>

</td>
<td class="right">
	&nbsp;
</td>
<td class="right">
	<?php printf("%.2f MB",$info['cached']/1024); ?>
</td>
<td class="right">
	&nbsp;
</td>
</tr>
<?php
			            // check if devices configured for swap, then output swap
			            if ($meminfo['devswap']):
				            $info = $meminfo['swap']; // handle swap
				            ?>
<tr>
	<td>
		Disk Swap
	</td>
	<td>
		<?php printf("<div class='ui-gauge' style='display:block;'>
		<div class='ui-gauge-slice ui-gauge-used gauge-used' style='width:%d%%'></div>
		<div class='ui-gauge-slice ui-gauge-free gauge-free' style='width:%d%%'></div>
		</div> <span class='ui-gauge-label'>%.2f%%</span>",
		$info['percent'],
		(100-$info['percent']),
		($info['total']-$info['free'])/$info['total']*100) ?>
	</td>
	<td class="right">
		&nbsp;
	</td>
	<td class="right">
		<?php printf("%.2f MB", $info['used']/1024); ?>
	</td>
	<td class="right">
		<?=\Formatter::commafy(sprintf("%.2f MB",$info['total']/1024));?>
	</td>
</tr>
<?php
				            foreach ($meminfo['devswap'] as $device => $info):
?>
<tr>
	<td>
		&nbsp;&nbsp;- <?=$device?>
	</td>
	<td>
		<?php printf("<div class='ui-gauge' style='display:block;'>
		<div class='ui-gauge-slice ui-gauge-used gauge-used' style='width:%d%%'></div>
		<div class='ui-gauge-slice ui-gauge-free gauge-free' style='width:%d%%'></div>
		</div> <span class='ui-gauge-label'>%.2f%%</span>",
		$info['percent'],
		(100-$info['percent']),
		($info['total']-$info['free'])/$info['total']*100) ?>
	</td>
	<td class="right">
		<?php printf("%.2f MB",$info['free']/1024); ?>
	</td>
	<td class="right">
		<?php printf("%.2f MB",$info['used']/1024); ?>
	</td>
	<td class="right">
		<?=\Formatter::commafy(sprintf("%.2f MB",$info['total']/1024)); ?>
	</td>
</tr>
<?php
				            endforeach;
			            endif;
		            ?>
</table>
</div>
</div>
</div>

<div class="card">
	<div class="card-header" role="tab" id="headingFilesystem">
		<h5 class="mb-0">
			<a class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#filesystem" aria-expanded="true"
			   aria-controls="filesystem">
				Filesystem
			</a>
		</h5>
	</div>
	<div id="filesystem" class="collapse" role="tabpanel" aria-labelledby="headingFilesystem">
		<?php $fsinfo = $Page->filesystem_information(); ?>
		<div class="card-block">
			<table width="100%" class="table">
				<thead>
				<tr>
					<th class="">
						Mount
					</th>
					<th class="">
						Type
					</th>
					<th class="">
						Type
					</th>
					<th class="">
						Percent Capacity
					</th>
					<th class="right">
						Free
					</th>
					<th class="right">
						Used
					</th>
					<th class="right">
						Size
					</th>
				</tr>
				</thead>

				<?php
			            foreach($fsinfo as $device):
				            ?>
				<tr>
					<td>
						<?=$device['disk']?>
					</td>
					<td>
						<?=$device['fstype']?>
					</td>
					<td>
						<?=$device['fstype']?>
					</td>
					<td>
						<?php printf("<div class='ui-gauge' style='display:block;'>
						<div class='ui-gauge-slice ui-gauge-used gauge-used' style='width:%d%%'></div>
						<div class='ui-gauge-slice ui-gauge-free gauge-free' style='width:%d%%'></div>
		</div>
		<span class='ui-gauge-label'>%.2f%%</span>",
		$device['percent'],
		(100-(int)$device['percent']),
		($device['used'])/$device['size']*100) ?>
		</td>
		<td class="right">
			<?=\Formatter::commafy(\Formatter::reduceBytes($device['free']*1024))?>
		</td>
		<td class="right">
			<?=\Formatter::commafy(\Formatter::reduceBytes($device['used']*1024))?>
		</td>
		<td class="right">
			<?=\Formatter::commafy(\Formatter::reduceBytes($device['size']*1024)) ?>
		</td>
		</tr>
		<?php
			            endforeach;
		            ?>
		</table>
	</div>
</div>
</div>
<?php
	$sastats = $Page->spamassassin();
	if ($sastats):
?>
<div class="card">
	<div class="card-header" role="tab" id="headingSpamfiltering">
		<h5 class="mb-0">
			<a class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#spamfiltering"
			   aria-expanded="true" aria-controls="spamfiltering">
				Spam Filtering
			</a>
		</h5>
	</div>

	<div id="spamfiltering" class="collapse" role="tabpanel" aria-labelledby="headingSpamfiltering">

		<div class="card-block">
			<table width="100%">
				<tr>
					<td class="">
						<h4>Period</h4>
					</td>
				</tr>
				<tr>
					<td align="center">
						<?=$sastats['begin_date']. ' - '.$sastats['end_date']?>
					</td>
				</tr>
				<tr>
					<td class=""><h4>Overall Parsing Information</h4></td>
				</tr>
				<tr>
					<td>
						<code class="prg-output"><?=$sastats['reporting_information']?></code>
					</td>
				</tr>
				<tr>
					<td class=""><h4>Parsing per Hour</h4></td>
				</tr>
				<tr>
					<td>
						<code class="prg-output"><?=$sastats['stats_by_hour']?></code>
					</td>
				</tr>
				<tr>
					<td>
						<h4>Score Information</h4>
					</td>
				</tr>
				<tr>
					<td>
						<code class="prg-output"><?=$sastats['rule_information']?></code>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<?php endif; ?>
</div>
