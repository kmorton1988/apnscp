<div id="theme" class="tab-pane" role="tabpanel" aria-labelledby="">
	<div class="form-check">
		<label class="custom-control custom-checkbox mb-0 align-items-center">
			<input class="form-check-input custom-control-input" type="checkbox" value="1" name="swap-buttons"
					{{ \Preferences::get(\Page_Renderer::THEME_SWAP_BUTTONS) ? 'checked="CHECKED"' : '' }} />
			<span class="custom-control-indicator"></span>
			<i class="fa fa-sticky-note-o mr-1" data-toggle="tooltip"
			   title="Switch orientation of menu from left to right."></i>
			Swap Menu Orientation
		</label>
	</div>
	<div class="form-check">
		<label class="custom-control custom-checkbox mb-0 align-items-center">
			<input class="form-check-input custom-control-input" type="checkbox" value="1" name="collapse-menu"
					{{ \Preferences::get(\Page_Renderer::THEME_MENU_ALWAYS_COLLAPSE) ? 'checked="CHECKED"' : '' }} />
			<span class="custom-control-indicator"></span>
			<i class="fa fa-sticky-note-o mr-1" data-toggle="tooltip" title="Always start with menu collapsed."></i>
			Always Collapse Menu
		</label>
	</div>
	@includeWhen(\Frontend\Css\StyleManager::allowSelection(), 'partials.theme-chooser')
</div>