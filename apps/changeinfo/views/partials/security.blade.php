<div id="security" class="tab-pane" role="tabpanel" aria-labelledby="">
	<h4>Email Notifications</h4>
	<p>All notifications will be sent to the address on record (<i>{{ $Page->getEmail() }}</i>).
		Your email address may be changed under <b>User Information</b>.
	</p>
	<fieldset class="form-group">
		<label class="block custom-control custom-checkbox align-middle">
			<input type="checkbox" class="custom-control-input" name="notify[login]" value="1"
			       @if ($Page->prefGet('login')) checked="CHECKED" @endif />
			<span class="custom-control-indicator"></span>
			CP login from unrecognized source
		</label>

		<label class="block custom-control custom-checkbox align-middle">
			<input type="checkbox" class="custom-control-input" name="notify[passwordchange]" value="1"
			       @if ($Page->prefGet('passwordchange')) checked="CHECKED" @endif />
			<span class="custom-control-indicator"></span>
			Password change
		</label>

		@if (UCard::is('site'))
			<label class="block custom-control custom-checkbox align-middle">
				<input type="checkbox" class="custom-control-input" name="notify[usernamechange]" value="1"
				       @if ($Page->prefGet('usernamechange')) checked="CHECKED" @endif />
				<span class="custom-control-indicator"></span>
				Username change
			</label>

			<label class="block custom-control custom-checkbox align-middle">
				<input type="checkbox" class="custom-control-input" name="notify[domainchange]" value="1"
				       @if ($Page->prefGet('domainchange')) checked="CHECKED" @endif />
				<span class="custom-control-indicator"></span>
				Primary domain change
			</label>
		@endif
	</fieldset>

	@if (\UCard::is('admin') || \Auth\IpRestrictor::instantiateContexted(\Auth::profile())->getLimit())
		<h4>Login Access</h4>
		<p>
			All IPv4, IPv6, and CIDRs listed below are permitted access. Any address <b>not listed</b>
			will <b>not be permitted access</b> to the panel on next login. Before adding any
			address, your active login, {{ \Auth::client_ip() }} will also be added.
		</p>
		<p>
			Do not restrict login access if you do not have access to a static IP address. This affects
			all services panel services including DAV and API.
		</p>
		<div class="row">
			<div class="col-12">
				<form action="{{ HTML_Kit::page_url() }}" method="POST">
					<label class="form-control-label d-block">
						IPv4, IPv6, or CIDR Range
					</label>
					<div class="form-inline">
						<div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-globe"></i>
						</span>
							<label for="ipInput" class="hinted">{{ \Auth::client_ip() }}</label>
							<input class="form-control mr-3" maxlength="39" size="39" id="ipInput" name="ip"
							       placeholder="" value=""/>
						</div>
						<button class=" btn btn-primary ui-action ui-action-add ui-action-label bindable" name="add-ip"
						        type="submit">
							Add Entry
						</button>
					</div>
					<ul class="text-danger list-unstyled mt-1" id="errors">
					</ul>

				</form>

				<form method="POST" id="active">
					<div id="ip-entries">
						@foreach (cmd('auth_get_ip_restrictions') as $entry => $gates)
							<div class="row ip-entry">
								<div class="col-12 mb-2">
									<button type="submit" data-ip="{{$entry}}"
									        onClick="return confirm('Are you sure you want to delete ' + $(this).val() + '?');"
									        name="remove-ip" value="{{ $entry }}"
									        class="ui-action warn ui-action-label ui-action-delete btn btn-secondary bindable">
										Delete
									</button>
									{{ $entry }}
								</div>
							</div>
						@endforeach
						<p class="alert-info note">
							Restrictions are not configured on this account. All IP addresses may login to this account.
						</p>
					</div>
				</form>

				<div class="row ip-entry hide" id="item-template">
					<div class="col-12 mb-2">
						<button type="submit" data-ip=""
						        onClick="return confirm('Are you sure you want to delete ' + $(this).val()  + '?');"
						        name="remove-ip"
						        value=""
						        class="ui-action warn ui-action-label ui-action-delete btn btn-secondary bindable">
							Delete
						</button>
						<span class="ip"></span>
					</div>
				</div>
			</div>
		</div>
	@endif
</div>