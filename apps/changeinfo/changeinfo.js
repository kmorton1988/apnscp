var dialogWindow;
var ruleMap = {
	'username': {
		fn: 'auth_change_username',
		rule: 'username',
		label: 'Change Username',
		input: 'New Username',
		icon: 'fa fa-user',
		'default': session.user
	},
	'domain': {
		fn: 'auth_change_domain',
		rule: 'domain',
		label: 'Change Domain',
		input: 'New Domain',
		icon: 'fa fa-cloud',
		'default': session.domain
	},
	'prefix': {
		fn: 'sql_change_prefix',
		rule: 'prefix',
		label: 'Change Prefix',
		input: 'New DB Prefix',
		icon: 'fa fa-database',
		'default': session.prefix
	},
	'wipe': {
		fn: 'site_wipe',
		rule: 'wipe',
		label: 'Reformat',
		input: 'Challenge Token',
		'default': ''
	},
	'getFunc': function (type) {
		if (!this[type]) {
			return function () {
			};
		}
		var fn = window[this[type].fn];
		if (typeof fn == 'function') {
			return function () {
			};
		}
		return fn();
	}

};
var keymap = [38, 38, 40, 40, 37, 39, 37, 39, 66, 65], keyptr = 0;

function sessionDestroyed() {
	try {
		apnscp.cmd('common_whoami', [], {useCustomHandlers: true, async: false}).fail(function () {
			throw("Success!");
		});
	} catch (error) {
		return true;
	}
	return false;
}


function wipe() {

	var html = $('<div class="alert alert-danger rounded-0 row px-0"><div class="col-12"><h3 class="text-danger">WARNING</h3>' +
		'Wiping your site is a <strong>permanent</strong> ' +
		'decision that will <strong>delete all data</strong>. Only do this if you want to restore your site to ' +
		'its original state when you signed up.<br /><br />Enter the following token to irreversibly delete all ' +
		'data from your account: <p class="challenge-token" id="challenge-token">' +
		'<i class="fa fa-spinner fa-spin"></i> Fetching...</p>' +
		'<label for="token-input" class="b">Challenge Token <input type="text" name="challenge" ' +
		'maxlength="8" size="8" value="" class="form-control" id="token-input" /></label></div></div>');
	var $indicator = apnscp.indicator();
	html.append($indicator);
	dialogWindow = $('#modal').bind('shown.bs.modal', function () {
		apnscp.cmd('site_wipe').done(function (e) {
			$('#modal').find('.challenge-token').hide().text(e['return']).fadeIn('fast');
		});
		html.keydown(function (e) {
			if (e.which == 13) {
				$('#format-account').click();
			}
		});
	});
	dialogWindow.find('.modal-body').empty().append(html);
	$('#modal .modal-body').append(
		$('<button type="submit" class="btn btn-primary warn">').text(ruleMap['wipe'].label).on('click', function () {
			var token = $('#token-input').val();
			if (!token) {
				alert("missing challenge token");
			} else {
				processChange.call(this, 'wipe', token);
			}
			return false;
		})
	);
	dialogWindow.modal();
	return;
}

$(window).on('load', function () {
	/** account param change */
	$('button.change').click(function (e) {
		var tmp = e.currentTarget.id,
			rule = tmp.substring(tmp.indexOf('-') + 1);
		if (ruleMap[rule] == null) {
			return false;
		}
		showWarning(rule);
		return false;
	});

	$('#load-theme-dialog').click(function () {
		var modal = apnscp.modal($('#upload-theme'));
		modal.on('show.bs.modal', function () {
			$(this).find('.hide').removeClass('hide');
		}).modal();
	});

	$('#password').focus(function () {
		$('.verify-password').show();
	}).blur(function () {
		if ($('#password').val().length < 1) {
			$('tr.verify-password').hide();
		}
	});

	$('#security .bindable').on('click', function (e) {
		e.preventDefault();
		switch ($(e.target).attr('name')) {
			case 'add-ip':
				return add($('#ipInput').val(), $('#ipInput'));
			case 'remove-ip':
				return remove($(this).val(), $(this));
			default:
				console.log("Unknown bound action " + $(e.target).attr('name'));
				return true;
		}
	});

	$("#localization select").combobox();
});

$(document).ready(function () {
	apnscp.navFocus();
	$(this).keydown(function (e) {
		if (keymap[keyptr] != e.keyCode) {
			keyptr = 0;
			return true;
		}
		keyptr++;
		if (keyptr == keymap.length) {
			keyptr = 0;
			wipe();
			return false;
		}
	});
	$('[data-toggle=tooltip]').tooltip();
});

function showWarning(rule) {
	$('#' + rule + '-rules').slideToggle(function () {
		$(this).find(':input').click(function () {
			if (!$(this).prop('checked')) {
				return true;
			}
			dialogWindow = $('#modal').on('show.bs.modal', function () {
				$('#change-form label').text(ruleMap[rule]['input']);
				$('#change-form input').val(ruleMap[rule]['default']);
				$('#change-form i').removeClass().addClass(ruleMap[rule]['icon']);
			}).on('shown.bs.modal', function () {
				$('#change-form').find(':input').setCursorPosition(0, -1);
			});
			dialogWindow.find('.modal-body').empty().append(
				$('#change-steps').removeClass('hide'),
				$('<button class="btn-primary btn" type="button">').text(ruleMap[rule].label).click(function () {
					processChange.call(this, rule, dialogWindow.find(':text').val());
				})
			).end().modal();
			return true;
		});
	});

}


function processChange(rule, data) {
	var $indicator = $('<i></i>'),
		fn = ruleMap[rule]['fn'],
		dialogBar = $('#modal');
	dialogBar.find('.btn-primary').find('.ui-ajax-indicator').remove().end().append($indicator);
	dialogWindow.find('.ui-ajax-error-msg').remove();
	$('#modal .btn-primary').prop('disabled', function (i, val) {
		$(this).data('old-title', $(this).text()).text('Processing').append($indicator);
		return true;
	});

	apnscp.cmd(fn, [data], {
		useCustomHandlers: true,
		indicator: $indicator
	}).fail(function (xhr, textStatus, errorThrown) {
		var status = $.parseJSON(xhr.responseText);
		var destroyed = sessionDestroyed();
		if (status['errors']) {
			var $err = [];
			if (destroyed) {
				$err.push($('<p class="ui-ajax-success-msg my-3">').
					text("Change successful, but errors occured during hook processing. " +
					"Log out of the panel manually, then log back in with new credentials."),
					$('<hr />')
				);
			}
			for (var i in status['errors']) {
				$err.push($('<p class="ui-ajax-error-msg">').text(status['errors'][i]));
			}
			dialogWindow.find('.modal-body').append($err);
		}
		$indicator.removeClass('ui-ajax-loading').addClass(destroyed ? 'ui-ajax-warning' : 'ui-ajax-error');
		$('#modal .btn-primary').prop('disabled', function (i, val) {
			$(this).text($(this).data('old-title'));
			return false;
		}).text();

		return false;
	}).done(function (status, textStatus, xhr) {
		if (!sessionDestroyed() || !status['success']) {
			return apnscp.ajaxError(xhr, textStatus, "unknown error");
		}

		var time = 3000;
		$indicator.removeClass('ui-ajax-loading').addClass('ui-ajax-success');
		dialogWindow.find('.modal-body').empty().append($('<p class="ui-ajax-success-msg">Success! Redirecting you to logout in ' +
			'<span class="seconds">' + time / 1000 + '</span>...</p>'));
		setInterval(function () {
			time -= 1000;
			dialogWindow.find('.seconds').text(time / 1000);
			if (time <= 0) {
				var domain = ruleMap['domain']['default'],
					username = ruleMap['username']['default'];
				if (rule == 'domain') {
					domain = data;
				} else if (rule == 'username') {
					username = data;
				} else if (rule == 'wipe') {
					username = session.user;
					domain = session.domain;
				}

				dialogWindow.modal('hide');
				window.location = '/logout?server=' + session.server + '&domain=' + domain + '&username=' + username;
			}
		}, 1000);
		return false;
		// failure
	});
}

function add(ip, $self) {
	apnscp.cmd('auth_restrict_ip', [ip], null, {useCustomHandlers: true}).then(function (data, textStatus, jqXHR) {
		var found = false;
		$('#active .row :submit').each(function (idx, e) {
			if (e.value === ip) {
				found = true;
				return false;
			}
		});
		if (found) {
			throw "IP " + ip + " is already authorized";
		}
		$('#ipInput').val("").effect("transfer", {to: $('#ip-entries')}, 500, function () {
			var $elem = $('#item-template').clone(true).data('ip', ip).
				removeAttr('id').find('.ip').text(ip).end().find(':submit').val(ip).end().removeClass('hide');
			$('#ip-entries .note').before($elem);
		});
	}).fail(function (jqXHR, textStatus, errorThrown) {
		var reason = [jqXHR];
		if (typeof jqXHR !== 'string') {
			// invalid IP, bad response from API
			reason = $.parseJSON(jqXHR.responseText)['errors'] || [];
		}
		for (var i in reason) {
			$("#errors").append($('<li>').text(reason[i]));
		}
		$('#ipInput').one('keypress', function () {
			$('#errors').empty();
		});

	});
	return false;
}

function remove(ip, $self) {
	apnscp.cmd('auth_remove_ip_restriction', [ip]).done(function (e) {
		$self.closest('.row').fadeOut('fast', function () {
			$(this).remove();
		});

	});
	return false;
}
