$(document).ready(function () {
	var fields = ['os_win', 'os_linux'];
	jQuery.each(fields, function (f, k) {
		$('select#' + k).change(function () {
			reset(k);
			selected = $(this).val();
			$('#instructions').children('div').each(function (a) {

				if ($(this).attr('id') == selected) {
					$(this).css("display", 'block');
				} else {
					$(this).css("display", 'none');
				}
			});
			if (selected == 'nautilus' || selected == 'winvista')
				return false;
			frames.davaction.location.href = '/apps/webdisk?download=' + selected + '&domain=' + session.domain + '&host=' + session.serverFull + '&port=2077';
			document.forms['dav'].action = '/apps/webdisk?download=' + selected + '&domain=' + session.domain + '&host=' + session.serverFull + '&port=2077';
			//alert($(this).val());

		});
	});

	function reset(r) {
		jQuery.each(fields, function (f, k) {
			if (k == r)
				return true;
			$('select#' + k).get(0).selectedIndex = 0;
		});
	}
});

$(window).on('load', function () {
	var COPY_SUCCESS_MSG = 'copied!';
	var clipboard = new Clipboard('[data-clipboard-target]');
	$('[data-toggle=tooltip]').tooltip();
	clipboard.on('success', function (e) {
		var originalTitle = $(e.trigger).attr('data-original-title');
		$(e.trigger).attr('data-original-title', COPY_SUCCESS_MSG).tooltip('show').on('hidden.bs.tooltip', function (e) {
			$(e.currentTarget).attr('data-original-title', originalTitle);
		});

		e.clearSelection();
	});
});
