@php $hasManifest = $app->hasManifest(); @endphp
<div class="btn-group mb-3" role="group">
	@if (!$hasManifest)
		<button data-note-target="#manifestNote" id="manifest" type="submit"
		        class="btn btn-secondary" value="create" name="manifest">
			<i class="fa fa-flask"></i> {{ _("Create Manifest") }}
		</button>
	@elseif (!$app->getManifest()->verifySignature())
		<button type="submit" name="manifest" class="btn btn-secondary text-danger"
		        value="sign">
			<i class="fa fa-exclamation-triangle mr-1"></i>
			{{ _("Sign Manifest") }}
		</button>
		<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
		        aria-expanded="false">
			<span class="sr-only">Toggle Dropdown</span>
		</button>
	@else
		<button data-note-target="#manifestNote" id="manifest" type="button"
		        class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
		        aria-haspopup="true" aria-expanded="false">
			<i class="fa fa-flask"></i> {{ _("Manifest") }}
		</button>
	@endif

	@if ($hasManifest)
		<div class="dropdown-menu  dropdown-menu-right">
			<a class="ui-action ui-action-label ui-action-edit btn-block dropdown-item"
			   href="{{ \HTML_Kit::new_page_url_params(\Template_Engine::init()->getPathFromApp('filemanager'), ['f' => $app->getManifest()->getManifestPath()]) }}">
				Edit Manifest
			</a>
			<div class="dropdown-divider">  </div>
			<button type="submit" name="manifest" value="delete"
			        class="btn-block dropdown-item ui-action warn ui-action-delete ui-action-label" href="#">
				{{ _("Remove Manifest") }}
			</button>
		</div>
	@endif
</div>