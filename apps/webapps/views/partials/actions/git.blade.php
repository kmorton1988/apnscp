@if (!$app->hasGit())
	<button data-note-target="#gitHelp" type="submit" class="mb-3 btn btn-secondary"
	        name="git-enable" value="{{ $app->getDocumentMetaPath() }}">
		<i class="fa fa-git-alt"></i>
		Enable Snapshots
	</button>
@else
	<div class="btn-group mb-3">
		<button data-note-target="#gitHelp" id="git-checkpoint" type="submit" class="btn btn-secondary"
		        name="git-checkpoint" value="{{ $app->getDocumentMetaPath() }}">
			<i class="ui-action ui-action-snapshot"></i>
			Create Snapshot
		</button>
		<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
		        aria-haspopup="true" aria-expanded="false">
			<span class="sr-only">Toggle Dropdown</span>
		</button>
		<div class="dropdown-menu  dropdown-menu-right">
			<button id="git-manage" name="git-manage" value="{{ $app->getDocumentMetaPath() }}"
			        type="submit" class="dropdown-item btn-block ui-action ui-action-restore ui-action-label">
				Restore Snapshot
			</button>
			<div class="dropdown-divider"></div>
			<button name="git-delete" class="dropdown-item ui-action ui-action-delete ui-action-label btn-block warn btn"
			   value="{{ $app->getDocumentMetaPath() }}">
				Delete Snapshots
			</button>
		</div>
	</div>
	@include('partials.modals.git')
	@include('partials.modals.git-snapshot')
@endif