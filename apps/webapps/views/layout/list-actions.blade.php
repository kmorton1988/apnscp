<div class="btn-group {{ $btnGroupClass ?? '' }}">
	<a class="ui-action ui-action-select ui-action-label btn btn-secondary" href="{{ $href }}">
		Select
	</a>
	<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
	        aria-haspopup="true"
	        aria-expanded="false">
		<span class="sr-only">Toggle Dropdown</span>
	</button>
	<div class="dropdown-menu {{ $dropdownLocation ?? 'dropdown-menu-left' }}" aria-labelledby="">
		<a href="{{ HTML_Kit::page_url_params(['stick' => $pane->getDocumentRoot(), 'smode' => $pane->getMeta('options.affixed') ? "0" : "1"]) }}"
		   class="fa fa-thumb-tack ui-action-label dropdown-item ui-action w-100">
			@if ($pane->getMeta('options.affixed'))
				Unpin Site
			@else
				Pin Site
			@endif
		</a>
		<a class="ui-action-folder-browse ui-action-label dropdown-item ui-action"
		   data-docroot="{{ $pane->getDocumentRoot() }}"
		   data-hostname="{{ $pane->getHostname() }}" data-path="{{ $pane->getPath() }}"
		   href="#">
			Select Folder
		</a>
		@if (!$pane->getPath() && $pane->getHostname() !== \Session::get('domain'))
			<a class="fa-globe ui-action-label dropdown-item ui-action"
			   href="{{ $pane->addonTypeMangementLink() }}">
				Manage @if ($pane->isSubdomain()) Subdomain @else Domain @endif
			</a>
		@endif
		<a class="ui-action-visit-site ui-action-label dropdown-item ui-action"
		   href="{{ $pane->getUrl() }}">
			Visit Site
		</a>
		<a class="ui-action-manage-files ui-action-label dropdown-item ui-action"
		   href="{{ HTML_Kit::new_page_url_params('/apps/filemanager', ['f' => $pane->getDocumentRoot()]) }}">
			Manage Files
		</a>
		<a class="fa-apache ui-action-label dropdown-item ui-action"
		   href="{{ HTML_Kit::new_page_url_params('/apps/personalities', $pane->toArray())}}">
			Edit .htaccess
		</a>

		<div class="dropdown-divider"></div>
		@if ($pane->getMeta('failed'))
			<a class="ui-action fa-check dropdown-item ui-action-label ui-action-warn"
			   data-toggle="tooltip"
			   data-title="Clear FAILED status. Enroll site in 1-click updates."
			   href="{{  HTML_Kit::new_page_url_params(null, ['clear' => $pane->getDocumentRoot()]) }}">
				Reset Failed
			</a>
		@endif

		<a class="ui-action-delete ui-action-label dropdown-item ui-action"
		   href="{{ HTML_Kit::new_page_url_params('/apps/webapps', ['forget' => $pane->getDocumentRoot()])}}">
			Forget Application
		</a>

	</div>

</div>