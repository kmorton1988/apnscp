<hr/>
<h5>{{ $domain }} details</h5>
<table class="table table-responsive table-striped border">
	<tr>
		<th>
			DNS driver
		</th>
		<td>
			{{-- Call $Page instead of \cmd() as $Page has the proper authContext afi instance --}}
			{{ $Page->dns_get_provider() }}
		</td>
	</tr>
	<tr>
		<th>
			Domain verified
		</th>
		<td>
			@if ($Page->dns_verified($domain))
				<i class="fa fa-check text-success" title="{{ _("Domain is verified") }}"></i>
			@else
				<span class="badge badge-warning">Pending</span>
			@endif
		</td>
	</tr>
	<tr>
		<th>
			Hosting nameservers
		</th>
		<td>
			@if ($usesNS = $Page->dns_domain_uses_nameservers($domain))
				<i class="fa fa-check text-success" title="{{ _("Domain uses configured nameservers") }}"></i>
			@endif
			{{ implode(", ", $Page->getHostingNameservers($domain) ?: ['UNKNOWN']) }}
		</td>
	</tr>
	@if (!$usesNS)
		<tr>
			<th>
				Detected nameservers
			</th>
			<td>
				{{ implode(", ", $Page->getAuthoritativeNameservers($domain) ?: ['UNKNOWN']) }}
			</td>
		</tr>
	@endif
	@if ($ts = $Page->dns_domain_expiration($domain))
		<tr>
			<th>Expiration date</th>
			<td>
				{{ UCard::get()->formatDate((new DateTime())->setTimestamp($ts), IntlDateFormatter::MEDIUM) }}
			</td>
		</tr>
	@endif
</table>
<hr />