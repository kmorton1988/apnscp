@if (!$Page->getDomain() && \UCard::get()->is('admin'))
	@if (\Error_Reporter::is_error())
		<p class="alert alert-danger">
			<b>An error was encountered while reading domains</b> <br/>
			{{ \Error_Reporter::get_errors()[0] }}
		</p>
	@else
		<p class="alert alert-info">
			Select a domain from above to get started
		</p>
	@endif
@else
	<p class="alert alert-danger">
		<i class="fa fa-exclamation-triangle"></i>
		@if (!\cmd('dns_configured'))
			DNS management unavailable. DNS has not been configured
			for this account. Contact your administrator.
		@else
			DNS management unavailable; non-authorative zone.
			Please add the domain via <b>DNS</b> &gt; <b>Addon Domains</b>.
		@endif
	</p>
@endif