// jQuery File Tree Plugin
//
// Version 1.01
//
// Cory S.N. LaViska
// A Beautiful Site (http://abeautifulsite.net/)
// 24 March 2008
//
// Visit http://abeautifulsite.net/notebook.php?article=58 for more information
//
// Usage: $('.fileTreeDemo').fileTree( options, callback )
//
// Options:  root           - root folder to display; default = /
//           script         - location of the serverside AJAX file to use; default = file-manager.php
//           folderEvent    - event to trigger expand/collapse; default = click
//           expandSpeed    - default = 500 (ms); use -1 for no animation
//           collapseSpeed  - default = 500 (ms); use -1 for no animation
//           expandEasing   - easing function to use on expand (optional)
//           collapseEasing - easing function to use on collapse (optional)
//           multiFolder    - whether or not to limit the browser to one subfolder at a time
//           loadMessage    - Message to display while initial tree loads (can be HTML)
//
// History:
//
// 1.01 - updated to work with foreign characters in directory/file names (12 April 2008)
// 1.00 - released (24 March 2008)
//
// TERMS OF USE
//
// jQuery File Tree is licensed under a Creative Commons License and is copyrighted (C)2008 by Cory S.N. LaViska.
// For details, visit http://creativecommons.org/licenses/by/3.0/us/
//
var fileTree =  {
	o: {},
	$lastChild: null,
	_treeInfo: {
		dir: null
	},


	defaults: {
		root:           '/',
		script:         '/ajax_engine?engine=file_browser',
		filter:     'filter=file,0;chown,1;show,/var/www;show,/home;show,/usr/local',
		folderEvent:    'click',
		expandSpeed:    0,
		collapseSpeed:  200,
		expandEasing:   'easeInOutBack',
		selected:       null,
		collapseEasing: 'easeInOutBack',
		multiFolder:    false,
		init:           true,
		loadMessage:    'Loading...',
		onSelect:    function() {},
		onClose:        function () { return this.close(); },
		onOpen:		function() {}
	},

	fileTree: function (o,h)
	{
		// Defaults
		$.extend(this.o, this.defaults, o || {});
		if (this.o.init) return this.showTree();
		return this;

	},

	redisplay: function (o,dir)
	{
		var oldFilter = this.o.filter;
		$.extend(this.o, o);
		// new filter setup
		$(this).find('.selected').removeClass('selected');
		if (o.filter && o.filter != oldFilter) {
			$(this).empty();
			this.$lastChild = undefined;
			this.showTree(this.o.dir)
		}
		if (dir) return this.open(dir);
		if (this.$lastChild === null)
				this.$lastChild = $('ul.file-manager li.expanded:last');
		return this;

	},

	showTree: function(dir)
	{
		var o = this.o;

		this.append('<ul class="file-manager start"><li class="wait">' + this.o.loadMessage + '<li></ul>');
		// Get the initial file list
		this.loadDir( $(this), this.o.root );
		this.bindTree();
		if (this.o.selected) this.open(this.o.selected);
		return this;
	},

	createDirectory: function(t)
	{
		if ($('input#new_dir').length > 0 ) return;
		var $dirInput = $('<input type="text" class="form-control" id="new_dir" value="" />');
		var $parentDir = $('li.expanded:last',this);

		$dirInput.bind('blur keypress',
			{base: $('a:last-of-type',$parentDir).attr('rel')},
			function(e) {
				if (e.type == 'keypress') {
					if (e.which != 0 && e.which != 13 /* enter */)
						return true;
					else
						if (e.which == 0)
							$(this).val("");
				}
				var dir = $(this).val();
				if (dir)
					apnscp.cmd('file_create_directory',[e.data.base + '/' + dir], function (r) {
						var $dir = $('<li class="directory collapsed"><a rel="' +
							e.data.base + '/' + dir + '" href="#">' + dir + '</a></li>');
						// parent has children
						// $parentDir li a(parent) > ul li a(subdir), li a(s2) ...
						if ($('ul',$parentDir).length < 1) {
								$parentDir.append($('<ul style="display: block;" class="file-manager"></ul>'));
						}
						$parentDir = $('ul',$parentDir);
						var append=0;
						for (var i=-1, newDir=''+dir.toLowerCase(),
							$members = $parentDir.children('li.directory'); i < $members.length; i++) {
							if ('' + ($($members[i]).children('a').text()).toLowerCase() > newDir) break;
							append=i;
						}
						if (append < 0) $parentDir.prepend($dir);
						else $parentDir.children(':eq(' + append + ')').after($dir);
						$parentDir.parent().removeClass('selected');
						$dir.find('a').click();

					});
				$dirInput.remove();
			});
		$dirInput.insertAfter($parentDir.children('a:last-of-type')).focus();

	},

	/**
	 * Open modal dialog
	 *
	 * @param dir
	 */
	open: function(dir)
	{
		var t = this;
		$(this).queue("browser", function () {
			t._openDir( $('UL:first LI',t), dir);
			var $active =  $('LI.expanded:last');
			$active.addClass('selected');
			this.$lastChild = $active;
			if (t.o.onSelect) {
				t.o.onSelect(dir,$active);
			}
			t.o.onOpen();
		});

	},

    close: function(dir)
    {
        $(document).off(this.o.folderEvent,  'ul.file-manager li a');
    },

	// open directory
	_openDir: function($t, dir)
	{
		var $parent, $el, leafpos;
		var dir = escape(dir);
		var tokens = dir.split("/");
		var leaves = [];
		$parent = $el = $t.parent();
		for (var i=tokens.length, leafpos=0; i > 0; i--)
		{
			var token = tokens[i-1];
			if (!token) continue;
			var rel = tokens.slice(0, i).join("/");
			// find leaf
			if (!leafpos) {
				$el = $('A[rel="' + rel + '"]', $parent);
				if (!$el.length) {
					leaves.push(token);
					continue;
				}
				// found
				leafpos = i;
			}
			// mark remaining directories as open
			$('LI A[rel="' + rel + '"]', $parent).parent().removeClass('collapsed').addClass('expanded');
			$parent = $parent.parent().parent();
		}
		$t = $el.parent();
		if (leaves.length) {
			// folder contains leaves
			$el.after($(this._doLeaf(leaves.pop(), leaves, tokens.slice(0, leafpos).join("/"))));
			$t = $t.find('LI.expanded:last').removeClass('leaf');
		}
		this.$lastChild = $t;
		this.loadDir($t, dir);
		this._treeInfo.dir = dir;
	},

	// Populate leaf directories
	_doLeaf: function(leaf, leaves, base)
	{
		if (!leaf) return '';
		return '<UL class="file-manager"><li class="directory ' + (leaves.length ? 'leaf' : '') + ' expanded"><a rel="' +
				base + '/' + leaf + '" href="#">' + leaf + this._doLeaf(leaves.pop(), leaves, base + '/' + leaf) + '</a></LI></UL>';
	},

	loadDir: function($t, dir)
	{
		var t = this;
		var o = this.o;
		$t.addClass('wait');
		$.post(o.script + '&' + o.filter, { dir: dir }, function(d) {
			if (dir == o.root) {
				$t.empty();
			}
			$('UL', $t).remove();
			if ($t.hasClass('leaf')) {
				d = $(d);
				var $replace = $('LI A[rel="' + $('UL LI A',$t).attr('rel') + '"]', d).parent().parent();
				$replace.replaceWith($('UL > LI:first', $t));
				$t.removeClass('leaf');
			}

			$t.append(d).removeClass('wait');
			$('UL',$t).show({ duration: o.expandSpeed, easing: o.expandEasing });
			return $(t).dequeue("browser");
		} /* fix for pagespeed */);

	},

	// register click listeners
	bindTree: function()
	{
		var o = this.o;
		var t = this;
		// @XXX jQuery 1.5.1 doesn't properly select when in context of this
		$(document).on(o.folderEvent, 'ul.file-manager li a', function()
		{
			var $dirent    = $(this);
			var $active_li = $dirent.parent();

			// Entry is Directory
			if( $active_li.hasClass('directory') ) {
				// Collapse
				if( !$active_li.hasClass('collapsed') ) {
					$active_li.find('UL').slideUp({ duration: o.collapseSpeed, easing: o.collapseEasing });
					$active_li.removeClass('expanded').addClass('collapsed');
					return false;
				}

				// Expand
				if( !o.multiFolder ) {
					$active_li.parent().find('UL').slideUp(
					{
						duration: o.collapseSpeed,
						easing: o.collapseEasing
					});
					$active_li.parent().find('LI.directory').removeClass('expanded').addClass('collapsed');
				}

				// Cleanup

				if (t.$lastChild) {
					t.$lastChild.removeClass('selected');
				}

				// Expand child
				t._openDir($active_li,$dirent.attr('rel').match( /.*/ ) );

				$active_li.removeClass('collapsed').addClass('expanded selected');
				t.$lastChild = $active_li;

			}
			if (o.onSelect) o.onSelect($dirent.attr('rel'), $active_li);

			return false;
		});

		// Prevent A from triggering the # on non-click events
		if( o.folderEvent.toLowerCase != 'click' ) t.find('LI A').bind('click', function() { return false; });
	}
}

$.fn.extend(fileTree);
