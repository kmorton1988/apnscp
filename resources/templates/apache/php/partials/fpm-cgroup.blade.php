####################
# cgroup accounting
#
# See https://www.freedesktop.org/software/systemd/man/systemd.resource-control.html
#
####################
# @TODO
# Slice={{ $config->getResourceManager()->getGroup() }}
CPUAccounting=yes
MemoryAccounting=yes
BlockIOAccounting=yes
TasksAccounting=yes