@includeWhen(!HTTPD_ALL_INTERFACES && !$svc->getServiceValue('ipinfo','namebased'), 'listen-directive', ['ips' => $ips, 'port' => $port, 'proto' => $proto])

{{-- HTTP configuration that appears for non-SSL requests --}}
<VirtualHost @include('common.ip-addresses', ['ips' => $ips, 'port' => $port])>
	@include("common.virtualhost-config")

	{{-- locate in config/custom/resources/templates/apache/, affects non-SSL virtualhost --}}
	@includeIf("virtualhost-custom")

</VirtualHost>
