@php
	/**
	 * @var \Opcenter\SiteConfiguration $svc
	 */
 @endphp
@component('mail::message')
# Welcome to {{ PANEL_BRAND }}!

Below you will find your account information for accessing your account.

## {{ PANEL_BRAND }} Control Panel

{{{ PANEL_BRAND }}} is your comprehensive control panel to manage your web site, email, DNS– the whole kitchen sink.

**Login:** `{{ $svc->getServiceValue('siteinfo', 'admin_user') }}` <br />
**Domain**: `{{ $svc->getServiceValue('siteinfo', 'domain') }}` <br/>
@if ($passwd = ($svc->getNewConfiguration('auth')['tpasswd'] ?? 'HIDDEN'))
**Password**: `{{ $passwd }}` <br/>
@endif

@component('mail::button', ['url' => \Auth_Redirect::getPreferredUri()])
{{ PANEL_BRAND }} Login
@endcomponent

## DNS
@if ($nameservers = $svc->getSiteFunctionInterceptor()->dns_get_hosting_nameservers($svc->getServiceValue('siteinfo', 'domain')))
Change your nameservers to {{ implode(', ', $nameservers) }} before your domain is live. DNS within {{ PANEL_BRAND }}
allows you unrestricted management of your DNS records and perfect integration with SSL issuance.
@if ($hasNameservers = $svc->getSiteFunctionInterceptor()->dns_get_authns_from_host($svc->getServiceValue('siteinfo', 'domain')))
You may retain your current nameservers *({{ implode(', ', $hasNameservers) }})* and simply change the IP address
to {{ $svc->getSiteFunctionInterceptor()->dns_get_public_ip() }}.
@else
Your domain doesn't appear to be registered. When it is, change the nameservers to {{ implode(', ', $nameservers) }}.
@endif
@endif

### Previewing domain
A domain may be previewed by overriding your hosts file. Helpful steps are outlined in the
[knowledgebase]({{ MISC_KB_BASE }}/dns/previewing-your-domain/). Your IP address is
`{{ $svc->getSiteFunctionInterceptor()->dns_get_public_ip() }}`.

@if ($svc->getServiceValue('mail', 'enabled'))
## Email login
**Hostname**: `{{ SERVER_NAME }}` <br/>
**Login**: `{{ $svc->getServiceValue('siteinfo', 'admin_user') }}{{ '@' . $svc->getServiceValue('siteinfo', 'domain') }}` <br/>
@if ($passwd)
**Password**: `{{ $passwd }}` <br/>
@endif
@endif

@if ($svc->getServiceValue('ssh', 'enabled'))
## SSH login
**Hostname**: `{{ SERVER_NAME }}` <br/>
**Login**: `{{ $svc->getServiceValue('siteinfo', 'admin_user') }}{{ '@' . $svc->getServiceValue('siteinfo', 'domain') }}`
<br/>
@if ($passwd)
**Password**: `{{ $passwd }}` <br/>
@endif
@endif

@if ($svc->getServiceValue('ftp', 'enabled'))
## FTP login
**Hostname**: `{{ SERVER_NAME }}` <br/>
**Login**: `{{ $svc->getServiceValue('siteinfo', 'admin_user') }}{{ '@' . $svc->getServiceValue('siteinfo', 'domain') }}`
<br/>
@if ($passwd)
**Password**: `{{ $passwd }}` <br/>
@endif
@endif

---

### Note on email
When setting up email, most clients will autoconfigure server settings. If your mail client requires manual configuration,
see [Accessing Email]({{ MISC_KB_BASE }}/email/accessing-e-mail/) in the knowledgebase.

Your account, **{{ $svc->getServiceValue('siteinfo', 'domain') }}** has been created.

---
@endcomponent

