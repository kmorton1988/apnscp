@component('mail::message')
# Hello,

It's your control panel, {{ PANEL_BRAND }}. Everything is setup on this server!
You can access the panel using the following URL:

---

**SERVER ADDRESS:** {{ $ip }}<br />
[https://{{ $hostname }}:2083](https://{{ $hostname }}:2083)

## Admin Credentials
**LOGIN:** `{{ $admin_user }}`<br />
**PASSWORD:** `{{ $admin_password }}`<br />

---

You should change your password as soon as possible in the panel through
**Account** > **Settings** or from the command-line:

`sudo {{ $apnscp_root }}/bin/cmd auth_change_password NEWPASSWORD`

Thanks for making Apis Networks a part of your hosting experience!

\- Matt Saladna<br />
Owner + Lead Developer

[apisnetworks.com](https://apisnetworks.com) · [Documentation](https://docs.apiscp.com) · [Blog](https://hq.apiscp.com)

@endcomponent

