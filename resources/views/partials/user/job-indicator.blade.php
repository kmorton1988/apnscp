<div id="ui-job-indicator" class="mx-3 ui-job-indicator">
	<span class="job-counter badge"></span>
	<div class="indicator  d-flex align-items-center justify-content-center dropdown-toggle" data-toggle="dropdown"
	        aria-haspopup="true" aria-expanded="false">
		@include('theme::partials.shared.gear-indicator', ['height' => 40])
		<span class="sr-only">Toggle Dropdown</span>
	</div>
	<div class="dropdown-menu  dropdown-menu-right pb-0">
		<ul id="ui-job-queue" class="px-2 list-unstyled mb-0"></ul>
		<span class="font-italic d-none px-2 pb-2 text-center">No jobs active</span>
		@if (\Lararia\JobDaemon::isStandalone())
			<hr class="my-0" />
			<a class="dropdown-item btn-block ui-menu-category-job-runner ui-action-label text-center"
			   href="{{ \Template_Engine::init()->getApplicationFromId('horizon')->getLink() }}">
				Job Runner
			</a>
		@endif
	</div>
</div>