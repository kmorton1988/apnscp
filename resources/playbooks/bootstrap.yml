#######################
# apnscp Bootstrapper #
#######################
#
# A utility for apnscp to provision a CentOS/RHEL 7.4+ platform
# for use with apnscp
#
# Part of apnscp core playbooks
# https://github.com/apisnetworks/apnscp-playbooks
#
---
- hosts: localhost
  module_defaults:
    yum:
      lock_timeout: "{{ yum_lock_max_wait }}"
    mysql_user:
      login_unix_socket: /var/lib/mysql/mysql.sock
    mysql_db:
      # Packet out of order error on CentOS 8
      login_unix_socket: /var/lib/mysql/mysql.sock
  become: yes
  connection: local
  gather_facts: yes
  vars_files:
    - 'apnscp-vars.yml'
    - 'roles/common/vars/apnscp-internals.yml'
    - ['{{ apnscp_user_defaults }}', '/dev/null']
    - ['{{ apnscp_last_run_vars }}', '/dev/null']
  roles:
    # Hook: before running playbooks
    - common/acquire-lock
    - custom/preflight
    - check-requirements
    - common/update-config
    - packages/configure-rhel
    - filesystem/make-mounts
    - system/kernel
    - system/selinux
    - epel
    - systemd/mask-services
    - packages/install
    - system/compiler
    - apnscp/testing
    - pgsql/install
    - mysql/install
    - system/rsyslog
    - vsftpd/configure
    - browscap
    - php/install
    - php/create-configuration
    - java/tomcat
    - apnscp/install-services
    - apnscp/bootstrap
    - apache/fcgid
    - apnscp/build-php
    - apnscp/install-extensions
    - apnscp/install-vendor-library
    - apnscp/initialize-db
    - software/powerdns
    # Checkpoint: admin should work fine at this point
    - apnscp/create-admin
    - apnscp/assert-admin-works
    # Hook: apnscp is minimally viable
    - custom/bootstrapped
    # Onto provisioning a test account
    - network/hostname
    - apache/configure
    - apache/modpagespeed
    - apnscp/link-bins
    - apnscp/initialize-filesystem-template
    - php/multiphp
    - apnscp/php-filesystem-template
    - php/install-pecl-module
    - apnscp/service-template
    - mail/configure-dovecot
    - mail/configure-postfix
    - software/haproxy
    - mail/configure-courier-authlib
    - apnscp/bandwidth-log
    - apnscp/storage-log
    - apnscp/admin-helper
    - apnscp/dev
    - system/nscd
    # todo bring up
    - system/sssd
    - system/nss
    - system/pam
    - system/limits
    - system/cgroup
    - system/sysctl
    - system/tuned
    - system/sshd
    - network/setup-firewall
    - fail2ban/whitelist-self
    - fail2ban/configure-jails
    - mail/maildir
    - mail/spamassassin
    - mail/rspamd
    - apnscp/register-ssl
    - network/optimizations
    # Odds and ends
    - apnscp/crons
    - system/yum
    - mysql/phpmyadmin
    - pgsql/phppgadmin
    - mail/webmail-squirrelmail
    - mail/webmail-roundcube
    - mail/webmail-horde
    - software/chrony
    - software/nvm
    - software/pyenv
    - software/rbenv
    - software/goenv
    - software/scl
    - software/imagick
    - software/passenger
    - software/etckeeper
    - software/tmpfiles
    - software/watchdog
    - software/telegraf
    - system/logs
    - clamav/setup
    - apache/modsecurity
    - system/misc-logrotate
    - software/argos
    - apnscp/filesystem-checks
    # Hook: before creating account (AddDomain)
    # Perform additional unit tests
    - custom/validate-account
    # Last checkpoint, validate a mock account
    - apnscp/assert-account-works
    - apnscp/notify-installed
    ####################################
    # Add post-provisioning roles here #
    ####################################
    - custom/installed

    #
    # Thanks for playing!
    #
    # (c) 2018 Apis Networks
