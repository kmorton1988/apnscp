---
- name: Query fail2ban version
  set_fact:
    fail2ban_version: "{{ lookup('pipe', 'rpm -q --queryformat=%{VERSION} fail2ban') }}"
- name: "Add firewallcmd-ipset-allports action"
  copy: src=files/firewallcmd-ipset-allports.conf dest="{{ fail2ban_directory }}/action.d/"
- name: Set jails
  include_tasks: set-jail.yml
  vars:
    name: "{{ config_grp.name }}"
    settings: "{{ config_grp.vars }}"
  with_items: "{{ jails }}"
  loop_control:
    loop_var: config_grp
    label: "Configuring jail {{ config_grp.name }}"
- name: Update filters
  copy:
    src: "files/{{ item.name }}.conf"
    dest: "{{ fail2ban_directory }}/filter.d/{{ item.name }}.conf"
  when: item.when | default(true)
  with_items: "{{ f2b_updated_filters }}"
  loop_control:
    label: "Replace filter.d/{{ item }}.conf"
  notify: Restart fail2ban
- name: Update filter regexes
  ini_file:
    path: "{{ fail2ban_directory }}/filter.d/{{ item.filter }}"
    section: "{{ item.section }}"
    option: "{{ item.option }}"
    value: "{{ item.value }}"
    state: present
  with_items: "{{ regex_updates }}"
  notify: Restart fail2ban
- template:
    src: templates/apnscp.conf.j2
    dest: /etc/fail2ban/jail.d/01-apnscp.conf
    force: yes
  notify: Restart fail2ban
- name: Increase max open files for fail2ban-server
  include_role: name=systemd/override-config
  vars:
    service: fail2ban
    config:
      - group: Service
        vars:
          LimitNOFILE: 16384
          TimeoutStartSec: "{{ fail2ban_service_timeout }}"
          TimeoutStopSec: "{{ fail2ban_service_timeout }}"
          PrivateDevices: "yes"
          PrivateTmp: "yes"
          ProtectHome: "read-only"
          # Enable postdrop usage, alternatively SupplementaryGroups: postdrop
          NoNewPrivileges: "no"
          CapabilityBoundingSet: "CAP_AUDIT_READ CAP_DAC_READ_SEARCH CAP_NET_ADMIN CAP_NET_RAW"
          # Has meaning on RHEL8, but not 7
          ProtectSystem: "{{ (ansible_distribution_major_version | int >= 8) | ternary('strict', None) }}"
          # /var/log requires r/w from logrotate moving file handle *or* add copytruncate to logrotate and possibly lose records
          ReadWritePaths: "{{ (ansible_distribution_major_version | int >= 8) | ternary('-/var/run/fail2ban -/var/lib/fail2ban /var/log -/var/spool/postfix/maildrop', None) }}"
- name: Setup pruning service
  template:
    src: templates/prune-db.sql.j2
    dest: /etc/cron.weekly/prune_fail2ban_db
    mode: 0755
  when: (f2b_prune_duration | int) > -1

- name: Alter dbpurgeage
  ini_file:
    path: "{{ fail2ban_directory }}/fail2ban.local"
    section: Definition
    option: dbpurgeage
    value: "{{ f2b_dbpurgeage }}"
  notify: Restart fail2ban
- name: Remove fail2ban database prune task
  file: path="/etc/cron.weekly/prune_fail2ban_db" state=absent
  when: (f2b_prune_duration | int) == -1

- include_role: name=apnscp/bootstrap tasks_from=set-config.yml
  vars:
    section: rampart
    option: "{{ item.option }}"
    value: "{{ item.value }}"
  with_items:
    - {option: driver, value: "{{ f2b_rampart_driver }}" }
    - {option: prefix, value: "{{ f2b_rampart_prefix }}" }
- meta: flush_handlers
